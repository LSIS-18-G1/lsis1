/* Apagar base de dados antiga */
DROP DATABASE robo;
/* Criar base de dados */
CREATE DATABASE IF NOT EXISTS robo DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
/* Usar base de dados */
USE robo;
/* Criar tabelas */
CREATE TABLE IF NOT EXISTS equipa (
	id BIGINT UNSIGNED AUTO_INCREMENT,
    nome VARCHAR(100),
	PRIMARY KEY(id) ) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE TABLE IF NOT EXISTS elementos (
	id  BIGINT UNSIGNED AUTO_INCREMENT,
	idequipa BIGINT,
    nome VARCHAR(100),
    num INT,
    mail VARCHAR(100),
	PRIMARY KEY(id) ) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE TABLE IF NOT EXISTS robo (
	id  BIGINT UNSIGNED AUTO_INCREMENT,
	idequipa BIGINT,
    nome_do_robo VARCHAR(100),
    modelo VARCHAR(100),
    num_sensor INT,
    melhor_tempo FLOAT,
	PRIMARY KEY(id) ) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE TABLE IF NOT EXISTS prova (
	id  BIGINT UNSIGNED AUTO_INCREMENT,
	idequipa BIGINT,
    idrobo BIGINT,
    estados VARCHAR(1000),
    tempo FLOAT,
	PRIMARY KEY(id) ) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;