#pragma once
#include <string>
using namespace std;

class Prova {

private:
	//parāmetros da classe Prova
	int id;
	int idequipa;
	int idrobo;
	string estados;
	float tempo;

public:
	//Construtores e Funcao Copia
	Prova();
	Prova(int id, int idequipa, int idrobo, string estados, float tempo);
	Prova(const Prova & prova);//copiar Prova

	//gets
	int getID() const;
	int getIDEquipa() const;
	int getIDRobo() const;
	string getEstados() const;
	float getTempo() const;

	//sets
	void setID(int id);
	void setIDEquipa(int idequipa);
	void setIDRobo(int idrobo);
	void setEstados(string estados);
	void setTempo(float tempo);

};