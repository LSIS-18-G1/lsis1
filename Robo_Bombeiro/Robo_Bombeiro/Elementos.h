#pragma once
#include <string>
using namespace std;

class Elementos {

private:
	//parāmetros da classe Elementos
	int id;
	int idequipa;
	string nome;
	int num;
	string email;

public:
	//Construtores e Funcao Copia
	Elementos();
	Elementos(int id, int idequipa, string nome, int num, string email);
	Elementos(const Elementos & elem);//copiar Elementos

	//gets
	int getID() const;
	int getIDEquipa() const;
	string getNome() const;
	int getNum() const;
	string getEmail() const;

	//sets
	void setID(int id);
	void setIDEquipa(int idequipa);
	void setNome(string nome);
	void setNum( int num);
	void setEmail(string email);

};