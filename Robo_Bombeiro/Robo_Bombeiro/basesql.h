#pragma once
#pragma once
#include "Equipa.h"
#include "Elementos.h"
#include "Robo.h"
#include "Prova.h"
#include <vector>

using namespace std;


class basesql {

public:

	void teste();
	
	void getEquipas(vector<Equipa>& equip);
	void getElementos(vector<Elementos>& elem);
	void getRobos(vector<Robo>& robo);
	void getProvas(vector<Prova>& prova);

	void insertEquipas(Equipa equip);
	void insertElementos(Elementos elem);
	void insertRobos(Robo robo);
	void insertProvas(Prova prova);

	void alterarEquipasNome(int id, string nome);

	void alterarElementosNome(int id, string nome);
	void alterarElementosNum(int id, int num);
	void alterarElementosEmail(int id, string email);

	void alterarRobosNome(int id, string nome);
	void alterarRobosModelo(int id, string modelo);
	void alterarRobosNum_sensor(int id, int num_sensor);
	void alterarRobosMelhor_tempo(int id, double melhor_tempo);

	void alterarProvasEstados(int id, string estados);
	void alterarProvasTempo(int id, double tempo);
	

	void apagarEquipas(int id);
	void apagarElementos(int id);
	void apagarRobos(int id);
	void apagarProvas(int id);
	
};