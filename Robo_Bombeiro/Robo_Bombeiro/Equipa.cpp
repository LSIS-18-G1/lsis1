#include "Equipa.h"
#include <string>

using namespace std;

Equipa::Equipa() {
}

Equipa::Equipa(int id, string nome) {
	this->id = id;
	this->nome = nome;
}

Equipa::Equipa(const Equipa &equip) {
	this->id = equip.id;
	this-> nome = equip.nome;
}

int Equipa::getID() const {
	return this->id;
}
string Equipa::getNome() const {
	return this->nome;
}

void Equipa::setID(int id) {
	this->id = id;
}
void Equipa::setNome(string nome) {
	this->nome = nome;
}