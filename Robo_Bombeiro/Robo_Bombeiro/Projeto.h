#pragma once
#include "Equipa.h"
#include "Elementos.h"
#include "Prova.h"
#include "Robo.h"
#include "basesql.h"
#include <vector>
using namespace std;

class Projeto {
public:
	vector<Equipa> equipas;
	vector<Elementos> elementos;
	vector<Prova> provas;
	vector<Robo> robos;

	void startConnection();

	Projeto();
	Projeto(vector<Equipa> equipas, vector<Elementos> elementos, vector<Robo> robos, vector<Prova> provas);
	Projeto(Projeto &proj);

	vector<Equipa> getListaEquipas();
	vector<Elementos> getListaElementos();
	vector<Robo> getListaRobos();
	vector<Prova> getListaProvas();

	void setListaEquipas(vector<Equipa> equip);
	void setListaElementos(vector<Elementos> elem);
	void setListaRobos(vector<Robo> robo);
	void setListaProvas(vector<Prova> prova);

	void addEquipas(Equipa equip);
	void addElementos(Elementos elem);
	void addRobos(Robo robo);
	void addProvas(Prova prova);

	void alterarEquipas(int pos, int alteracao);
	void alterarElementos(int pos, int alteracao);
	void alterarRobos(int pos, int alteracao);
	void alterarProvas(int pos, int alteracao);

	void apagarEquipas(int pos);
	void apagarElementos(int pos);
	void apagarRobos(int pos);
	void apagarProvas(int pos);
};