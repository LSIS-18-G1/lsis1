#include "Prova.h"
#include <string>

using namespace std;

Prova::Prova() {
}

Prova::Prova(int id, int idequipa, int idrobo, string estados, float tempo) {
	this->id = id;
	this->idequipa = idequipa;
	this->idrobo = idrobo;
	this->estados = estados;
	this->tempo = tempo;
}

Prova::Prova(const Prova &prova) {
	this->id = prova.id;
	this->idequipa = prova.idequipa;
	this->idrobo = prova.idrobo;
	this->estados= prova.estados;
	this->tempo = prova.tempo;
}

int Prova::getID() const {
	return this->id;
}
int Prova::getIDEquipa() const {
	return this->idequipa;
}
int Prova::getIDRobo() const {
	return this->idrobo;
}
string Prova::getEstados() const {
	return this->estados;
}
float Prova::getTempo() const {
	return this->tempo;
}

void Prova::setID(int id) {
	this->id = id;
}
void Prova::setIDEquipa(int idequipa) {
	this->idequipa = idequipa;
}
void Prova::setIDRobo(int idrobo) {
	this->idrobo = idrobo;
}
void Prova::setEstados(string estados) {
	this->estados = estados;
}
void Prova::setTempo(float tempo) {
	this->tempo = tempo;
}