#pragma once
#include "Projeto.h"
#include "Equipa.h"
#include "Elementos.h"
#include "Robo.h"
#include "Prova.h"
#include "Utils.h"

using namespace std;

class ProjetoUI {
public:

	ProjetoUI();

	void startConnection();

	void inserirEquipa();
	void inserirElementos();
	void inserirRobo();
	void inserirProva();

	void alterarEquipa();
	void alterarElementos();
	void alterarRobo();
	void alterarProva();

	void removerEquipa();
	void removerElementos();
	void removerRobo();
	void removerProva();

	void listarEquipa();
	void listarElementos();
	void listarRobo();
	void listarProva();

};