#pragma once
#include <string>
using namespace std;

class Equipa {

private:
	//parāmetros da classe equipa
	int id;
	string nome;

public:
	//Construtores e Funcao Copia
	Equipa();
	Equipa(int id, string nome);
	Equipa(const Equipa & equip);//copiar equipa

	//gets
	int getID() const;
	string getNome() const;
	
	//sets
	void setID(int id);
	void setNome(string nome);

};