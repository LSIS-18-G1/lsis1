#include "Robo.h"
#include <string>

using namespace std;

Robo::Robo() {
}

Robo::Robo(int id, int idequipa, string nome, string modelo, int num_sensor, float melhor_tempo) {
	this->id = id;
	this->idequipa = idequipa;
	this->nome = nome;
	this->modelo = modelo;
	this->num_sensor = num_sensor;
	this->melhor_tempo = melhor_tempo;
}

Robo::Robo(const Robo &robo) {
	this->id = robo.id;
	this->idequipa = robo.idequipa;
	this->nome = robo.nome;
	this->modelo = robo.modelo;
	this->num_sensor = robo.num_sensor;
	this->melhor_tempo = robo.melhor_tempo;
}

int Robo::getID() const {
	return this->id;
}
int Robo::getIDEquipa() const {
	return this->idequipa;
}
string Robo::getNome() const {
	return this->nome;
}
string Robo::getModelo() const {
	return this->modelo;
}
int Robo::getNum_sensor() const {
	return this->num_sensor;
}
float Robo::getMelhor_tempo() const {
	return this->melhor_tempo;
}

void Robo::setID(int id) {
	this->id = id;
}
void Robo::setIDEquipa(int idequipa) {
	this->idequipa = idequipa;
}
void Robo::setNome(string nome) {
	this->nome = nome;
}
void Robo::setModelo(string modelo) {
	this->modelo = modelo;
}
void Robo::setNum_sensor(int num_sensor) {
	this->num_sensor = num_sensor;
}
void Robo::setMelhor_tempo(float melhor_tempo) {
	this->melhor_tempo = melhor_tempo;
}