#include <string>
#include <locale>  
#include "Utils.h"

using namespace std;

string Utils::converterMinuscula(string conv) {

	int i = 0;
	string nova(conv);
	char c;
	locale loc;

	for (i = 0; i < conv.length(); ++i) {
		c = tolower(conv[i], loc);
		nova[i] = c;
	}

	return nova;
}

string Utils::converterMaiuscula(string conv) {

	int i = 0;
	string nova(conv);
	char c;
	locale loc;

	for (i = 0; i < conv.length(); ++i) {
		c = toupper(conv[i], loc);
		nova[i] = c;
	}

	return nova;
}