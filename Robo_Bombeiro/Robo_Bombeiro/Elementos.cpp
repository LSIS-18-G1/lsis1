#include "Elementos.h"
#include <string>

using namespace std;

Elementos::Elementos() {
}

Elementos::Elementos(int id, int idequipa, string nome, int num, string email){
	this->id = id;
	this->idequipa = idequipa;
	this->nome = nome;
	this->num = num;
	this->email = email;
}

Elementos::Elementos(const Elementos &elem) {
	this->id = elem.id;
	this->idequipa = elem.idequipa;
	this->nome = elem.nome;
	this->num = elem.num;
	this->email = elem.email;

}

int Elementos::getID() const {
	return this->id;
}
int Elementos::getIDEquipa() const {
	return this->idequipa;
}
string Elementos::getNome() const {
	return this->nome;
}
int Elementos::getNum() const {
	return this->num;
}
string Elementos::getEmail() const {
	return this->email;
}

void Elementos::setID(int id) {
	this->id = id;
}
void Elementos::setIDEquipa(int idequipa) {
	this->idequipa = idequipa;
}
void Elementos::setNome(string nome) {
	this->nome = nome;
}
void Elementos::setNum(int num) {
	this->num = num;
}
void Elementos::setEmail(string email) {
	this->email = email;
}