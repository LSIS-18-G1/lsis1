#pragma once
#include <string>
using namespace std;

class Robo {

private:
	//parāmetros da classe Robo
	int id;
	int idequipa;
	string nome;
	string modelo;
	int num_sensor;
	float melhor_tempo;

public:
	//Construtores e Funcao Copia
	Robo();
	Robo(int id, int idequipa, string nome, string modelo, int num_sensor, float melhor_tempo);
	Robo(const Robo & robo);//copiar Robo

	//gets
	int getID() const;
	int getIDEquipa() const;
	string getNome() const;
	string getModelo() const;
	int getNum_sensor() const;
	float getMelhor_tempo() const;

	//sets
	void setID(int id);
	void setIDEquipa(int idequipa);
	void setNome(string nome);
	void setModelo(string modelo);
	void setNum_sensor(int num_sensor);
	void setMelhor_tempo(float melhor_tempo);
};