#include <iostream>
#include <string>
#include "ProjetoUI.h"

using namespace std;
Projeto proj;
Utils util;

ProjetoUI::ProjetoUI() {
}

void ProjetoUI::startConnection() {
	proj.startConnection();
}

void ProjetoUI::inserirEquipa() {

	vector<Equipa> equipas= proj.getListaEquipas();

	int id = 0;
	string nome;

	string nomeS;
	bool same = false;
	bool sameS = false;

	cout << endl << "Introduzir Equipa:" << endl;
	cout << endl << "Insira o nome da Equipa:" << endl;
	cin >> nome;
	nome = util.converterMinuscula(nome);
	
	if (proj.getListaEquipas().size() == 0) {
		id = 1;
	}
	else {
		id = proj.getListaEquipas().back().getID() + 1;
	}

	Equipa equip(id, nome);

	if (equipas.size() == 0) {

		proj.addEquipas(equip);
	}
	else {

		for (int i = 0; i < equipas.size(); i++) {

			nomeS = equipas[i].getNome();

			if (nome == nomeS) {
				sameS = true;
			}
		}

		if (sameS == false) {

			proj.addEquipas(equip);
		}
		else {
			cout << endl << "A equipa que pretende inserir ja existe no sistema!" << endl;
		}
	}

}

void ProjetoUI::inserirElementos() {

	vector<Elementos> elementos = proj.getListaElementos();

	int id = 0, idequipa = 0, num=0;
	string nome, email;
	bool same = false;

	int numS = 0;
	string nomeS, emailS;
	bool sameS = false;

	cout << endl << "Introduzir o elemento:" << endl;

	cout << endl << "Insira o nome:" << endl;
	cin >> nome;
	nome = util.converterMinuscula(nome);

	do {
		cout << endl << "Insira o numero do elemento:" << endl;
		cin >> num;
	} while (num <= 0);

	cout << endl << "Insira o email:" << endl;
	cin >> email;
	email = util.converterMinuscula(email);
	

	if (proj.getListaElementos().size() == 0) {
		id = 1;
	}
	else {
		id = proj.getListaElementos().back().getID() + 1;
	}

	Elementos elem(id, idequipa, nome, num, email);

	if (elementos.size() == 0) {

		proj.addElementos(elem);
	}
	else {

		for (int i = 0; i < elementos.size(); i++) {

			nomeS = elementos[i].getNome();
			numS = elementos[i].getNum();
			emailS = elementos[i].getEmail();

			if (nome == nomeS && num == numS && email == emailS) {
				sameS = true;
			}
		}

		if (sameS == false) {

			proj.addElementos(elem);
		}
		else {
			cout << endl << "O elemento que pretende inserir ja existe no sistema!" << endl;
		}
	}
}

void ProjetoUI::inserirRobo() {

	vector<Robo> robos = proj.getListaRobos();

	int id = 0, idequipa = 0, num_sensor = 0;
	string nome, modelo;
	double melhor_tempo;
	bool same = false;

	int num_sensorS = 0;
	string nomeS, modeloS;
	double melhor_tempoS;
	bool sameS = false;

	cout << endl << "Introduzir Robo:" << endl;
	cout << endl << "Insira o nome do robo:" << endl;
	cin >> nome;

	nome = util.converterMinuscula(nome);
	cout << endl << "Insira o modelo do robo:" << endl;
	cin >> modelo;
	modelo = util.converterMinuscula(modelo);

	do {
		cout << endl << "Insira o numero de sensores do robo:" << endl;
		cin >> num_sensor;
	} while (num_sensor <= 0);

	do {
		cout << endl << "Insira o melhor tempo:" << endl;
		cin >> melhor_tempo;
	} while (melhor_tempo <= 0);

	if (proj.getListaRobos().size() == 0) {
		id = 1;
	}
	else {
		id = proj.getListaRobos().back().getID() + 1;
	}

	Robo robo(id, idequipa, nome, modelo, num_sensor, melhor_tempo);

	if (robos.size() == 0) {

		proj.addRobos(robo);
	}
	else {

		for (int i = 0; i < robos.size(); i++) {

			nomeS = robos[i].getNome();
			modeloS = robos[i].getModelo();
			num_sensorS = robos[i].getNum_sensor();
			melhor_tempoS = robos[i].getMelhor_tempo();

			if (nome == nomeS && modelo == modeloS && num_sensor == num_sensorS && melhor_tempo == melhor_tempoS) {
				sameS = true;
			}
		}

		if (sameS == false) {

			proj.addRobos(robo);
		}
		else {
			cout << endl << "O robo que pretende inserir ja existe no sistema!" << endl;
		}
	}
}

void ProjetoUI::inserirProva() {

	vector<Prova> provas = proj.getListaProvas();

	int id=0, idequipa=0, idrobo=0;
	string estados;
	double tempo;
	bool same = false;

	string estadosS;
	double tempoS;
	bool sameS = false;

	cout << endl << "Introduzir Prova:" << endl;

	cout << endl << "Insira o estados do robo:" << endl;
	cin >> estados;
	estados = util.converterMinuscula(estados);

	do {
		cout << endl << "Insira o tempo que realizou na prova" << endl;
		cin >> tempo;
		estados = util.converterMinuscula(estados);
	} while (tempo<=0);

	if (proj.getListaProvas().size() == 0) {
		id = 1;
	}
	else {
		id = proj.getListaProvas().back().getID() + 1;
	}

	Prova prova(id, idequipa, idrobo, estados, tempo);

	if (provas.size() == 0) {

		proj.addProvas(prova);
	}
	else {

		for (int i = 0; i < provas.size(); i++) {

			estadosS = provas[i].getEstados();
			tempoS = provas[i].getTempo();

			if (estados == estadosS && tempo == tempoS) {
				sameS = true;
			}
		}

		if (sameS == false) {

			proj.addProvas(prova);
		}
		else {
			cout << endl << "A prova que pretende inserir ja existe no sistema!" << endl;
		}
	}

}

void ProjetoUI::alterarEquipa() {

	vector<Equipa> equipas = proj.getListaEquipas();
	int num, alteracao, pos = 0;

	if (equipas.size() == 0) {

		cout << endl << "Nao foram introduzidas equipas no sistema." << endl;
	}
	else {

		do {
			cout << "Selecione o numero da equipa da listada abaixo para ser alterada." << endl;
			listarEquipa();
			cin >> num;
		} while (num < 1 || num > equipas.size());

		pos = num - 1;

		do {
			cout << "O que pretende alterar na Equipa?" << endl;
			cout << "1. Nome." << endl;
			cin >> alteracao;
			cout << endl;
		} while (alteracao < 1);

		proj.alterarEquipas(pos, alteracao);
	}
}

void ProjetoUI::alterarElementos() {

	vector<Elementos> elementos = proj.getListaElementos();
	int num, alteracao, pos = 0;

	if (elementos.size() == 0) {

		cout << endl << "Nao foram introduzidas elementos no sistema." << endl;
	}
	else {

		do {
			cout << "Selecione o numero do elemento da listada abaixo para ser alterada." << endl;
			listarElementos();
			cin >> num;
		} while (num < 1 || num > elementos.size());

		pos = num - 1;

		do {
			cout << "O que pretende alterar nos elementos?" << endl;
			cout << "1. Nome." << endl;
			cout << "2. Numero." << endl;
			cout << "3. Email." << endl;
			cin >> alteracao;
			cout << endl;
		} while (alteracao < 1 || alteracao > 3);

		proj.alterarElementos(pos, alteracao);
	}
}

void ProjetoUI::alterarRobo() {

	vector<Robo> robos = proj.getListaRobos();
	int num, alteracao, pos = 0;

	if (robos.size() == 0) {

		cout << endl << "Nao foram introduzidos robos no sistema." << endl;
	}
	else {

		do {
			cout << "Selecione o numero de um robo da lista abaixo para ser alterado." << endl;
			listarRobo();
			cin >> num;
		} while (num < 1 || num > robos.size());

		pos = num - 1;

		do {
			cout << "O que pretende alterar no robo?" << endl;
			cout << "1. Nome do Robo." << endl;
			cout << "2. Modelo do Robo." << endl;
			cout << "3. Numero de sensores do robo." << endl;
			cout << "4. Melhor Tempo." << endl;
			cin >> alteracao;
			cout << endl;
		} while (alteracao < 1 || alteracao > 4);

		proj.alterarRobos(pos, alteracao);
	}
}

void ProjetoUI::alterarProva() {

	vector<Prova> provas = proj.getListaProvas();
	int num, alteracao, pos = 0;

	if (provas.size() == 0) {

		cout << endl << "Nao foram introduzidos provas no sistema." << endl;
	}
	else {

		do {
			cout << "Selecione o numero de uma prova da lista abaixo para ser alterado." << endl;
			listarProva();
			cin >> num;
		} while (num < 1 || num > provas.size());

		pos = num - 1;

		do {
			cout << "O que pretende alterar no Prova?" << endl;
			cout << "1. Estados." << endl;
			cout << "2. Tempo." << endl;
			cin >> alteracao;
			cout << endl;
		} while (alteracao < 1 || alteracao > 2);

		proj.alterarProvas(pos, alteracao);
	}
}

void ProjetoUI::removerEquipa() {

	vector<Equipa> equipas = proj.getListaEquipas();
	int num, pos = 0;

	if (equipas.size() == 0) {

		cout << endl << "Nao foram introduzidas equipas no sistema." << endl;
	}
	else {

		do {
			cout << "Selecione o numero da Equipa da lista abaixo para ser eliminada." << endl;
			listarEquipa();
			cin >> num;
		} while (num < 1 || num > equipas.size());

		pos = num - 1;

		proj.apagarEquipas(pos);
	}
}

void ProjetoUI::removerElementos() {

	vector<Elementos> elementos = proj.getListaElementos();
	int num, pos = 0;

	if (elementos.size() == 0) {

		cout << endl << "Nao foi introduzido o elemento no sistema." << endl;
	}
	else {

		do {
			cout << "Selecione o numero do Elemento da listada abaixo para ser eliminada." << endl;
			listarElementos();
			cin >> num;
		} while (num < 1 || num > elementos.size());

		pos = num - 1;

		proj.apagarElementos(pos);
	}
}

void ProjetoUI::removerRobo() {

	vector<Robo> robos = proj.getListaRobos();
	int num, pos = 0;

	if (robos.size() == 0) {

		cout << endl << "Nao foram introduzidos robos no sistema." << endl;
	}
	else {

		do {
			cout << "Selecione o numero de um robo da lista abaixo para ser eliminado." << endl;
			listarRobo();
			cin >> num;
		} while (num < 1 || num > robos.size());

		pos = num - 1;

		proj.apagarRobos(pos);
	}
}

void ProjetoUI::removerProva() {

	vector<Prova> provas = proj.getListaProvas();
	int num, pos = 0;

	if (provas.size() == 0) {

		cout << endl << "Nao foram introduzidas provas no sistema." << endl;
	}
	else {

		do {
			cout << "Selecione o numero de uma prova da lista abaixo para ser eliminado." << endl;
			listarProva();
			cin >> num;
		} while (num < 1 || num > provas.size());

		pos = num - 1;

		proj.apagarProvas(pos);
	}
}

void ProjetoUI::listarEquipa() {

	vector<Equipa> equipas = proj.getListaEquipas();
	int i = 0, id;
	string nome;

	if (equipas.size() == 0) {

		cout << endl << "Nao foram introduzidas equipas no sistema." << endl;
	}
	else {

		cout << endl << "Num:	ID	Nome" << endl;

		for (i = 0; i < equipas.size(); i++) {

			id = equipas[i].getID();
			nome = equipas[i].getNome();

			cout << (i + 1) << ":	" << id << "	" << nome << endl;
		}
	}
}

void ProjetoUI::listarElementos() {

	vector<Elementos> elementos = proj.getListaElementos();
	int i = 0, id, num;
	string nome, email;
	

	if (elementos.size() == 0) {

		cout << endl << "Nao foram introduzidos elementos no sistema." << endl;
	}
	else {

		cout << endl << "Num:	ID	Nome	Numero	Email" << endl;

		for (i = 0; i < elementos.size(); i++) {

			id = elementos[i].getID();
			nome = elementos[i].getNome();
			num = elementos[i].getNum();
			email = elementos[i].getEmail();

			cout << (i + 1) << ":	" << id << "	" << nome << "	" << num << "	" << email << endl;
		}
	}
}

void ProjetoUI::listarRobo() {

	vector<Robo> robos = proj.getListaRobos();
	int i = 0, id, num_sensor;
	string nome_do_robo, modelo;
	double melhor_tempo;

	if (robos.size() == 0) {

		cout << endl << "Nao foram introduzidos robos no sistema." << endl;
	}
	else {

		cout << endl << "Num:	ID	Nome	Modelo	Numero do Sensor	Melhor Tempo" << endl;

		for (i = 0; i < robos.size(); i++) {

			id = robos[i].getID();
			nome_do_robo = robos[i].getNome();
			modelo = robos[i].getModelo();
			num_sensor = robos[i].getNum_sensor();
			melhor_tempo = robos[i].getMelhor_tempo();

			cout << (i + 1) << ":	" << id << "	" << nome_do_robo << "	" << modelo << "	" << num_sensor << "	" << melhor_tempo << endl;
		}
	}
}

void ProjetoUI::listarProva() {

	vector<Prova> provas = proj.getListaProvas();
	int i = 0, id;
	string estados;
	double tempo;

	if (provas.size() == 0) {

		cout << endl << "Nao foram introduzidas provas no sistema." << endl;
	}
	else {

		cout << endl << "Num:	ID	Estados Tempo" << endl;

		for (i = 0; i < provas.size(); i++) {

			id = provas[i].getID();
			estados = provas[i].getEstados();
			tempo = provas[i].getTempo();

			cout << (i + 1) << ":	" << id << "	" << estados << "	" << tempo << endl;
		}
	}
}