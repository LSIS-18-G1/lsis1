#include <iostream>
#include "MenuUI.h"

using namespace std;

ProjetoUI mUI;

MenuUI::MenuUI() {

}

void MenuUI::menu() {

	int opcao1 = 0, opcao2 = 0, opcao3 = 0;

	mUI.startConnection();

	do {
		cout << endl;

		cout << "    oooooooooooooooooooooooooooo" << endl;
		cout << "    | 1 - Inserir. |" << endl;
		cout << "    | 2 - Alterar. |" << endl;
		cout << "    | 3 - Remover. |" << endl;
		cout << "    | 4 - Listar.  |" << endl;
		cout << "    | 0 - Sair.                |" << endl;
		cout << "    oooooooooooooooooooooooooooo" << endl;

		cout << endl;
		while ((cout << "Introduza uma opcao:" << endl && !(cin >> opcao1)) || (opcao1 < 0) || (opcao1 > 4) || cin.peek() != '\n')
		{
			cout << "Introduza uma opcao valida (numero inteiro de 0 a 4 inclusive)!" << endl;
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}

		switch (opcao1) {
			case 1:

				cout << endl << "Introduza o que pretende inserir." << endl;

				cout << "1 - Inserir uma equipa." << endl;
				cout << "2 - Inserir um elemento." << endl;
				cout << "3 - Inserir um robo." << endl;
				cout << "4 - Inserir uma prova." << endl;

				cout << endl;

				while ((cout << "Introduza uma opcao:" << endl && !(cin >> opcao2)) || (opcao2 < 1) || (opcao2 > 4) || cin.peek() != '\n')
				{
					cout << "Introduza uma opcao valida (numero inteiro de 1 a 4 inclusive)!" << endl;
					cin.clear();
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}

				switch (opcao2) {
				case 1:
					mUI.inserirEquipa();
					break;
				case 2:
					mUI.inserirElementos();
					break;
				case 3:
					mUI.inserirRobo();
					break;
				case 4:
					mUI.inserirProva();
					break;
				}
				break;
			case 2:

				cout << endl << "Introduza o que pretende alterar." << endl;

				cout << "1 - Alterar uma equipa." << endl;
				cout << "2 - Alterar um elemento." << endl;
				cout << "3 - Alterar um robo." << endl;
				cout << "4 - Alterar uma prova." << endl;

				cout << endl;

				while ((cout << "Introduza uma opcao:" << endl && !(cin >> opcao2)) || (opcao2 < 1) || (opcao2 > 4) || cin.peek() != '\n')
				{
					cout << "Introduza uma opcao valida (numero inteiro de 1 a 4 inclusive)!" << endl;
					cin.clear();
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}

				switch (opcao2) {
				case 1:
					mUI.alterarEquipa();
					break;
				case 2:
					mUI.alterarElementos();
					break;
				case 3:
					mUI.alterarRobo();
					break;
				case 4:
					mUI.alterarProva();
					break;
				}
				break;
			case 3:

				cout << endl << "Introduza o que pretende remover." << endl;

				cout << "1 - Remover uma equipa." << endl;
				cout << "2 - Remover um elemento." << endl;
				cout << "3 - Remover um robo." << endl;
				cout << "4 - Remover uma prova." << endl;

				cout << endl;

				while ((cout << "Introduza uma opcao:" << endl && !(cin >> opcao2)) || (opcao2 < 1) || (opcao2 > 4) || cin.peek() != '\n')
				{
					cout << "Introduza uma opcao valida (numero inteiro de 1 a 4 inclusive)!" << endl;
					cin.clear();
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}

				switch (opcao2) {
				case 1:
					mUI.removerEquipa();
					break;
				case 2:
					mUI.removerElementos();
					break;
				case 3:
					mUI.removerRobo();
					break;
				case 4:
					mUI.removerProva();
					break;
				}
				break;
			case 4:

				cout << endl << "Introduza o que pretende listar." << endl;

				cout << "1 - Listar equipas." << endl;
				cout << "2 - Listar elementos." << endl;
				cout << "3 - Listar robos." << endl;
				cout << "4 - Listar provas." << endl;

				cout << endl;

				while ((cout << "Introduza uma opcao:" << endl && !(cin >> opcao2)) || (opcao2 < 1) || (opcao2 > 4) || cin.peek() != '\n')
				{
					cout << "Introduza uma opcao valida (numero inteiro de 1 a 4 inclusive)!" << endl;
					cin.clear();
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}

				switch (opcao2) {
				case 1:
					mUI.listarEquipa();
					break;
				case 2:
					mUI.listarElementos();
					break;
				case 3:
					mUI.listarRobo();
					break;
				case 4:
					mUI.listarProva();

					while ((cout << "Introduza uma opcao:" << endl && !(cin >> opcao3)) || (opcao3 < 1) || (opcao3 > 4) || cin.peek() != '\n')
					{
						cout << "Introduza uma opcao valida (numero inteiro de 1 a 4 inclusive)!" << endl;
						cin.clear();
						cin.ignore(numeric_limits<streamsize>::max(), '\n');
					}
					break;
				}
		}
	} while (opcao1 != 0);
	
}