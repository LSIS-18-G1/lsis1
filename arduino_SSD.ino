
#include <SPI.h>
#include <SD.h>

File myFile;

int estadoActual1=0;
int estadoActual2=0;
int estadoUltimo=0;
int contador=0;
float RaioEmCM=32.1;   //raio da roda em cm
float pi=3.1416;
float perimetroRoda=2*pi*(RaioEmCM/100);  //Calcula Perimetro metros
float distPercorrida=0;
float distKM=0;
float TempoInicial=0;
float TempoFinal=0;
float DeltaTms=0;
float DeltaTh=0;
float velocidade=0;
float TempoEscrita=0;
int fecho = 1;

void setup(){
        pinMode(4,OUTPUT);
        pinMode(2,INPUT);
        Serial.begin(9600);
        SD.begin(10);
        
        myFile = SD.open("prova.txt", FILE_WRITE);

           //imprimir menu no pc
           Serial.print("Volta Numero  ");
           Serial.print(" Distancia percorrida  ");
           Serial.print(" Tempo Inicial  ");
           Serial.print(" Tempo Final  ");
           Serial.print(" Delta Tempo (ms)  ");
           Serial.print(" Delta Tempo (h)  ");
           Serial.print(" Velocidade (KmH)");
           Serial.println();

           //imprimir menu no cartão
           myFile.print("Volta Numero  ");
           myFile.print(" Distancia percorrida  ");
           myFile.print(" Tempo Inicial  ");
           myFile.print(" Tempo Final  ");
           myFile.print(" Delta Tempo (ms)  ");
           myFile.print(" Delta Tempo (h)  ");
           myFile.print(" Velocidade (KmH)");
           myFile.println();
           TempoInicial =  TempoFinal;
        
}

void loop(){

    estadoActual1=digitalRead(2);    //read se high ou how....
    TempoFinal=millis();
    
    //vericar se passou iman 
    if (estadoActual1 == LOW){   // é necessário verificar se é LOW senão coloquem HIGH
                   
            contador = contador + 1;
            
            distPercorrida=perimetroRoda*contador;          //  cálculo da distância percorrida (em metros)
            distKM=distPercorrida/1000;
         
            DeltaTms=abs(TempoFinal-TempoInicial);          //  cálculo da variação de tempo aka quanto tempo demorou a fazer essa volta
            DeltaTh=(((DeltaTms/1000.0)/60.0)/60.0);        //  conversão do tempo em horas
            velocidade=((perimetroRoda/1000)/DeltaTh);     //perimetro em metros / 1000 fica em km por hora
            
            //imprimir resultados no pc
            Serial.print("Volta ",contador,"          ");
            Serial.print(distPercorrida,"          ");
            Serial.print(TempoInicial,"          ");            
            Serial.print(TempoFinal,"          ");            
            Serial.print(DeltaTms,"          ");
            Serial.print(DeltaTh,"          ");
            Serial.print(velocidade); 
            Serial.println();


            if(TempoEscrita < 120001){

                  //imprimir resultados no cartão enquanto não tiverem passado 2 minutos
                  myFile.print("Volta ", contador,"          ");
                  myFile.print(distPercorrida,"          ");
                  myFile.print(TempoInicial,"          ");
                  myFile.print(TempoFinal,"          ");
                  myFile.print(DeltaTms,"          ");
                  myFile.print(DeltaTh,"          ");
                  myFile.print(velocidade); 
                  myFile.println();
                  
            }
            
            TempoInicial =  TempoFinal;
            TempoEscrita = TempoEscrita + DeltaTms;
            digitalWrite(4, HIGH);    //set the digital pin 4 to high!!!!
            delay(100);
    }
           
    
    digitalWrite(4, LOW);     // end the "thing" by setting it to low!!
    
    if(TempoEscrita > 120001 && fecho == 1){
          
          myFile.close();
          fecho=0;
    }
}


