#include "elementocontroller.h"

ElementoController::ElementoController(Projeto *proj, QObject *parent) : QObject(parent), m_projeto(proj)
{
    Q_ASSERT(proj != nullptr);
}

Elemento *ElementoController::novoElemento()
{
    auto novo =  m_projeto->novoElemento();
    if (novo != nullptr) {
        novo->setNomeElemento("Sem nome");
        novo->setNumeroElemento("Sem numero");
        novo->setNomeEquipaElemento("Sem equipa");
    }
    return novo;

}

bool ElementoController::eliminarElemento(Elemento *elemento)
{
    return m_projeto->eliminarElemento(elemento);
}
