#include "mainwindow.h"
#include "basedados.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Projeto m_proj;
    EquipaController EquipaController(&m_proj);
    ElementoController ElementoController(&m_proj);
    RobotController RobotController(&m_proj);
    ProvaController ProvaController(&m_proj);

	BaseDados m_BaseDados;
	m_BaseDados.inicio();

    MainWindow window(&EquipaController, &ElementoController, &RobotController, &ProvaController);
    window.show();

    return a.exec();
}
