#ifndef ELEMENTOCONTROLLER_H
#define ELEMENTOCONTROLLER_H

#include <QObject>
#include <projeto.h>

class ElementoController : public QObject
{
    Q_OBJECT
public:
    explicit ElementoController(Projeto *proj, QObject *parent = nullptr);

    Elemento *novoElemento();
    bool eliminarElemento(Elemento *elemento);

signals:

public slots:

private:
    Projeto *m_projeto;
};

#endif // ELEMENTOCONTROLLER_H
