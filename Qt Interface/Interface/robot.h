#ifndef ROBOT_H
#define ROBOT_H

#include <equipa.h>

#include <QObject>
#include <QString>
#include <QStringList>

class Robot : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString nomeRobot READ nomeRobot WRITE setNomeRobot NOTIFY nomeRobotChanged())
    Q_PROPERTY(QString modeloRobot READ modeloRobot WRITE setModeloRobot NOTIFY modeloRobotChanged())
    Q_PROPERTY(QString numeroSensorRobot READ numeroSensorRobot WRITE setNumeroSensorRobot NOTIFY numeroSensorRobotChanged())
    Q_PROPERTY(QString nomeEquipaRobot READ nomeEquipaRobot WRITE setNomeEquipaRobot NOTIFY nomeEquipaRobotChanged)

public:
    explicit Robot(QObject *parent = nullptr);

    QString nomeRobot() const;
    void setNomeRobot(const QString &nomeRobot);

    QString modeloRobot() const;
    void setModeloRobot(const QString &modeloRobot);

    QString numeroSensorRobot() const;
    void setNumeroSensorRobot(const QString &numeroSensorRobot);

    QString nomeEquipaRobot() const;
    void setNomeEquipaRobot(const QString &nomeEquipaRobot);

signals:
    void nomeRobotChanged();
    void modeloRobotChanged();
    void numeroSensorRobotChanged();
    void nomeEquipaRobotChanged();

public slots:

private:
    QString m_nomeRobot;
    QString m_modeloRobot;
    QString m_numeroSensorRobot;
    QString m_nomeEquipaRobot;
};

#endif // ROBOT_H
