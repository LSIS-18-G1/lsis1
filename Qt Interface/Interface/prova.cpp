#include "prova.h"

Prova::Prova(QObject *parent) : QObject(parent)
{

}

QString Prova::nomeProva() const
{
    return m_nomeProva;
}

void Prova::setNomeProva(const QString &nomeProva)
{
    if (m_nomeProva != nomeProva) {
        m_nomeProva = nomeProva;
        emit NomeProvaChanged();
    }
}

QString Prova::estadosProva() const
{
    return m_estadosProva;
}

void Prova::setEstadosProva(const QString &estadosProva)
{
    if (m_estadosProva != estadosProva) {
        m_estadosProva = estadosProva;
        emit EstadosProvaChanged();
    }
}

QTime Prova::tempoProva() const
{
    return m_tempoProva;
}

void Prova::setTempoProva(const QTime &tempoProva)
{
    if (m_tempoProva != tempoProva) {
        m_tempoProva = tempoProva;
        emit TempoProvaChanged();
    }
}

QDate Prova::dataProva() const
{
    return m_dataProva;
}

void Prova::setDataProva(const QDate &dataProva)
{
    if (m_dataProva != dataProva) {
        m_dataProva = dataProva;
        emit DataProvaChanged();
    }
}

QString Prova::nomeRobotProva() const
{
    return m_nomeRobotProva;
}

void Prova::setNomeRobotProva(const QString &nomeRobotProva)
{
    if (m_nomeRobotProva != nomeRobotProva) {
        m_nomeRobotProva = nomeRobotProva;
        emit NomeRobotProvaChanged();
    }
}
