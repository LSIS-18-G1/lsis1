#ifndef EQUIPA_H
#define EQUIPA_H

#include <QObject>
#include <QString>
#include <QStringList>

class Equipa : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString nomeEquipa READ nomeEquipa WRITE setNomeEquipa NOTIFY nomeEquipaChanged)

public:
    explicit Equipa(QObject *parent = nullptr);


    QString nomeEquipa() const;
    void setNomeEquipa(const QString &nomeEquipa);

signals:
    void nomeEquipaChanged();

public slots:


private:
    QString m_nomeEquipa;

};

#endif // EQUIPA_H
