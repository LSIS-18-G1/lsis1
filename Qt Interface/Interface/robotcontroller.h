#ifndef ROBOTCONTROLLER_H
#define ROBOTCONTROLLER_H

#include <QObject>
#include <projeto.h>

class RobotController : public QObject
{
    Q_OBJECT
public:
    explicit RobotController(Projeto *proj, QObject *parent = nullptr);

    Robot *novoRobot();
    bool eliminarRobot(Robot *robot);

signals:

public slots:

private:
    Projeto *m_projeto;
};
#endif // ROBOTCONTROLLER_H
