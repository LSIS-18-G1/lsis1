#pragma once

#include <projeto.h>
#include <equipa.h>
#include <elemento.h>
#include <robot.h>
#include <prova.h>

#include "ui_mainwindow.h"

#include "EquipaController.h"
#include "ElementoController.h"
#include "RobotController.h"
#include "ProvaController.h"

class BaseDados
{
public:
	
	void connect();
	void inicio();

	void getEquipas();
	void getElementos();
	void getRobots();
	void getProvas();

	void insertEquipas(QString nomeEQ); 
	void insertElementos(QString nomeElQ, QString numElQ, QString equipaElQ); 
	void insertRobots(QString nomeRQ, QString modeloRQ, QString numsensorRQ, QString equipaRQ); 
	void insertProvas(QString nomePQ, QString estadosPQ, QTime tempoPQ, QDate dataPQ, QString robotPQ); 
	
	void updateEquipas(QString nomeEQold, QString nomeEQnew);
	void updateElementos(QString nomeElQold, QString numElQold, QString equipaElQold, QString nomeElQnew, QString numElQnew, QString equipaElQnew);
	void updateRobots(QString nomeRQold, QString modeloRQold, QString numsensorRQold, QString equipaRQold, QString nomeRQnew, QString modeloRQnew, QString numsensorRQnew, QString equipaRQnew);
	void updateProvas(QString nomePQold, QString estadosPQold, QTime tempoPQold, QDate dataPQold, QString robotPQold, QString nomePQnew, QString estadosPQnew, QTime tempoPQnew, QDate dataPQnew, QString robotPQnew);

	void deleteEquipas(QString nomeEQ);
	void deleteElementos(QString nomeElQ, QString numElQ, QString equipaElQ);
	void deleteRobots(QString nomeRQ, QString modeloRQ, QString numsensorRQ, QString equipaRQ);
	void deleteProvas(QString nomePQ, QString estadosPQ, QTime tempoPQ, QDate dataPQ, QString robotPQ);

	int getIDEquipa(QString nomeEQ);
	int getIDElemento(QString nomeElQ, QString numElQ, QString equipaElQ);
	int getIDRobot(QString nomeRQ, QString modeloRQ, QString numsensorRQ, QString equipaRQ);
	int getIDProva(QString nomePQ, QString estadosPQ, QTime tempoPQ, QDate dataPQ, QString robotPQ);

private:

	Ui::MainWindow *ui;

	EquipaController *m_EquipaCtl;
	QHash<QListWidgetItem*, Equipa*> m_EquipaID;

	ElementoController *m_ElementoCtl;
	QHash<QListWidgetItem*, Elemento*> m_ElementoID;

	RobotController *m_RobotCtl;
	QHash<QListWidgetItem*, Robot*> m_RobotID;

	ProvaController *m_ProvaCtl;
	QHash<QListWidgetItem*, Prova*> m_ProvaID;

};

