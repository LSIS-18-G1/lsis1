#include "projeto.h"

Projeto::Projeto(QObject *parent) : QObject(parent)
{

}

// Equipa

Projeto::ListaEquipa Projeto::listaEquipa() const
{
    return m_listaEquipa;
}

Equipa *Projeto::novaEquipa()
{
    auto novo = new Equipa(this);
    if(novo != nullptr){
        m_listaEquipa.append(novo);
        emit equipaAdded(novo);
    }
    return novo;

}

bool Projeto::eliminarEquipa(Equipa *equipa)
{
    if (m_listaEquipa.contains(equipa)) {
            emit equipaRemoved(equipa);
            m_listaEquipa.removeOne(equipa);
            delete equipa;
            return true;
        }
        return false;
}

// Elemento

Projeto::ListaElemento Projeto::listaElemento() const {

    return m_listaElemento;
}

Elemento *Projeto::novoElemento() {

    auto novo = new Elemento(this);
    if(novo != nullptr){
        m_listaElemento.append(novo);
        emit elementoAdded(novo);
    }
    return novo;
}

bool Projeto::eliminarElemento(Elemento *elemento)
{
    if (m_listaElemento.contains(elemento)) {
            emit elementoRemoved(elemento);
            m_listaElemento.removeOne(elemento);
            delete elemento;
            return true;
        }
        return false;
}

// Prova

Projeto::ListaProva Projeto::listaProva() const
{
    return m_listaProva;
}

Prova *Projeto::novaProva()
{
    auto novo = new Prova(this);
    if(novo != nullptr){
        m_listaProva.append(novo);
        emit provaAdded(novo);
    }
    return novo;
}

bool Projeto::eliminarProva(Prova *prova)
{
    if (m_listaProva.contains(prova)) {
            emit provaRemoved(prova);
            m_listaProva.removeOne(prova);
            delete prova;
            return true;
        }
        return false;
}

// Robot

Projeto::ListaRobot Projeto::listaRobot() const
{
    return m_listaRobot;
}

Robot *Projeto::novoRobot()
{
    auto novo = new Robot(this);
    if(novo != nullptr){
        m_listaRobot.append(novo);
        emit robotAdded(novo);
    }
    return novo;
}

bool Projeto::eliminarRobot(Robot *robot)
{
    if (m_listaRobot.contains(robot)) {
            emit robotRemoved(robot);
            m_listaRobot.removeOne(robot);
            delete robot;
            return true;
        }
        return false;
}
