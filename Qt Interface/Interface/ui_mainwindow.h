/********************************************************************************
** Form generated from reading UI file 'mainwindowgfUPIe.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MAINWINDOWGFUPIE_H
#define MAINWINDOWGFUPIE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTimeEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actInsEquipa;
    QAction *actInsElemento;
    QAction *actInsRobot;
    QAction *actInsProva;
    QAction *actListEquipa;
    QAction *actListElemento;
    QAction *actListRobot;
    QAction *actListProva;
    QAction *actAltEquipa;
    QAction *actAltElemento;
    QAction *actAltRobot;
    QAction *actAltProva;
    QAction *actElimEquipa;
    QAction *actElimElemento;
    QAction *actElimRobot;
    QAction *actElimProva;
    QAction *actIniciarProva;
    QAction *actSave;
    QAction *actClose;
    QAction *actSearchData;
    QAction *actRegistarEstados;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QStackedWidget *stackedWidget;
    QWidget *Principal;
    QGridLayout *gridLayout_2;
    QLabel *Titulo;
    QLabel *Versao;
    QLabel *bot;
    QLabel *top;
    QLabel *GrupoAno;
    QWidget *InsEquipa;
    QGridLayout *gridLayout_3;
    QDialogButtonBox *InsEquipaOkCancel;
    QFormLayout *InsEquipaLayout;
    QLabel *TInsEquipaNome;
    QLineEdit *InsEquipaNome;
    QLabel *TInsEquipa;
    QWidget *AltEquipa;
    QGridLayout *gridLayout_4;
    QDialogButtonBox *AltEquipaOkCancel;
    QFormLayout *AltEquipaLayout;
    QLabel *TAltEquipaNome;
    QLineEdit *AltEquipaNome;
    QLabel *TAltEquipa;
    QWidget *ListEquipa;
    QGridLayout *gridLayout_6;
    QLabel *TListEquipa;
    QLabel *top1;
    QGridLayout *ListEquipaGridLayout;
    QListWidget *ListEquipas;
    QVBoxLayout *ListEquipaVerticalLayout;
    QPushButton *ListEquipaAlterar;
    QPushButton *ListEquipaEliminar;
    QPushButton *ListEquipaOk;
    QLabel *bot1;
    QWidget *InsElemento;
    QGridLayout *gridLayout_7;
    QGridLayout *InsElementoLayout;
    QLineEdit *InsElementoNumero;
    QLabel *TInsElementoNome;
    QLineEdit *InsElementoNome;
    QLabel *TInsElementoNumero;
    QLabel *TInsElementoEquipa;
    QComboBox *InsElementoEquipa;
    QDialogButtonBox *InsElementoOkCancel;
    QLabel *TInsElemento;
    QWidget *AltElemento;
    QGridLayout *gridLayout_8;
    QLabel *TAltElemento;
    QGridLayout *AltElementoLayout;
    QLabel *TAltElementoNome;
    QLineEdit *AltElementoNome;
    QLabel *TAltElementoNumero;
    QLineEdit *AltElementoNumero;
    QComboBox *AltElementoEquipa;
    QLabel *TAltElementoEquipa;
    QDialogButtonBox *AltElementoOkCancel;
    QWidget *ListElemento;
    QGridLayout *gridLayout_5;
    QLabel *top3;
    QLabel *TListElemento;
    QLabel *bot3;
    QGridLayout *ListElementoGridLayout;
    QListWidget *ListElementos;
    QVBoxLayout *ListElementoVerticalLayout;
    QPushButton *ListElementoAlterar;
    QPushButton *ListElementoEliminar;
    QPushButton *ListElementoOk;
    QWidget *InsRobot;
    QGridLayout *gridLayout_11;
    QLabel *TInsRobot;
    QGridLayout *InsRobotLayout;
    QLineEdit *InsRobotModelo;
    QLineEdit *InsRobotNome;
    QLabel *TInsRobotSensor;
    QLabel *TInsRobotEquipa;
    QLabel *TInsRobotModelo;
    QLabel *TInsRobotNome;
    QComboBox *InsRobotEquipa;
    QSpinBox *InsRobotSensor;
    QDialogButtonBox *InsRobotOkCancel;
    QWidget *AltRobot;
    QGridLayout *gridLayout_12;
    QLabel *TAltRobot;
    QGridLayout *AltRobotLayout;
    QLabel *TAltRobotSensor;
    QLineEdit *AltRobotNome;
    QLabel *TAltRobotModelo;
    QLabel *TAltRobotNome;
    QLineEdit *AltRobotModelo;
    QComboBox *AltRobotEquipa;
    QLabel *TAltRobotEquipa;
    QSpinBox *AltRobotSensor;
    QDialogButtonBox *AltRobotOkCancel;
    QWidget *ListRobot;
    QGridLayout *gridLayout_13;
    QLabel *TListRobot;
    QLabel *bot5;
    QLabel *top5;
    QGridLayout *ListRobotsGridLayout;
    QListWidget *ListRobots;
    QVBoxLayout *ListRobosVerticalLayout;
    QPushButton *ListRobotAlterar;
    QPushButton *ListRobotEliminar;
    QPushButton *ListRobotOk;
    QWidget *InsProva;
    QGridLayout *gridLayout_27;
    QLabel *TInsProva;
    QDialogButtonBox *InsProvaOkCancel;
    QGridLayout *InsProvaLayout;
    QComboBox *InsProvaRobot;
    QLabel *TInsProvaTempo;
    QLabel *TInsProvaEstado;
    QLabel *TInsProvaData;
    QLabel *TInsProvaRobot;
    QTimeEdit *InsProvaTempo;
    QDateEdit *InsProvaData;
    QLabel *TInsProvaNome;
    QLineEdit *InsProvaNome;
    QComboBox *InsProvaEstado;
    QWidget *AltProva;
    QGridLayout *gridLayout_28;
    QGridLayout *AltProvaLayout;
    QComboBox *AltProvaRobot;
    QLabel *TAltProvaTempo;
    QLabel *TAltProvaEstado;
    QLabel *TAltProvaData;
    QLabel *TAltProvaRobot;
    QTimeEdit *AltProvaTempo;
    QDateEdit *AltProvaData;
    QLineEdit *AltProvaNome;
    QLabel *TAltProvaNome;
    QComboBox *AltProvaEstado;
    QLabel *TAltProva;
    QDialogButtonBox *AltProvaOkCancel;
    QWidget *ListProva;
    QGridLayout *gridLayout_29;
    QLabel *bot7;
    QLabel *top7;
    QLabel *TListProva;
    QGridLayout *ListProvaGridLayout;
    QListWidget *ListProvas;
    QVBoxLayout *ListProvaVerticalLayout;
    QPushButton *ListProvaAlterar;
    QPushButton *ListProvaEliminar;
    QPushButton *ListProvaOk;
    QWidget *IniciarProva;
    QGridLayout *gridLayout_10;
    QLabel *TIniciarProva;
    QVBoxLayout *IniciarProvaVerticalLayout;
    QPushButton *IniciarProvaGuardar;
    QPushButton *IniciarProvaCancelar;
    QLabel *bot8;
    QLabel *top8;
    QStackedWidget *IniciarProvaEstados;
    QWidget *SWEstadoParado;
    QGridLayout *gridLayout_18;
    QLabel *TEstadoParado;
    QWidget *SWEstadoFrente;
    QGridLayout *gridLayout_14;
    QLabel *TEstadoFrente;
    QWidget *SWEstadoTras;
    QGridLayout *gridLayout_15;
    QLabel *TEstadoTras;
    QWidget *SWEstadoDireita;
    QGridLayout *gridLayout_16;
    QLabel *TEstadoDireita;
    QWidget *SWEstadoEsquerda;
    QGridLayout *gridLayout_17;
    QLabel *TEstadoEsquerda;
    QWidget *RegistarEstados;
    QGridLayout *gridLayout_9;
    QGridLayout *BotoesEstadosGridLayout;
    QPushButton *EstadoFrente;
    QPushButton *EstadoEsquerda;
    QPushButton *EstadoDireita;
    QPushButton *EstadoTras;
    QLabel *top9;
    QStackedWidget *RegistarEstadosEstados;
    QWidget *SWMEstadoParado;
    QGridLayout *gridLayout_19;
    QLabel *TMEstadoParado;
    QWidget *SWMEstadoFrente;
    QGridLayout *gridLayout_20;
    QLabel *TMEstadoFrente;
    QWidget *SWMEstadoTras;
    QGridLayout *gridLayout_21;
    QLabel *TMEstadoTras;
    QWidget *SWMEstadoDireita;
    QGridLayout *gridLayout_22;
    QLabel *TMEstadoDireita;
    QWidget *SWMEstadoEsquerda;
    QGridLayout *gridLayout_23;
    QLabel *TMEstadoEsquerda;
    QLabel *bot9;
    QLabel *TRegistarEstados;
    QHBoxLayout *RegistarEstadosHorizontalLayout;
    QPushButton *RegistarEstadosGuardar;
    QPushButton *RegistarEstadosCancelar;
    QLabel *meio9;
    QMenuBar *menuBar;
    QMenu *menuMenu;
    QMenu *menuInserir;
    QMenu *menuListar;
    QMenu *menuPrograma;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(588, 452);
        actInsEquipa = new QAction(MainWindow);
        actInsEquipa->setObjectName(QStringLiteral("actInsEquipa"));
        actInsElemento = new QAction(MainWindow);
        actInsElemento->setObjectName(QStringLiteral("actInsElemento"));
        actInsRobot = new QAction(MainWindow);
        actInsRobot->setObjectName(QStringLiteral("actInsRobot"));
        actInsProva = new QAction(MainWindow);
        actInsProva->setObjectName(QStringLiteral("actInsProva"));
        actListEquipa = new QAction(MainWindow);
        actListEquipa->setObjectName(QStringLiteral("actListEquipa"));
        actListElemento = new QAction(MainWindow);
        actListElemento->setObjectName(QStringLiteral("actListElemento"));
        actListRobot = new QAction(MainWindow);
        actListRobot->setObjectName(QStringLiteral("actListRobot"));
        actListProva = new QAction(MainWindow);
        actListProva->setObjectName(QStringLiteral("actListProva"));
        actAltEquipa = new QAction(MainWindow);
        actAltEquipa->setObjectName(QStringLiteral("actAltEquipa"));
        actAltElemento = new QAction(MainWindow);
        actAltElemento->setObjectName(QStringLiteral("actAltElemento"));
        actAltRobot = new QAction(MainWindow);
        actAltRobot->setObjectName(QStringLiteral("actAltRobot"));
        actAltProva = new QAction(MainWindow);
        actAltProva->setObjectName(QStringLiteral("actAltProva"));
        actElimEquipa = new QAction(MainWindow);
        actElimEquipa->setObjectName(QStringLiteral("actElimEquipa"));
        actElimElemento = new QAction(MainWindow);
        actElimElemento->setObjectName(QStringLiteral("actElimElemento"));
        actElimRobot = new QAction(MainWindow);
        actElimRobot->setObjectName(QStringLiteral("actElimRobot"));
        actElimProva = new QAction(MainWindow);
        actElimProva->setObjectName(QStringLiteral("actElimProva"));
        actIniciarProva = new QAction(MainWindow);
        actIniciarProva->setObjectName(QStringLiteral("actIniciarProva"));
        actSave = new QAction(MainWindow);
        actSave->setObjectName(QStringLiteral("actSave"));
        actClose = new QAction(MainWindow);
        actClose->setObjectName(QStringLiteral("actClose"));
        actSearchData = new QAction(MainWindow);
        actSearchData->setObjectName(QStringLiteral("actSearchData"));
        actRegistarEstados = new QAction(MainWindow);
        actRegistarEstados->setObjectName(QStringLiteral("actRegistarEstados"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        Principal = new QWidget();
        Principal->setObjectName(QStringLiteral("Principal"));
        gridLayout_2 = new QGridLayout(Principal);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        Titulo = new QLabel(Principal);
        Titulo->setObjectName(QStringLiteral("Titulo"));

        gridLayout_2->addWidget(Titulo, 1, 0, 1, 1);

        Versao = new QLabel(Principal);
        Versao->setObjectName(QStringLiteral("Versao"));

        gridLayout_2->addWidget(Versao, 4, 0, 1, 1);

        bot = new QLabel(Principal);
        bot->setObjectName(QStringLiteral("bot"));

        gridLayout_2->addWidget(bot, 3, 0, 1, 1);

        top = new QLabel(Principal);
        top->setObjectName(QStringLiteral("top"));

        gridLayout_2->addWidget(top, 0, 0, 1, 1);

        GrupoAno = new QLabel(Principal);
        GrupoAno->setObjectName(QStringLiteral("GrupoAno"));

        gridLayout_2->addWidget(GrupoAno, 2, 0, 1, 1);

        stackedWidget->addWidget(Principal);
        InsEquipa = new QWidget();
        InsEquipa->setObjectName(QStringLiteral("InsEquipa"));
        gridLayout_3 = new QGridLayout(InsEquipa);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        InsEquipaOkCancel = new QDialogButtonBox(InsEquipa);
        InsEquipaOkCancel->setObjectName(QStringLiteral("InsEquipaOkCancel"));
        InsEquipaOkCancel->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_3->addWidget(InsEquipaOkCancel, 4, 0, 1, 1);

        InsEquipaLayout = new QFormLayout();
        InsEquipaLayout->setSpacing(6);
        InsEquipaLayout->setObjectName(QStringLiteral("InsEquipaLayout"));
        TInsEquipaNome = new QLabel(InsEquipa);
        TInsEquipaNome->setObjectName(QStringLiteral("TInsEquipaNome"));

        InsEquipaLayout->setWidget(0, QFormLayout::LabelRole, TInsEquipaNome);

        InsEquipaNome = new QLineEdit(InsEquipa);
        InsEquipaNome->setObjectName(QStringLiteral("InsEquipaNome"));

        InsEquipaLayout->setWidget(0, QFormLayout::FieldRole, InsEquipaNome);


        gridLayout_3->addLayout(InsEquipaLayout, 2, 0, 1, 1);

        TInsEquipa = new QLabel(InsEquipa);
        TInsEquipa->setObjectName(QStringLiteral("TInsEquipa"));

        gridLayout_3->addWidget(TInsEquipa, 0, 0, 1, 1);

        stackedWidget->addWidget(InsEquipa);
        AltEquipa = new QWidget();
        AltEquipa->setObjectName(QStringLiteral("AltEquipa"));
        gridLayout_4 = new QGridLayout(AltEquipa);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        AltEquipaOkCancel = new QDialogButtonBox(AltEquipa);
        AltEquipaOkCancel->setObjectName(QStringLiteral("AltEquipaOkCancel"));
        AltEquipaOkCancel->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_4->addWidget(AltEquipaOkCancel, 3, 1, 1, 1);

        AltEquipaLayout = new QFormLayout();
        AltEquipaLayout->setSpacing(6);
        AltEquipaLayout->setObjectName(QStringLiteral("AltEquipaLayout"));
        TAltEquipaNome = new QLabel(AltEquipa);
        TAltEquipaNome->setObjectName(QStringLiteral("TAltEquipaNome"));

        AltEquipaLayout->setWidget(0, QFormLayout::LabelRole, TAltEquipaNome);

        AltEquipaNome = new QLineEdit(AltEquipa);
        AltEquipaNome->setObjectName(QStringLiteral("AltEquipaNome"));

        AltEquipaLayout->setWidget(0, QFormLayout::FieldRole, AltEquipaNome);


        gridLayout_4->addLayout(AltEquipaLayout, 2, 0, 1, 2);

        TAltEquipa = new QLabel(AltEquipa);
        TAltEquipa->setObjectName(QStringLiteral("TAltEquipa"));

        gridLayout_4->addWidget(TAltEquipa, 1, 0, 1, 2);

        stackedWidget->addWidget(AltEquipa);
        ListEquipa = new QWidget();
        ListEquipa->setObjectName(QStringLiteral("ListEquipa"));
        gridLayout_6 = new QGridLayout(ListEquipa);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        TListEquipa = new QLabel(ListEquipa);
        TListEquipa->setObjectName(QStringLiteral("TListEquipa"));

        gridLayout_6->addWidget(TListEquipa, 1, 0, 1, 1);

        top1 = new QLabel(ListEquipa);
        top1->setObjectName(QStringLiteral("top1"));

        gridLayout_6->addWidget(top1, 0, 0, 1, 1);

        ListEquipaGridLayout = new QGridLayout();
        ListEquipaGridLayout->setSpacing(6);
        ListEquipaGridLayout->setObjectName(QStringLiteral("ListEquipaGridLayout"));
        ListEquipas = new QListWidget(ListEquipa);
        ListEquipas->setObjectName(QStringLiteral("ListEquipas"));

        ListEquipaGridLayout->addWidget(ListEquipas, 0, 0, 1, 1);

        ListEquipaVerticalLayout = new QVBoxLayout();
        ListEquipaVerticalLayout->setSpacing(6);
        ListEquipaVerticalLayout->setObjectName(QStringLiteral("ListEquipaVerticalLayout"));
        ListEquipaAlterar = new QPushButton(ListEquipa);
        ListEquipaAlterar->setObjectName(QStringLiteral("ListEquipaAlterar"));

        ListEquipaVerticalLayout->addWidget(ListEquipaAlterar);

        ListEquipaEliminar = new QPushButton(ListEquipa);
        ListEquipaEliminar->setObjectName(QStringLiteral("ListEquipaEliminar"));

        ListEquipaVerticalLayout->addWidget(ListEquipaEliminar);

        ListEquipaOk = new QPushButton(ListEquipa);
        ListEquipaOk->setObjectName(QStringLiteral("ListEquipaOk"));

        ListEquipaVerticalLayout->addWidget(ListEquipaOk);


        ListEquipaGridLayout->addLayout(ListEquipaVerticalLayout, 0, 1, 1, 1);


        gridLayout_6->addLayout(ListEquipaGridLayout, 5, 0, 1, 1);

        bot1 = new QLabel(ListEquipa);
        bot1->setObjectName(QStringLiteral("bot1"));

        gridLayout_6->addWidget(bot1, 4, 0, 1, 1);

        stackedWidget->addWidget(ListEquipa);
        InsElemento = new QWidget();
        InsElemento->setObjectName(QStringLiteral("InsElemento"));
        gridLayout_7 = new QGridLayout(InsElemento);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        InsElementoLayout = new QGridLayout();
        InsElementoLayout->setSpacing(6);
        InsElementoLayout->setObjectName(QStringLiteral("InsElementoLayout"));
        InsElementoNumero = new QLineEdit(InsElemento);
        InsElementoNumero->setObjectName(QStringLiteral("InsElementoNumero"));

        InsElementoLayout->addWidget(InsElementoNumero, 1, 1, 1, 1);

        TInsElementoNome = new QLabel(InsElemento);
        TInsElementoNome->setObjectName(QStringLiteral("TInsElementoNome"));

        InsElementoLayout->addWidget(TInsElementoNome, 0, 0, 1, 1);

        InsElementoNome = new QLineEdit(InsElemento);
        InsElementoNome->setObjectName(QStringLiteral("InsElementoNome"));

        InsElementoLayout->addWidget(InsElementoNome, 0, 1, 1, 1);

        TInsElementoNumero = new QLabel(InsElemento);
        TInsElementoNumero->setObjectName(QStringLiteral("TInsElementoNumero"));

        InsElementoLayout->addWidget(TInsElementoNumero, 1, 0, 1, 1);

        TInsElementoEquipa = new QLabel(InsElemento);
        TInsElementoEquipa->setObjectName(QStringLiteral("TInsElementoEquipa"));

        InsElementoLayout->addWidget(TInsElementoEquipa, 2, 0, 1, 1);

        InsElementoEquipa = new QComboBox(InsElemento);
        InsElementoEquipa->setObjectName(QStringLiteral("InsElementoEquipa"));

        InsElementoLayout->addWidget(InsElementoEquipa, 2, 1, 1, 1);


        gridLayout_7->addLayout(InsElementoLayout, 1, 0, 1, 2);

        InsElementoOkCancel = new QDialogButtonBox(InsElemento);
        InsElementoOkCancel->setObjectName(QStringLiteral("InsElementoOkCancel"));
        InsElementoOkCancel->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_7->addWidget(InsElementoOkCancel, 3, 1, 1, 1);

        TInsElemento = new QLabel(InsElemento);
        TInsElemento->setObjectName(QStringLiteral("TInsElemento"));

        gridLayout_7->addWidget(TInsElemento, 0, 0, 1, 2);

        stackedWidget->addWidget(InsElemento);
        AltElemento = new QWidget();
        AltElemento->setObjectName(QStringLiteral("AltElemento"));
        gridLayout_8 = new QGridLayout(AltElemento);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        TAltElemento = new QLabel(AltElemento);
        TAltElemento->setObjectName(QStringLiteral("TAltElemento"));

        gridLayout_8->addWidget(TAltElemento, 0, 0, 1, 1);

        AltElementoLayout = new QGridLayout();
        AltElementoLayout->setSpacing(6);
        AltElementoLayout->setObjectName(QStringLiteral("AltElementoLayout"));
        TAltElementoNome = new QLabel(AltElemento);
        TAltElementoNome->setObjectName(QStringLiteral("TAltElementoNome"));

        AltElementoLayout->addWidget(TAltElementoNome, 0, 0, 1, 1);

        AltElementoNome = new QLineEdit(AltElemento);
        AltElementoNome->setObjectName(QStringLiteral("AltElementoNome"));

        AltElementoLayout->addWidget(AltElementoNome, 0, 1, 1, 1);

        TAltElementoNumero = new QLabel(AltElemento);
        TAltElementoNumero->setObjectName(QStringLiteral("TAltElementoNumero"));

        AltElementoLayout->addWidget(TAltElementoNumero, 1, 0, 1, 1);

        AltElementoNumero = new QLineEdit(AltElemento);
        AltElementoNumero->setObjectName(QStringLiteral("AltElementoNumero"));

        AltElementoLayout->addWidget(AltElementoNumero, 1, 1, 1, 1);

        AltElementoEquipa = new QComboBox(AltElemento);
        AltElementoEquipa->setObjectName(QStringLiteral("AltElementoEquipa"));

        AltElementoLayout->addWidget(AltElementoEquipa, 2, 1, 1, 1);

        TAltElementoEquipa = new QLabel(AltElemento);
        TAltElementoEquipa->setObjectName(QStringLiteral("TAltElementoEquipa"));

        AltElementoLayout->addWidget(TAltElementoEquipa, 2, 0, 1, 1);


        gridLayout_8->addLayout(AltElementoLayout, 1, 0, 1, 1);

        AltElementoOkCancel = new QDialogButtonBox(AltElemento);
        AltElementoOkCancel->setObjectName(QStringLiteral("AltElementoOkCancel"));
        AltElementoOkCancel->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_8->addWidget(AltElementoOkCancel, 2, 0, 1, 1);

        stackedWidget->addWidget(AltElemento);
        ListElemento = new QWidget();
        ListElemento->setObjectName(QStringLiteral("ListElemento"));
        gridLayout_5 = new QGridLayout(ListElemento);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        top3 = new QLabel(ListElemento);
        top3->setObjectName(QStringLiteral("top3"));

        gridLayout_5->addWidget(top3, 0, 0, 1, 1);

        TListElemento = new QLabel(ListElemento);
        TListElemento->setObjectName(QStringLiteral("TListElemento"));

        gridLayout_5->addWidget(TListElemento, 1, 0, 1, 1);

        bot3 = new QLabel(ListElemento);
        bot3->setObjectName(QStringLiteral("bot3"));

        gridLayout_5->addWidget(bot3, 2, 0, 1, 1);

        ListElementoGridLayout = new QGridLayout();
        ListElementoGridLayout->setSpacing(6);
        ListElementoGridLayout->setObjectName(QStringLiteral("ListElementoGridLayout"));
        ListElementos = new QListWidget(ListElemento);
        ListElementos->setObjectName(QStringLiteral("ListElementos"));

        ListElementoGridLayout->addWidget(ListElementos, 0, 0, 1, 1);

        ListElementoVerticalLayout = new QVBoxLayout();
        ListElementoVerticalLayout->setSpacing(6);
        ListElementoVerticalLayout->setObjectName(QStringLiteral("ListElementoVerticalLayout"));
        ListElementoAlterar = new QPushButton(ListElemento);
        ListElementoAlterar->setObjectName(QStringLiteral("ListElementoAlterar"));

        ListElementoVerticalLayout->addWidget(ListElementoAlterar);

        ListElementoEliminar = new QPushButton(ListElemento);
        ListElementoEliminar->setObjectName(QStringLiteral("ListElementoEliminar"));

        ListElementoVerticalLayout->addWidget(ListElementoEliminar);

        ListElementoOk = new QPushButton(ListElemento);
        ListElementoOk->setObjectName(QStringLiteral("ListElementoOk"));

        ListElementoVerticalLayout->addWidget(ListElementoOk);


        ListElementoGridLayout->addLayout(ListElementoVerticalLayout, 0, 1, 1, 1);


        gridLayout_5->addLayout(ListElementoGridLayout, 3, 0, 1, 1);

        stackedWidget->addWidget(ListElemento);
        InsRobot = new QWidget();
        InsRobot->setObjectName(QStringLiteral("InsRobot"));
        gridLayout_11 = new QGridLayout(InsRobot);
        gridLayout_11->setSpacing(6);
        gridLayout_11->setContentsMargins(11, 11, 11, 11);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        TInsRobot = new QLabel(InsRobot);
        TInsRobot->setObjectName(QStringLiteral("TInsRobot"));

        gridLayout_11->addWidget(TInsRobot, 0, 0, 1, 1);

        InsRobotLayout = new QGridLayout();
        InsRobotLayout->setSpacing(6);
        InsRobotLayout->setObjectName(QStringLiteral("InsRobotLayout"));
        InsRobotModelo = new QLineEdit(InsRobot);
        InsRobotModelo->setObjectName(QStringLiteral("InsRobotModelo"));

        InsRobotLayout->addWidget(InsRobotModelo, 1, 1, 1, 1);

        InsRobotNome = new QLineEdit(InsRobot);
        InsRobotNome->setObjectName(QStringLiteral("InsRobotNome"));

        InsRobotLayout->addWidget(InsRobotNome, 0, 1, 1, 1);

        TInsRobotSensor = new QLabel(InsRobot);
        TInsRobotSensor->setObjectName(QStringLiteral("TInsRobotSensor"));

        InsRobotLayout->addWidget(TInsRobotSensor, 2, 0, 1, 1);

        TInsRobotEquipa = new QLabel(InsRobot);
        TInsRobotEquipa->setObjectName(QStringLiteral("TInsRobotEquipa"));

        InsRobotLayout->addWidget(TInsRobotEquipa, 3, 0, 1, 1);

        TInsRobotModelo = new QLabel(InsRobot);
        TInsRobotModelo->setObjectName(QStringLiteral("TInsRobotModelo"));

        InsRobotLayout->addWidget(TInsRobotModelo, 1, 0, 1, 1);

        TInsRobotNome = new QLabel(InsRobot);
        TInsRobotNome->setObjectName(QStringLiteral("TInsRobotNome"));

        InsRobotLayout->addWidget(TInsRobotNome, 0, 0, 1, 1);

        InsRobotEquipa = new QComboBox(InsRobot);
        InsRobotEquipa->setObjectName(QStringLiteral("InsRobotEquipa"));

        InsRobotLayout->addWidget(InsRobotEquipa, 3, 1, 1, 1);

        InsRobotSensor = new QSpinBox(InsRobot);
        InsRobotSensor->setObjectName(QStringLiteral("InsRobotSensor"));

        InsRobotLayout->addWidget(InsRobotSensor, 2, 1, 1, 1);


        gridLayout_11->addLayout(InsRobotLayout, 1, 0, 1, 1);

        InsRobotOkCancel = new QDialogButtonBox(InsRobot);
        InsRobotOkCancel->setObjectName(QStringLiteral("InsRobotOkCancel"));
        InsRobotOkCancel->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_11->addWidget(InsRobotOkCancel, 2, 0, 1, 1);

        stackedWidget->addWidget(InsRobot);
        AltRobot = new QWidget();
        AltRobot->setObjectName(QStringLiteral("AltRobot"));
        gridLayout_12 = new QGridLayout(AltRobot);
        gridLayout_12->setSpacing(6);
        gridLayout_12->setContentsMargins(11, 11, 11, 11);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        TAltRobot = new QLabel(AltRobot);
        TAltRobot->setObjectName(QStringLiteral("TAltRobot"));

        gridLayout_12->addWidget(TAltRobot, 0, 0, 1, 1);

        AltRobotLayout = new QGridLayout();
        AltRobotLayout->setSpacing(6);
        AltRobotLayout->setObjectName(QStringLiteral("AltRobotLayout"));
        TAltRobotSensor = new QLabel(AltRobot);
        TAltRobotSensor->setObjectName(QStringLiteral("TAltRobotSensor"));

        AltRobotLayout->addWidget(TAltRobotSensor, 2, 0, 1, 1);

        AltRobotNome = new QLineEdit(AltRobot);
        AltRobotNome->setObjectName(QStringLiteral("AltRobotNome"));

        AltRobotLayout->addWidget(AltRobotNome, 0, 1, 1, 1);

        TAltRobotModelo = new QLabel(AltRobot);
        TAltRobotModelo->setObjectName(QStringLiteral("TAltRobotModelo"));

        AltRobotLayout->addWidget(TAltRobotModelo, 1, 0, 1, 1);

        TAltRobotNome = new QLabel(AltRobot);
        TAltRobotNome->setObjectName(QStringLiteral("TAltRobotNome"));

        AltRobotLayout->addWidget(TAltRobotNome, 0, 0, 1, 1);

        AltRobotModelo = new QLineEdit(AltRobot);
        AltRobotModelo->setObjectName(QStringLiteral("AltRobotModelo"));

        AltRobotLayout->addWidget(AltRobotModelo, 1, 1, 1, 1);

        AltRobotEquipa = new QComboBox(AltRobot);
        AltRobotEquipa->setObjectName(QStringLiteral("AltRobotEquipa"));

        AltRobotLayout->addWidget(AltRobotEquipa, 3, 1, 1, 1);

        TAltRobotEquipa = new QLabel(AltRobot);
        TAltRobotEquipa->setObjectName(QStringLiteral("TAltRobotEquipa"));

        AltRobotLayout->addWidget(TAltRobotEquipa, 3, 0, 1, 1);

        AltRobotSensor = new QSpinBox(AltRobot);
        AltRobotSensor->setObjectName(QStringLiteral("AltRobotSensor"));

        AltRobotLayout->addWidget(AltRobotSensor, 2, 1, 1, 1);


        gridLayout_12->addLayout(AltRobotLayout, 1, 0, 1, 1);

        AltRobotOkCancel = new QDialogButtonBox(AltRobot);
        AltRobotOkCancel->setObjectName(QStringLiteral("AltRobotOkCancel"));
        AltRobotOkCancel->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_12->addWidget(AltRobotOkCancel, 2, 0, 1, 1);

        stackedWidget->addWidget(AltRobot);
        ListRobot = new QWidget();
        ListRobot->setObjectName(QStringLiteral("ListRobot"));
        gridLayout_13 = new QGridLayout(ListRobot);
        gridLayout_13->setSpacing(6);
        gridLayout_13->setContentsMargins(11, 11, 11, 11);
        gridLayout_13->setObjectName(QStringLiteral("gridLayout_13"));
        TListRobot = new QLabel(ListRobot);
        TListRobot->setObjectName(QStringLiteral("TListRobot"));

        gridLayout_13->addWidget(TListRobot, 1, 0, 1, 1);

        bot5 = new QLabel(ListRobot);
        bot5->setObjectName(QStringLiteral("bot5"));

        gridLayout_13->addWidget(bot5, 2, 0, 1, 1);

        top5 = new QLabel(ListRobot);
        top5->setObjectName(QStringLiteral("top5"));

        gridLayout_13->addWidget(top5, 0, 0, 1, 1);

        ListRobotsGridLayout = new QGridLayout();
        ListRobotsGridLayout->setSpacing(6);
        ListRobotsGridLayout->setObjectName(QStringLiteral("ListRobotsGridLayout"));
        ListRobots = new QListWidget(ListRobot);
        ListRobots->setObjectName(QStringLiteral("ListRobots"));

        ListRobotsGridLayout->addWidget(ListRobots, 0, 0, 1, 1);

        ListRobosVerticalLayout = new QVBoxLayout();
        ListRobosVerticalLayout->setSpacing(6);
        ListRobosVerticalLayout->setObjectName(QStringLiteral("ListRobosVerticalLayout"));
        ListRobotAlterar = new QPushButton(ListRobot);
        ListRobotAlterar->setObjectName(QStringLiteral("ListRobotAlterar"));

        ListRobosVerticalLayout->addWidget(ListRobotAlterar);

        ListRobotEliminar = new QPushButton(ListRobot);
        ListRobotEliminar->setObjectName(QStringLiteral("ListRobotEliminar"));

        ListRobosVerticalLayout->addWidget(ListRobotEliminar);

        ListRobotOk = new QPushButton(ListRobot);
        ListRobotOk->setObjectName(QStringLiteral("ListRobotOk"));

        ListRobosVerticalLayout->addWidget(ListRobotOk);


        ListRobotsGridLayout->addLayout(ListRobosVerticalLayout, 0, 1, 1, 1);


        gridLayout_13->addLayout(ListRobotsGridLayout, 4, 0, 1, 1);

        stackedWidget->addWidget(ListRobot);
        InsProva = new QWidget();
        InsProva->setObjectName(QStringLiteral("InsProva"));
        gridLayout_27 = new QGridLayout(InsProva);
        gridLayout_27->setSpacing(6);
        gridLayout_27->setContentsMargins(11, 11, 11, 11);
        gridLayout_27->setObjectName(QStringLiteral("gridLayout_27"));
        TInsProva = new QLabel(InsProva);
        TInsProva->setObjectName(QStringLiteral("TInsProva"));

        gridLayout_27->addWidget(TInsProva, 0, 0, 1, 1);

        InsProvaOkCancel = new QDialogButtonBox(InsProva);
        InsProvaOkCancel->setObjectName(QStringLiteral("InsProvaOkCancel"));
        InsProvaOkCancel->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_27->addWidget(InsProvaOkCancel, 3, 0, 1, 1);

        InsProvaLayout = new QGridLayout();
        InsProvaLayout->setSpacing(6);
        InsProvaLayout->setObjectName(QStringLiteral("InsProvaLayout"));
        InsProvaRobot = new QComboBox(InsProva);
        InsProvaRobot->setObjectName(QStringLiteral("InsProvaRobot"));

        InsProvaLayout->addWidget(InsProvaRobot, 1, 1, 1, 1);

        TInsProvaTempo = new QLabel(InsProva);
        TInsProvaTempo->setObjectName(QStringLiteral("TInsProvaTempo"));

        InsProvaLayout->addWidget(TInsProvaTempo, 3, 0, 1, 1);

        TInsProvaEstado = new QLabel(InsProva);
        TInsProvaEstado->setObjectName(QStringLiteral("TInsProvaEstado"));

        InsProvaLayout->addWidget(TInsProvaEstado, 2, 0, 1, 1);

        TInsProvaData = new QLabel(InsProva);
        TInsProvaData->setObjectName(QStringLiteral("TInsProvaData"));

        InsProvaLayout->addWidget(TInsProvaData, 4, 0, 1, 1);

        TInsProvaRobot = new QLabel(InsProva);
        TInsProvaRobot->setObjectName(QStringLiteral("TInsProvaRobot"));

        InsProvaLayout->addWidget(TInsProvaRobot, 1, 0, 1, 1);

        InsProvaTempo = new QTimeEdit(InsProva);
        InsProvaTempo->setObjectName(QStringLiteral("InsProvaTempo"));

        InsProvaLayout->addWidget(InsProvaTempo, 3, 1, 1, 1);

        InsProvaData = new QDateEdit(InsProva);
        InsProvaData->setObjectName(QStringLiteral("InsProvaData"));

        InsProvaLayout->addWidget(InsProvaData, 4, 1, 1, 1);

        TInsProvaNome = new QLabel(InsProva);
        TInsProvaNome->setObjectName(QStringLiteral("TInsProvaNome"));

        InsProvaLayout->addWidget(TInsProvaNome, 0, 0, 1, 1);

        InsProvaNome = new QLineEdit(InsProva);
        InsProvaNome->setObjectName(QStringLiteral("InsProvaNome"));

        InsProvaLayout->addWidget(InsProvaNome, 0, 1, 1, 1);

        InsProvaEstado = new QComboBox(InsProva);
        InsProvaEstado->setObjectName(QStringLiteral("InsProvaEstado"));

        InsProvaLayout->addWidget(InsProvaEstado, 2, 1, 1, 1);


        gridLayout_27->addLayout(InsProvaLayout, 2, 0, 1, 1);

        stackedWidget->addWidget(InsProva);
        AltProva = new QWidget();
        AltProva->setObjectName(QStringLiteral("AltProva"));
        gridLayout_28 = new QGridLayout(AltProva);
        gridLayout_28->setSpacing(6);
        gridLayout_28->setContentsMargins(11, 11, 11, 11);
        gridLayout_28->setObjectName(QStringLiteral("gridLayout_28"));
        AltProvaLayout = new QGridLayout();
        AltProvaLayout->setSpacing(6);
        AltProvaLayout->setObjectName(QStringLiteral("AltProvaLayout"));
        AltProvaRobot = new QComboBox(AltProva);
        AltProvaRobot->setObjectName(QStringLiteral("AltProvaRobot"));

        AltProvaLayout->addWidget(AltProvaRobot, 1, 1, 1, 1);

        TAltProvaTempo = new QLabel(AltProva);
        TAltProvaTempo->setObjectName(QStringLiteral("TAltProvaTempo"));

        AltProvaLayout->addWidget(TAltProvaTempo, 3, 0, 1, 1);

        TAltProvaEstado = new QLabel(AltProva);
        TAltProvaEstado->setObjectName(QStringLiteral("TAltProvaEstado"));

        AltProvaLayout->addWidget(TAltProvaEstado, 2, 0, 1, 1);

        TAltProvaData = new QLabel(AltProva);
        TAltProvaData->setObjectName(QStringLiteral("TAltProvaData"));

        AltProvaLayout->addWidget(TAltProvaData, 4, 0, 1, 1);

        TAltProvaRobot = new QLabel(AltProva);
        TAltProvaRobot->setObjectName(QStringLiteral("TAltProvaRobot"));

        AltProvaLayout->addWidget(TAltProvaRobot, 1, 0, 1, 1);

        AltProvaTempo = new QTimeEdit(AltProva);
        AltProvaTempo->setObjectName(QStringLiteral("AltProvaTempo"));

        AltProvaLayout->addWidget(AltProvaTempo, 3, 1, 1, 1);

        AltProvaData = new QDateEdit(AltProva);
        AltProvaData->setObjectName(QStringLiteral("AltProvaData"));

        AltProvaLayout->addWidget(AltProvaData, 4, 1, 1, 1);

        AltProvaNome = new QLineEdit(AltProva);
        AltProvaNome->setObjectName(QStringLiteral("AltProvaNome"));

        AltProvaLayout->addWidget(AltProvaNome, 0, 1, 1, 1);

        TAltProvaNome = new QLabel(AltProva);
        TAltProvaNome->setObjectName(QStringLiteral("TAltProvaNome"));

        AltProvaLayout->addWidget(TAltProvaNome, 0, 0, 1, 1);

        AltProvaEstado = new QComboBox(AltProva);
        AltProvaEstado->setObjectName(QStringLiteral("AltProvaEstado"));

        AltProvaLayout->addWidget(AltProvaEstado, 2, 1, 1, 1);


        gridLayout_28->addLayout(AltProvaLayout, 1, 0, 1, 1);

        TAltProva = new QLabel(AltProva);
        TAltProva->setObjectName(QStringLiteral("TAltProva"));

        gridLayout_28->addWidget(TAltProva, 0, 0, 1, 1);

        AltProvaOkCancel = new QDialogButtonBox(AltProva);
        AltProvaOkCancel->setObjectName(QStringLiteral("AltProvaOkCancel"));
        AltProvaOkCancel->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_28->addWidget(AltProvaOkCancel, 2, 0, 1, 1);

        stackedWidget->addWidget(AltProva);
        ListProva = new QWidget();
        ListProva->setObjectName(QStringLiteral("ListProva"));
        gridLayout_29 = new QGridLayout(ListProva);
        gridLayout_29->setSpacing(6);
        gridLayout_29->setContentsMargins(11, 11, 11, 11);
        gridLayout_29->setObjectName(QStringLiteral("gridLayout_29"));
        bot7 = new QLabel(ListProva);
        bot7->setObjectName(QStringLiteral("bot7"));

        gridLayout_29->addWidget(bot7, 2, 0, 1, 1);

        top7 = new QLabel(ListProva);
        top7->setObjectName(QStringLiteral("top7"));

        gridLayout_29->addWidget(top7, 0, 0, 1, 1);

        TListProva = new QLabel(ListProva);
        TListProva->setObjectName(QStringLiteral("TListProva"));

        gridLayout_29->addWidget(TListProva, 1, 0, 1, 1);

        ListProvaGridLayout = new QGridLayout();
        ListProvaGridLayout->setSpacing(6);
        ListProvaGridLayout->setObjectName(QStringLiteral("ListProvaGridLayout"));
        ListProvas = new QListWidget(ListProva);
        ListProvas->setObjectName(QStringLiteral("ListProvas"));

        ListProvaGridLayout->addWidget(ListProvas, 0, 0, 1, 1);

        ListProvaVerticalLayout = new QVBoxLayout();
        ListProvaVerticalLayout->setSpacing(6);
        ListProvaVerticalLayout->setObjectName(QStringLiteral("ListProvaVerticalLayout"));
        ListProvaAlterar = new QPushButton(ListProva);
        ListProvaAlterar->setObjectName(QStringLiteral("ListProvaAlterar"));

        ListProvaVerticalLayout->addWidget(ListProvaAlterar);

        ListProvaEliminar = new QPushButton(ListProva);
        ListProvaEliminar->setObjectName(QStringLiteral("ListProvaEliminar"));

        ListProvaVerticalLayout->addWidget(ListProvaEliminar);

        ListProvaOk = new QPushButton(ListProva);
        ListProvaOk->setObjectName(QStringLiteral("ListProvaOk"));

        ListProvaVerticalLayout->addWidget(ListProvaOk);


        ListProvaGridLayout->addLayout(ListProvaVerticalLayout, 0, 1, 1, 1);


        gridLayout_29->addLayout(ListProvaGridLayout, 4, 0, 1, 1);

        stackedWidget->addWidget(ListProva);
        IniciarProva = new QWidget();
        IniciarProva->setObjectName(QStringLiteral("IniciarProva"));
        gridLayout_10 = new QGridLayout(IniciarProva);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        TIniciarProva = new QLabel(IniciarProva);
        TIniciarProva->setObjectName(QStringLiteral("TIniciarProva"));

        gridLayout_10->addWidget(TIniciarProva, 1, 0, 1, 2);

        IniciarProvaVerticalLayout = new QVBoxLayout();
        IniciarProvaVerticalLayout->setSpacing(6);
        IniciarProvaVerticalLayout->setObjectName(QStringLiteral("IniciarProvaVerticalLayout"));
        IniciarProvaGuardar = new QPushButton(IniciarProva);
        IniciarProvaGuardar->setObjectName(QStringLiteral("IniciarProvaGuardar"));

        IniciarProvaVerticalLayout->addWidget(IniciarProvaGuardar);

        IniciarProvaCancelar = new QPushButton(IniciarProva);
        IniciarProvaCancelar->setObjectName(QStringLiteral("IniciarProvaCancelar"));

        IniciarProvaVerticalLayout->addWidget(IniciarProvaCancelar);


        gridLayout_10->addLayout(IniciarProvaVerticalLayout, 3, 1, 1, 1);

        bot8 = new QLabel(IniciarProva);
        bot8->setObjectName(QStringLiteral("bot8"));

        gridLayout_10->addWidget(bot8, 2, 0, 1, 1);

        top8 = new QLabel(IniciarProva);
        top8->setObjectName(QStringLiteral("top8"));

        gridLayout_10->addWidget(top8, 0, 0, 1, 1);

        IniciarProvaEstados = new QStackedWidget(IniciarProva);
        IniciarProvaEstados->setObjectName(QStringLiteral("IniciarProvaEstados"));
        SWEstadoParado = new QWidget();
        SWEstadoParado->setObjectName(QStringLiteral("SWEstadoParado"));
        gridLayout_18 = new QGridLayout(SWEstadoParado);
        gridLayout_18->setSpacing(6);
        gridLayout_18->setContentsMargins(11, 11, 11, 11);
        gridLayout_18->setObjectName(QStringLiteral("gridLayout_18"));
        TEstadoParado = new QLabel(SWEstadoParado);
        TEstadoParado->setObjectName(QStringLiteral("TEstadoParado"));

        gridLayout_18->addWidget(TEstadoParado, 0, 0, 1, 1);

        IniciarProvaEstados->addWidget(SWEstadoParado);
        SWEstadoFrente = new QWidget();
        SWEstadoFrente->setObjectName(QStringLiteral("SWEstadoFrente"));
        gridLayout_14 = new QGridLayout(SWEstadoFrente);
        gridLayout_14->setSpacing(6);
        gridLayout_14->setContentsMargins(11, 11, 11, 11);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        TEstadoFrente = new QLabel(SWEstadoFrente);
        TEstadoFrente->setObjectName(QStringLiteral("TEstadoFrente"));

        gridLayout_14->addWidget(TEstadoFrente, 0, 0, 1, 1);

        IniciarProvaEstados->addWidget(SWEstadoFrente);
        SWEstadoTras = new QWidget();
        SWEstadoTras->setObjectName(QStringLiteral("SWEstadoTras"));
        gridLayout_15 = new QGridLayout(SWEstadoTras);
        gridLayout_15->setSpacing(6);
        gridLayout_15->setContentsMargins(11, 11, 11, 11);
        gridLayout_15->setObjectName(QStringLiteral("gridLayout_15"));
        TEstadoTras = new QLabel(SWEstadoTras);
        TEstadoTras->setObjectName(QStringLiteral("TEstadoTras"));

        gridLayout_15->addWidget(TEstadoTras, 0, 0, 1, 1);

        IniciarProvaEstados->addWidget(SWEstadoTras);
        SWEstadoDireita = new QWidget();
        SWEstadoDireita->setObjectName(QStringLiteral("SWEstadoDireita"));
        gridLayout_16 = new QGridLayout(SWEstadoDireita);
        gridLayout_16->setSpacing(6);
        gridLayout_16->setContentsMargins(11, 11, 11, 11);
        gridLayout_16->setObjectName(QStringLiteral("gridLayout_16"));
        TEstadoDireita = new QLabel(SWEstadoDireita);
        TEstadoDireita->setObjectName(QStringLiteral("TEstadoDireita"));

        gridLayout_16->addWidget(TEstadoDireita, 0, 0, 1, 1);

        IniciarProvaEstados->addWidget(SWEstadoDireita);
        SWEstadoEsquerda = new QWidget();
        SWEstadoEsquerda->setObjectName(QStringLiteral("SWEstadoEsquerda"));
        gridLayout_17 = new QGridLayout(SWEstadoEsquerda);
        gridLayout_17->setSpacing(6);
        gridLayout_17->setContentsMargins(11, 11, 11, 11);
        gridLayout_17->setObjectName(QStringLiteral("gridLayout_17"));
        TEstadoEsquerda = new QLabel(SWEstadoEsquerda);
        TEstadoEsquerda->setObjectName(QStringLiteral("TEstadoEsquerda"));

        gridLayout_17->addWidget(TEstadoEsquerda, 0, 0, 1, 1);

        IniciarProvaEstados->addWidget(SWEstadoEsquerda);

        gridLayout_10->addWidget(IniciarProvaEstados, 3, 0, 1, 1);

        stackedWidget->addWidget(IniciarProva);
        RegistarEstados = new QWidget();
        RegistarEstados->setObjectName(QStringLiteral("RegistarEstados"));
        gridLayout_9 = new QGridLayout(RegistarEstados);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        BotoesEstadosGridLayout = new QGridLayout();
        BotoesEstadosGridLayout->setSpacing(6);
        BotoesEstadosGridLayout->setObjectName(QStringLiteral("BotoesEstadosGridLayout"));
        BotoesEstadosGridLayout->setHorizontalSpacing(50);
        BotoesEstadosGridLayout->setVerticalSpacing(20);
        BotoesEstadosGridLayout->setContentsMargins(50, -1, 50, 0);
        EstadoFrente = new QPushButton(RegistarEstados);
        EstadoFrente->setObjectName(QStringLiteral("EstadoFrente"));

        BotoesEstadosGridLayout->addWidget(EstadoFrente, 0, 0, 1, 1);

        EstadoEsquerda = new QPushButton(RegistarEstados);
        EstadoEsquerda->setObjectName(QStringLiteral("EstadoEsquerda"));

        BotoesEstadosGridLayout->addWidget(EstadoEsquerda, 1, 0, 1, 1);

        EstadoDireita = new QPushButton(RegistarEstados);
        EstadoDireita->setObjectName(QStringLiteral("EstadoDireita"));

        BotoesEstadosGridLayout->addWidget(EstadoDireita, 1, 1, 1, 1);

        EstadoTras = new QPushButton(RegistarEstados);
        EstadoTras->setObjectName(QStringLiteral("EstadoTras"));

        BotoesEstadosGridLayout->addWidget(EstadoTras, 0, 1, 1, 1);


        gridLayout_9->addLayout(BotoesEstadosGridLayout, 6, 0, 1, 1);

        top9 = new QLabel(RegistarEstados);
        top9->setObjectName(QStringLiteral("top9"));

        gridLayout_9->addWidget(top9, 0, 0, 1, 1);

        RegistarEstadosEstados = new QStackedWidget(RegistarEstados);
        RegistarEstadosEstados->setObjectName(QStringLiteral("RegistarEstadosEstados"));
        SWMEstadoParado = new QWidget();
        SWMEstadoParado->setObjectName(QStringLiteral("SWMEstadoParado"));
        gridLayout_19 = new QGridLayout(SWMEstadoParado);
        gridLayout_19->setSpacing(6);
        gridLayout_19->setContentsMargins(11, 11, 11, 11);
        gridLayout_19->setObjectName(QStringLiteral("gridLayout_19"));
        TMEstadoParado = new QLabel(SWMEstadoParado);
        TMEstadoParado->setObjectName(QStringLiteral("TMEstadoParado"));

        gridLayout_19->addWidget(TMEstadoParado, 1, 0, 1, 1);

        RegistarEstadosEstados->addWidget(SWMEstadoParado);
        SWMEstadoFrente = new QWidget();
        SWMEstadoFrente->setObjectName(QStringLiteral("SWMEstadoFrente"));
        gridLayout_20 = new QGridLayout(SWMEstadoFrente);
        gridLayout_20->setSpacing(6);
        gridLayout_20->setContentsMargins(11, 11, 11, 11);
        gridLayout_20->setObjectName(QStringLiteral("gridLayout_20"));
        TMEstadoFrente = new QLabel(SWMEstadoFrente);
        TMEstadoFrente->setObjectName(QStringLiteral("TMEstadoFrente"));

        gridLayout_20->addWidget(TMEstadoFrente, 1, 1, 1, 1);

        RegistarEstadosEstados->addWidget(SWMEstadoFrente);
        SWMEstadoTras = new QWidget();
        SWMEstadoTras->setObjectName(QStringLiteral("SWMEstadoTras"));
        gridLayout_21 = new QGridLayout(SWMEstadoTras);
        gridLayout_21->setSpacing(6);
        gridLayout_21->setContentsMargins(11, 11, 11, 11);
        gridLayout_21->setObjectName(QStringLiteral("gridLayout_21"));
        TMEstadoTras = new QLabel(SWMEstadoTras);
        TMEstadoTras->setObjectName(QStringLiteral("TMEstadoTras"));

        gridLayout_21->addWidget(TMEstadoTras, 0, 0, 1, 1);

        RegistarEstadosEstados->addWidget(SWMEstadoTras);
        SWMEstadoDireita = new QWidget();
        SWMEstadoDireita->setObjectName(QStringLiteral("SWMEstadoDireita"));
        gridLayout_22 = new QGridLayout(SWMEstadoDireita);
        gridLayout_22->setSpacing(6);
        gridLayout_22->setContentsMargins(11, 11, 11, 11);
        gridLayout_22->setObjectName(QStringLiteral("gridLayout_22"));
        TMEstadoDireita = new QLabel(SWMEstadoDireita);
        TMEstadoDireita->setObjectName(QStringLiteral("TMEstadoDireita"));

        gridLayout_22->addWidget(TMEstadoDireita, 0, 0, 1, 1);

        RegistarEstadosEstados->addWidget(SWMEstadoDireita);
        SWMEstadoEsquerda = new QWidget();
        SWMEstadoEsquerda->setObjectName(QStringLiteral("SWMEstadoEsquerda"));
        gridLayout_23 = new QGridLayout(SWMEstadoEsquerda);
        gridLayout_23->setSpacing(6);
        gridLayout_23->setContentsMargins(11, 11, 11, 11);
        gridLayout_23->setObjectName(QStringLiteral("gridLayout_23"));
        TMEstadoEsquerda = new QLabel(SWMEstadoEsquerda);
        TMEstadoEsquerda->setObjectName(QStringLiteral("TMEstadoEsquerda"));

        gridLayout_23->addWidget(TMEstadoEsquerda, 0, 0, 1, 1);

        RegistarEstadosEstados->addWidget(SWMEstadoEsquerda);

        gridLayout_9->addWidget(RegistarEstadosEstados, 4, 0, 1, 1);

        bot9 = new QLabel(RegistarEstados);
        bot9->setObjectName(QStringLiteral("bot9"));

        gridLayout_9->addWidget(bot9, 3, 0, 1, 1);

        TRegistarEstados = new QLabel(RegistarEstados);
        TRegistarEstados->setObjectName(QStringLiteral("TRegistarEstados"));

        gridLayout_9->addWidget(TRegistarEstados, 2, 0, 1, 2);

        RegistarEstadosHorizontalLayout = new QHBoxLayout();
        RegistarEstadosHorizontalLayout->setSpacing(6);
        RegistarEstadosHorizontalLayout->setObjectName(QStringLiteral("RegistarEstadosHorizontalLayout"));
        RegistarEstadosGuardar = new QPushButton(RegistarEstados);
        RegistarEstadosGuardar->setObjectName(QStringLiteral("RegistarEstadosGuardar"));

        RegistarEstadosHorizontalLayout->addWidget(RegistarEstadosGuardar, 0, Qt::AlignHCenter);

        RegistarEstadosCancelar = new QPushButton(RegistarEstados);
        RegistarEstadosCancelar->setObjectName(QStringLiteral("RegistarEstadosCancelar"));

        RegistarEstadosHorizontalLayout->addWidget(RegistarEstadosCancelar, 0, Qt::AlignHCenter);


        gridLayout_9->addLayout(RegistarEstadosHorizontalLayout, 8, 0, 1, 1);

        meio9 = new QLabel(RegistarEstados);
        meio9->setObjectName(QStringLiteral("meio9"));

        gridLayout_9->addWidget(meio9, 7, 0, 1, 1);

        stackedWidget->addWidget(RegistarEstados);

        gridLayout->addWidget(stackedWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 588, 26));
        menuMenu = new QMenu(menuBar);
        menuMenu->setObjectName(QStringLiteral("menuMenu"));
        menuInserir = new QMenu(menuMenu);
        menuInserir->setObjectName(QStringLiteral("menuInserir"));
        menuListar = new QMenu(menuMenu);
        menuListar->setObjectName(QStringLiteral("menuListar"));
        menuPrograma = new QMenu(menuBar);
        menuPrograma->setObjectName(QStringLiteral("menuPrograma"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuMenu->menuAction());
        menuBar->addAction(menuPrograma->menuAction());
        menuMenu->addAction(menuInserir->menuAction());
        menuMenu->addAction(menuListar->menuAction());
        menuInserir->addAction(actInsEquipa);
        menuInserir->addAction(actInsElemento);
        menuInserir->addAction(actInsRobot);
        menuInserir->addAction(actInsProva);
        menuListar->addAction(actListEquipa);
        menuListar->addAction(actListElemento);
        menuListar->addAction(actListRobot);
        menuListar->addAction(actListProva);
        menuPrograma->addAction(actIniciarProva);
        menuPrograma->addAction(actRegistarEstados);
        menuPrograma->addAction(actSearchData);
        menuPrograma->addAction(actClose);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(8);
        IniciarProvaEstados->setCurrentIndex(2);
        RegistarEstadosEstados->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Interface", nullptr));
        actInsEquipa->setText(QApplication::translate("MainWindow", "Equipa", nullptr));
        actInsElemento->setText(QApplication::translate("MainWindow", "Elemento", nullptr));
        actInsRobot->setText(QApplication::translate("MainWindow", "Robot", nullptr));
        actInsProva->setText(QApplication::translate("MainWindow", "Prova", nullptr));
        actListEquipa->setText(QApplication::translate("MainWindow", "Equipa", nullptr));
        actListElemento->setText(QApplication::translate("MainWindow", "Elemento", nullptr));
        actListRobot->setText(QApplication::translate("MainWindow", "Robot", nullptr));
        actListProva->setText(QApplication::translate("MainWindow", "Prova", nullptr));
        actAltEquipa->setText(QApplication::translate("MainWindow", "Equipa", nullptr));
        actAltElemento->setText(QApplication::translate("MainWindow", "Elemento", nullptr));
        actAltRobot->setText(QApplication::translate("MainWindow", "Robot", nullptr));
        actAltProva->setText(QApplication::translate("MainWindow", "Prova", nullptr));
        actElimEquipa->setText(QApplication::translate("MainWindow", "Equipa", nullptr));
        actElimElemento->setText(QApplication::translate("MainWindow", "Elemento", nullptr));
        actElimRobot->setText(QApplication::translate("MainWindow", "Robot", nullptr));
        actElimProva->setText(QApplication::translate("MainWindow", "Prova", nullptr));
        actIniciarProva->setText(QApplication::translate("MainWindow", "Iniciar Prova", nullptr));
        actSave->setText(QApplication::translate("MainWindow", "Save", nullptr));
        actClose->setText(QApplication::translate("MainWindow", "Close", nullptr));
        actSearchData->setText(QApplication::translate("MainWindow", "Search Data", nullptr));
        actRegistarEstados->setText(QApplication::translate("MainWindow", "Registar Estados", nullptr));
        Titulo->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:22pt;\">LSIS1 - Robot Bombeiro</span></p></body></html>", nullptr));
        Versao->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
        bot->setText(QString());
        top->setText(QString());
        GrupoAno->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">Grupo 1 - 2018</span></p></body></html>", nullptr));
        TInsEquipaNome->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira o nome da Equipa:</span></p></body></html>", nullptr));
        TInsEquipa->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Inserir Equipa:</span></p></body></html>", nullptr));
        TAltEquipaNome->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere o nome da Equipa:</span></p></body></html>", nullptr));
        TAltEquipa->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Alterar Equipa:</span></p></body></html>", nullptr));
        TListEquipa->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Equipas:</span></p></body></html>", nullptr));
        top1->setText(QString());
        ListEquipaAlterar->setText(QApplication::translate("MainWindow", "Alterar", nullptr));
        ListEquipaEliminar->setText(QApplication::translate("MainWindow", "Eliminar", nullptr));
        ListEquipaOk->setText(QApplication::translate("MainWindow", "Ok", nullptr));
        bot1->setText(QString());
        TInsElementoNome->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira o nome do Aluno:</span></p></body></html>", nullptr));
        TInsElementoNumero->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira a identifica\303\247\303\243o do Aluno:</span></p></body></html>", nullptr));
        TInsElementoEquipa->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira a equipa do Aluno:</span></p></body></html>", nullptr));
        TInsElemento->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Inserir Elemento:</span></p></body></html>", nullptr));
        TAltElemento->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Alterar Elemento:</span></p></body></html>", nullptr));
        TAltElementoNome->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere o nome do Aluno:</span></p></body></html>", nullptr));
        TAltElementoNumero->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere a identifica\303\247\303\243o do Aluno:</span></p></body></html>", nullptr));
        TAltElementoEquipa->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere a equipa do Aluno:</span></p></body></html>", nullptr));
        top3->setText(QString());
        TListElemento->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Elementos:</span></p></body></html>", nullptr));
        bot3->setText(QString());
        ListElementoAlterar->setText(QApplication::translate("MainWindow", "Alterar", nullptr));
        ListElementoEliminar->setText(QApplication::translate("MainWindow", "Eliminar", nullptr));
        ListElementoOk->setText(QApplication::translate("MainWindow", "Ok", nullptr));
        TInsRobot->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Inserir Robot:</span></p></body></html>", nullptr));
        TInsRobotSensor->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira o numero de sensores do Robot:</span></p></body></html>", nullptr));
        TInsRobotEquipa->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira a equipa do Robot:</span></p></body></html>", nullptr));
        TInsRobotModelo->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira o modelo do Robot:</span></p></body></html>", nullptr));
        TInsRobotNome->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira o nome do Robot:</span></p></body></html>", nullptr));
        TAltRobot->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Alterar Robot:</span></p></body></html>", nullptr));
        TAltRobotSensor->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere o numero de sensores do Robot:</span></p></body></html>", nullptr));
        TAltRobotModelo->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere o modelo do Robot:</span></p></body></html>", nullptr));
        TAltRobotNome->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere o nome do Robot:</span></p></body></html>", nullptr));
        TAltRobotEquipa->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere a equipa do Robot:</span></p></body></html>", nullptr));
        TListRobot->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Robots:</span></p></body></html>", nullptr));
        bot5->setText(QString());
        top5->setText(QString());
        ListRobotAlterar->setText(QApplication::translate("MainWindow", "Alterar", nullptr));
        ListRobotEliminar->setText(QApplication::translate("MainWindow", "Eliminar", nullptr));
        ListRobotOk->setText(QApplication::translate("MainWindow", "Ok", nullptr));
        TInsProva->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Inserir Prova:</span></p></body></html>", nullptr));
        TInsProvaTempo->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira o tempo de Prova:</span></p></body></html>", nullptr));
        TInsProvaEstado->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira a sequencia de estados do Robot:</span></p></body></html>", nullptr));
        TInsProvaData->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira a data de realiza\303\247ao da Prova:</span></p></body></html>", nullptr));
        TInsProvaRobot->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira o robot utilizado na Prova:</span></p></body></html>", nullptr));
        TInsProvaNome->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Insira o nome da Prova:</span></p></body></html>", nullptr));
        TAltProvaTempo->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere o tempo de Prova:</span></p></body></html>", nullptr));
        TAltProvaEstado->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere a sequencia de estados do Robot:</span></p></body></html>", nullptr));
        TAltProvaData->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere a data de realiza\303\247ao da Prova:</span></p></body></html>", nullptr));
        TAltProvaRobot->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere o robot utilizado na Prova:</span></p></body></html>", nullptr));
        TAltProvaNome->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Altere o nome da Prova:</span></p></body></html>", nullptr));
        TAltProva->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Alterar Prova:</span></p></body></html>", nullptr));
        bot7->setText(QString());
        top7->setText(QString());
        TListProva->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Listar Provas:</span></p></body></html>", nullptr));
        ListProvaAlterar->setText(QApplication::translate("MainWindow", "Alterar", nullptr));
        ListProvaEliminar->setText(QApplication::translate("MainWindow", "Eliminar", nullptr));
        ListProvaOk->setText(QApplication::translate("MainWindow", "Ok", nullptr));
        TIniciarProva->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Prova:</span></p></body></html>", nullptr));
        IniciarProvaGuardar->setText(QApplication::translate("MainWindow", "Guardar Estados", nullptr));
        IniciarProvaCancelar->setText(QApplication::translate("MainWindow", "Cancelar Prova", nullptr));
        bot8->setText(QString());
        top8->setText(QString());
        TEstadoParado->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">O Programa est\303\241 \303\240</span></p><p align=\"center\"><span style=\" font-size:12pt;\">espera de receber dados</span></p><p align=\"center\"><span style=\" font-size:12pt;\">do arduino</span></p></body></html>", nullptr));
        TEstadoFrente->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">O Robot est\303\241 a andar</span></p><p align=\"center\"><span style=\" font-size:12pt;\">para a Frente</span></p></body></html>", nullptr));
        TEstadoTras->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">O Robot est\303\241 a andar</span></p><p align=\"center\"><span style=\" font-size:12pt;\">para Tr\303\241s</span></p></body></html>", nullptr));
        TEstadoDireita->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">O Robot est\303\241 a andar</span></p><p align=\"center\"><span style=\" font-size:12pt;\">para a Direita</span></p></body></html>", nullptr));
        TEstadoEsquerda->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">O Robot est\303\241 a andar</span></p><p align=\"center\"><span style=\" font-size:12pt;\">para a Esquerda</span></p></body></html>", nullptr));
        EstadoFrente->setText(QApplication::translate("MainWindow", "Frente", nullptr));
        EstadoEsquerda->setText(QApplication::translate("MainWindow", "Esquerda", nullptr));
        EstadoDireita->setText(QApplication::translate("MainWindow", "Direita", nullptr));
        EstadoTras->setText(QApplication::translate("MainWindow", "Tr\303\241s", nullptr));
        top9->setText(QString());
        TMEstadoParado->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">Assim que a prova comece, introduza os estados do robot</span></p><p align=\"center\"><span style=\" font-size:12pt;\">sempre que este altere o seu comportamento</span></p></body></html>", nullptr));
        TMEstadoFrente->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">O Robot est\303\241 a andar</span></p><p align=\"center\"><span style=\" font-size:12pt;\">para a Frente</span></p></body></html>", nullptr));
        TMEstadoTras->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">O Robot est\303\241 a andar</span></p><p align=\"center\"><span style=\" font-size:12pt;\">para Tr\303\241s</span></p></body></html>", nullptr));
        TMEstadoDireita->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">O Robot est\303\241 a andar</span></p><p align=\"center\"><span style=\" font-size:12pt;\">para a Direita</span></p></body></html>", nullptr));
        TMEstadoEsquerda->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">O Robot est\303\241 a andar</span></p><p align=\"center\"><span style=\" font-size:12pt;\">para a Esquerda</span></p></body></html>", nullptr));
        bot9->setText(QString());
        TRegistarEstados->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; text-decoration: underline;\">Menu Registar Estados Prova:</span></p></body></html>", nullptr));
        RegistarEstadosGuardar->setText(QApplication::translate("MainWindow", "Guardar Estados", nullptr));
        RegistarEstadosCancelar->setText(QApplication::translate("MainWindow", "Cancelar Prova", nullptr));
        meio9->setText(QString());
        menuMenu->setTitle(QApplication::translate("MainWindow", "Menu", nullptr));
        menuInserir->setTitle(QApplication::translate("MainWindow", "Inserir", nullptr));
        menuListar->setTitle(QApplication::translate("MainWindow", "Visualizar", nullptr));
        menuPrograma->setTitle(QApplication::translate("MainWindow", "Programa", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // MAINWINDOWGFUPIE_H
