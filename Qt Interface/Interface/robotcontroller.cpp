#include "robotcontroller.h"

RobotController::RobotController(Projeto *proj, QObject *parent) : QObject(parent), m_projeto(proj)
{
    Q_ASSERT(proj != nullptr);
}

Robot *RobotController::novoRobot()
{
    auto novo =  m_projeto->novoRobot();
    if (novo != nullptr) {
        novo->setNomeRobot("Sem nome");
        novo->setModeloRobot("Sem modelo");
        novo->setNumeroSensorRobot("Sem sensor");
        novo->setNomeEquipaRobot("Sem equipa");
    }
    return novo;

}

bool RobotController::eliminarRobot(Robot *robot)
{
    return m_projeto->eliminarRobot(robot);
}
