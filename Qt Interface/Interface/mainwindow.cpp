#include "mainwindow.h"

#include <QDebug>
#include <iostream>
#include <qexception.h>

#include <QMessageBox>

MainWindow::MainWindow(EquipaController *EquipaCtl, ElementoController *ElementoCtl, RobotController *RobotCtl, ProvaController *ProvaCtl, QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow), m_EquipaCtl(EquipaCtl), m_ElementoCtl(ElementoCtl), m_RobotCtl(RobotCtl), m_ProvaCtl(ProvaCtl)
{

	ui->setupUi(this);

	const QString qtVersion = QLatin1String(QT_VERSION_STR);
	ui->Versao->setText("Qt Creator versao: " + qtVersion);

	ui->stackedWidget->setCurrentWidget(ui->Principal);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::back() {
	ui->stackedWidget->setCurrentWidget(ui->Principal);
	ui->menuBar->setEnabled(true);
}

// Programa

void MainWindow::on_actSearchData_triggered() {
	// POR DADOS DA BASE DE DADOS NOS VETORES

	//m_BaseDados.getEquipas();
	//m_BaseDados.getElementos();
	//m_BaseDados.getRobots();
	//m_BaseDados.getProvas();
	ui->actSearchData->setEnabled(false);
}

void MainWindow::on_actClose_triggered() {
	this->close();
}

// Registar Estados

void MainWindow::on_actRegistarEstados_triggered() {

	ui->stackedWidget->setCurrentWidget(ui->RegistarEstados);
	ui->RegistarEstadosEstados->setCurrentWidget(ui->SWMEstadoParado);

	ui->menuBar->setEnabled(false);

}

void MainWindow::on_EstadoFrente_clicked() {

	ui->RegistarEstadosEstados->setCurrentWidget(ui->SWMEstadoFrente);

	Estados = Estados + "F-";

}

void MainWindow::on_EstadoTras_clicked() {

	ui->RegistarEstadosEstados->setCurrentWidget(ui->SWMEstadoTras);

	Estados = Estados + "T-";

}

void MainWindow::on_EstadoDireita_clicked() {

	ui->RegistarEstadosEstados->setCurrentWidget(ui->SWMEstadoDireita);

	Estados = Estados + "D-";

}

void MainWindow::on_EstadoEsquerda_clicked() {

	ui->RegistarEstadosEstados->setCurrentWidget(ui->SWMEstadoEsquerda);

	Estados = Estados + "E-";

}

void MainWindow::on_RegistarEstadosGuardar_clicked() {

	int pos = Estados.lastIndexOf(QChar('-'));

	ui->InsProvaEstado->addItem(Estados.left(pos));
	ui->AltProvaEstado->addItem(Estados.left(pos));

	Estados = "";

	back();

}

void MainWindow::on_RegistarEstadosCancelar_clicked() {

	Estados = "";

	back();
}

// Iniciar Recolha de dados da Prova

void MainWindow::on_actIniciarProva_triggered() {

	ui->stackedWidget->setCurrentWidget(ui->IniciarProva);
	ui->IniciarProvaEstados->setCurrentWidget(ui->SWEstadoParado);

	ui->menuBar->setEnabled(false);

}

void MainWindow::on_IniciarProvaCancelar_clicked() {

	back();
}

// Equipa:

//Inserir
void MainWindow::on_actInsEquipa_triggered() {

	ui->stackedWidget->setCurrentWidget(ui->InsEquipa);
	ui->menuBar->setEnabled(false);

	auto equipa = m_EquipaCtl->novaEquipa();

	if (equipa) {
		ui->ListEquipas->addItem(equipa->nomeEquipa());
		ui->InsElementoEquipa->addItem(equipa->nomeEquipa());
		ui->AltElementoEquipa->addItem(equipa->nomeEquipa());
		ui->InsRobotEquipa->addItem(equipa->nomeEquipa());
		ui->AltRobotEquipa->addItem(equipa->nomeEquipa());
		auto ID = ui->ListEquipas->item(ui->ListEquipas->count() - 1);
		m_EquipaID.insert(ID, equipa);
	}
	ui->ListEquipas->setCurrentRow(ui->ListEquipas->count() - 1);

}

void MainWindow::on_InsEquipaOkCancel_accepted() {

	auto equipaLista = ui->ListEquipas->currentItem();
	if (equipaLista) {
		auto equipa = m_EquipaID.value(equipaLista);

		if (equipa) {
			int indexInsElemento = ui->InsElementoEquipa->findText(equipa->nomeEquipa());
			int indexAltElemento = ui->AltElementoEquipa->findText(equipa->nomeEquipa());
			int indexInsRobot = ui->InsRobotEquipa->findText(equipa->nomeEquipa());
			int indexAltRobot = ui->AltRobotEquipa->findText(equipa->nomeEquipa());

			bool same = false;
			
			int quantidade = ui->ListEquipas->count();

			for (int i = 0; i < quantidade; i++) {

				QString nome = ui->InsEquipaNome->text();

				//if(nome == "") {
				//	nome = "Sem nome";
				//}

				ui->ListEquipas->setCurrentRow(i);
				auto equipaListai = ui->ListEquipas->currentItem();
				auto equipai = m_EquipaID.value(equipaListai);

				if (nome == equipai->nomeEquipa()) {
					same = true;
				}
			}

			if (same == false) {

				if (ui->InsEquipaNome->text().isEmpty()) {
					equipa->setNomeEquipa(equipa->nomeEquipa());
				}
				else {
					equipa->setNomeEquipa(ui->InsEquipaNome->text());
				}

				m_BaseDados.insertEquipas(equipa->nomeEquipa());  // Inserir equipa base de dados

				equipaLista->setText(equipa->nomeEquipa());
				ui->InsElementoEquipa->setItemText(indexInsElemento, equipa->nomeEquipa());
				ui->AltElementoEquipa->setItemText(indexAltElemento, equipa->nomeEquipa());
				ui->InsRobotEquipa->setItemText(indexInsRobot, equipa->nomeEquipa());
				ui->AltRobotEquipa->setItemText(indexAltRobot, equipa->nomeEquipa());

			}
			else {

				if (m_EquipaCtl->eliminarEquipa(equipa)) {
					m_EquipaID.remove(equipaLista);
					delete equipaLista;

					ui->InsElementoEquipa->removeItem(indexInsElemento);
					ui->AltElementoEquipa->removeItem(indexAltElemento);
					ui->InsRobotEquipa->removeItem(indexInsRobot);
					ui->AltRobotEquipa->removeItem(indexAltRobot);
				}

				QMessageBox messageBox;
				messageBox;
				messageBox.setWindowTitle("Erro!");
				messageBox.setText("A equipa nao foi introduzida porque ja existe uma equipa com o nome '" + ui->InsEquipaNome->text() + "'.");
				messageBox.exec();
			}

			ui->InsEquipaNome->clear();
			back();
		}
	}
}

void MainWindow::on_InsEquipaOkCancel_rejected() {

	auto equipaLista = ui->ListEquipas->currentItem();
	if (equipaLista) {
		auto equipa = m_EquipaID.value(equipaLista);
		if (equipa) {
			int indexInsElemento = ui->InsElementoEquipa->findText(equipa->nomeEquipa());
			int indexAltElemento = ui->AltElementoEquipa->findText(equipa->nomeEquipa());
			int indexInsRobot = ui->InsRobotEquipa->findText(equipa->nomeEquipa());
			int indexAltRobot = ui->AltRobotEquipa->findText(equipa->nomeEquipa());

			if (m_EquipaCtl->eliminarEquipa(equipa)) {
				m_EquipaID.remove(equipaLista);
				delete equipaLista;

				ui->InsElementoEquipa->removeItem(indexInsElemento);
				ui->AltElementoEquipa->removeItem(indexAltElemento);
				ui->InsRobotEquipa->removeItem(indexInsRobot);
				ui->AltRobotEquipa->removeItem(indexAltRobot);

			}
		}
	}
	ui->InsEquipaNome->clear();
	back();
}

//Listar
void MainWindow::on_actListEquipa_triggered() {

	ui->stackedWidget->setCurrentWidget(ui->ListEquipa);
	ui->menuBar->setEnabled(false);
}

void MainWindow::on_ListEquipaOk_clicked() {
	back();
}

//Alterar
void MainWindow::on_ListEquipaAlterar_clicked() {

	ui->stackedWidget->setCurrentWidget(ui->AltEquipa);
	ui->menuBar->setEnabled(false);

	auto equipaLista = ui->ListEquipas->currentItem();
	if (equipaLista) {
		auto equipa = m_EquipaID.value(equipaLista);
		if (equipa) {
			ui->stackedWidget->setCurrentWidget(ui->AltEquipa);
			ui->AltEquipaNome->setPlaceholderText(equipa->nomeEquipa());
		}
	}
}

void MainWindow::on_AltEquipaOkCancel_accepted() {

	auto equipaLista = ui->ListEquipas->currentItem();
	if (equipaLista) {
		auto equipa = m_EquipaID.value(equipaLista);
		if (equipa) {

			QString nomeEQold = equipa->nomeEquipa();
			QString nomeEQnew;
			int indexInsElemento = ui->InsElementoEquipa->findText(equipa->nomeEquipa());
			int indexAltElemento = ui->AltElementoEquipa->findText(equipa->nomeEquipa());
			int indexInsRobot = ui->InsRobotEquipa->findText(equipa->nomeEquipa());
			int indexAltRobot = ui->AltRobotEquipa->findText(equipa->nomeEquipa());

			bool same = false;

			int quantidade = ui->ListEquipas->count();

			for (int i = 0; i < quantidade; i++) {

				QString nome = ui->AltEquipaNome->text();

				//if(nome == "") {
				//	nome = "Sem nome";
				//}

				ui->ListEquipas->setCurrentRow(i);
				auto equipaListai = ui->ListEquipas->currentItem();
				auto equipai = m_EquipaID.value(equipaListai);

				if (nome == equipai->nomeEquipa()) {
					same = true;
				}
			}

			if (same == false) {

				if (ui->AltEquipaNome->text().isEmpty()) {
					equipa->setNomeEquipa(equipa->nomeEquipa());
					nomeEQnew = equipa->nomeEquipa();
				}
				else {
					equipa->setNomeEquipa(ui->AltEquipaNome->text());
					nomeEQnew = ui->AltEquipaNome->text();
				}

				m_BaseDados.updateEquipas(nomeEQold, nomeEQnew);  // Alterar equipa base de dados

				equipaLista->setText(equipa->nomeEquipa());

				ui->InsElementoEquipa->setItemText(indexInsElemento, equipa->nomeEquipa());
				ui->AltElementoEquipa->setItemText(indexAltElemento, equipa->nomeEquipa());
				ui->InsRobotEquipa->setItemText(indexInsRobot, equipa->nomeEquipa());
				ui->AltRobotEquipa->setItemText(indexAltRobot, equipa->nomeEquipa());
			
			}
			else {

				QMessageBox messageBox;
				messageBox;
				messageBox.setWindowTitle("Erro!");
				messageBox.setText("A equipa nao foi alterada porque ja existe uma equipa com o nome '" + ui->AltEquipaNome->text() + "'.");
				messageBox.exec();
			}

			ui->AltEquipaNome->clear();
			back();
		}
	}
}

void MainWindow::on_AltEquipaOkCancel_rejected() {
	ui->AltEquipaNome->clear();
	back();
}

//Eliminar
void MainWindow::on_ListEquipaEliminar_clicked() {

	auto equipaLista = ui->ListEquipas->currentItem();
	if (equipaLista) {
		auto equipa = m_EquipaID.value(equipaLista);
		if (equipa) {
			int indexInsElemento = ui->InsElementoEquipa->findText(equipa->nomeEquipa());
			int indexAltElemento = ui->AltElementoEquipa->findText(equipa->nomeEquipa());
			int indexInsRobot = ui->InsRobotEquipa->findText(equipa->nomeEquipa());
			int indexAltRobot = ui->AltRobotEquipa->findText(equipa->nomeEquipa());

			if (m_EquipaCtl->eliminarEquipa(equipa)) {
				m_EquipaID.remove(equipaLista);
				delete equipaLista;

				ui->InsElementoEquipa->removeItem(indexInsElemento);
				ui->AltElementoEquipa->removeItem(indexAltElemento);
				ui->InsRobotEquipa->removeItem(indexInsRobot);
				ui->AltRobotEquipa->removeItem(indexAltRobot);

			}

			m_BaseDados.deleteEquipas(equipa->nomeEquipa()); // Eliminar Equipa Base de Dados
		}
	}
}

//Elemento :

//Inserir
void MainWindow::on_actInsElemento_triggered() {

	ui->stackedWidget->setCurrentWidget(ui->InsElemento);
	ui->menuBar->setEnabled(false);

	auto elemento = m_ElementoCtl->novoElemento();

	if (elemento) {
		ui->ListElementos->addItem(elemento->nomeElemento());
		auto ID = ui->ListElementos->item(ui->ListElementos->count() - 1);
		m_ElementoID.insert(ID, elemento);
	}
	ui->ListElementos->setCurrentRow(ui->ListElementos->count() - 1);

}

void MainWindow::on_InsElementoOkCancel_accepted() {

	auto elementoLista = ui->ListElementos->currentItem();
	if (elementoLista) {
		auto elemento = m_ElementoID.value(elementoLista);

		if (elemento) {

			bool same = false;

			int quantidade = ui->ListElementos->count();

			for (int i = 0; i < quantidade; i++) {

				QString numero = ui->InsElementoNumero->text();

				//if(numero == "") {
				//	numero = "Sem numero";
				//}

				ui->ListElementos->setCurrentRow(i);
				auto elementoListai = ui->ListElementos->currentItem();
				auto elementoi = m_ElementoID.value(elementoListai);

				if (numero == elementoi->numeroElemento()) {
					same = true;
				}
			}

			if (same == false) {

				if (ui->InsElementoNome->text().isEmpty()) {
					elemento->setNomeElemento(elemento->nomeElemento());
				}
				else {
					elemento->setNomeElemento(ui->InsElementoNome->text());
				}
				if (ui->InsElementoNumero->text().isEmpty()) {
					elemento->setNumeroElemento(elemento->numeroElemento());
				}
				else {
					elemento->setNumeroElemento(ui->InsElementoNumero->text());
				}
				if (ui->InsElementoEquipa->currentText().isEmpty()) {
					elemento->setNomeEquipaElemento(elemento->nomeEquipaElemento());
				}
				else {
					elemento->setNomeEquipaElemento(ui->InsElementoEquipa->currentText());
				}

				m_BaseDados.insertElementos(elemento->nomeElemento(), elemento->numeroElemento(), elemento->nomeEquipaElemento()); // Inserir elemento base de dados

				elementoLista->setText(elemento->nomeElemento());
			}
			else {

				if (m_ElementoCtl->eliminarElemento(elemento)) {
					m_ElementoID.remove(elementoLista);
					delete elementoLista;

				}	
				
				QMessageBox messageBox;
				messageBox;
				messageBox.setWindowTitle("Erro!");
				messageBox.setText("O elemento nao foi inserido porque ja existe um elemento com o a identificašao '" + ui->AltElementoNumero->text() + "'.");
				messageBox.exec();
			}

			ui->InsElementoNome->clear();
			ui->InsElementoNumero->clear();
			back();

		}
	}
}

void MainWindow::on_InsElementoOkCancel_rejected() {

	auto elementoLista = ui->ListElementos->currentItem();
	if (elementoLista) {
		auto elemento = m_ElementoID.value(elementoLista);
		if (elemento) {

			if (m_ElementoCtl->eliminarElemento(elemento)) {
				m_ElementoID.remove(elementoLista);
				delete elementoLista;

			}
		}
	}
	ui->InsElementoNome->clear();
	ui->InsElementoNumero->clear();
	back();
}

//Listar
void MainWindow::on_actListElemento_triggered() {

	ui->stackedWidget->setCurrentWidget(ui->ListElemento);
	ui->menuBar->setEnabled(false);

}

void MainWindow::on_ListElementoOk_clicked() {
	back();
}

//Alterar
void MainWindow::on_ListElementoAlterar_clicked() {

	ui->stackedWidget->setCurrentWidget(ui->AltElemento);
	ui->menuBar->setEnabled(false);

	auto elementoLista = ui->ListElementos->currentItem();
	if (elementoLista) {
		auto elemento = m_ElementoID.value(elementoLista);
		if (elemento) {
			ui->stackedWidget->setCurrentWidget(ui->AltElemento);
			ui->AltElementoNome->setPlaceholderText(elemento->nomeElemento());
			ui->AltElementoNumero->setPlaceholderText(elemento->numeroElemento());
		}
	}
}

void MainWindow::on_AltElementoOkCancel_accepted() {

	auto elementoLista = ui->ListElementos->currentItem();
	if (elementoLista) {
		auto elemento = m_ElementoID.value(elementoLista);
		if (elemento) {

			QString nomeElQold = elemento->nomeElemento();
			QString numElQold = elemento->numeroElemento();
			QString equipaElQold = elemento->nomeEquipaElemento();
			QString nomeElQnew, numElQnew, equipaElQnew;

			bool same = false;

			int quantidade = ui->ListElementos->count();

			for (int i = 0; i < quantidade; i++) {

				QString numero = ui->AltElementoNumero->text();

				//if(numero == "") {
				//	numero = "Sem numero";
				//}

				ui->ListElementos->setCurrentRow(i);
				auto elementoListai = ui->ListElementos->currentItem();
				auto elementoi = m_ElementoID.value(elementoListai);

				if (numero == elementoi->numeroElemento()) {
					same = true;
				}
			}

			if (same == false) {

				if (ui->AltElementoNome->text().isEmpty()) {
					elemento->setNomeElemento(elemento->nomeElemento());
					nomeElQnew = elemento->nomeElemento();
				}
				else {
					elemento->setNomeElemento(ui->AltElementoNome->text());
					nomeElQnew = ui->AltElementoNome->text();
				}
				if (ui->AltElementoNumero->text().isEmpty()) {
					elemento->setNumeroElemento(elemento->numeroElemento());
					numElQnew = elemento->numeroElemento();
				}
				else {
					elemento->setNumeroElemento(ui->AltElementoNumero->text());
					numElQnew = ui->AltElementoNumero->text();
				}
				if (ui->AltElementoEquipa->currentText().isEmpty()) {
					elemento->setNomeEquipaElemento(elemento->nomeEquipaElemento());
					equipaElQnew = elemento->nomeEquipaElemento();
				}
				else {
					elemento->setNomeEquipaElemento(ui->AltElementoEquipa->currentText());
					equipaElQnew = ui->AltElementoEquipa->currentText();
				}

				//m_BaseDados.updateElementos(nomeElQold, numElQold, equipaElQold, nomeElQnew, numElQnew, equipaElQnew);  // Alterar elemento base de dados

				elementoLista->setText(elemento->nomeElemento());

			}
			else {

				QMessageBox messageBox;
				messageBox;
				messageBox.setWindowTitle("Erro!");
				messageBox.setText("O elemento nao foi alterado porque ja existe um elemento com o a identificašao '" + ui->AltElementoNumero->text() + "'.");
				messageBox.exec();
			}

			ui->AltElementoNome->clear();
			ui->AltElementoNumero->clear();
			back();
		}
	}
}

void MainWindow::on_AltElementoOkCancel_rejected() {
	ui->AltElementoNome->clear();
	ui->AltElementoNumero->clear();
	back();
}

//Eliminar
void MainWindow::on_ListElementoEliminar_clicked() {

	auto elementoLista = ui->ListElementos->currentItem();
	if (elementoLista) {
		auto elemento = m_ElementoID.value(elementoLista);
		if (elemento) {

			if (m_ElementoCtl->eliminarElemento(elemento)) {
				m_ElementoID.remove(elementoLista);
				delete elementoLista;

			}

			//m_BaseDados.deleteElementos(elemento->nomeElemento(), elemento->numeroElemento(), elemento->nomeEquipaElemento());  // Eliminar Elementos Base de Dados

		}
	}

	//qDebug() << "ELIMINA";
	//QListWidgetItem *elementoLista;
	//try {
	//	if (ui->ListElementos->count() > 0) {
	//		elementoLista = ui->ListElementos->currentItem();

	//		//if (elementoLista) {
	//		Elemento * elemento = m_ElementoID.value(elementoLista);
	//		if (elemento) {

	//			if (m_ElementoCtl->eliminarElemento(elemento)) {
	//				m_ElementoID.remove(elementoLista);
	//				delete elementoLista;
	//				qDebug() << "on_AltElementoOkCancel_rejected";
	//				m_BaseDados.deleteElementos(elemento->nomeElemento(), elemento->numeroElemento(), elemento->nomeEquipaElemento());  // Eliminar Elementos Base de Dados
	//			}
	//		}
	//		//}
	//	}
	//	else { qDebug() << "Lista Vazia"; return; }
	//}
	//catch (QException & e) {
	//	qDebug() << e.what();
	//}
	//qDebug() << "elemento " << elementoLista->text();
}

//Robot :

//Inserir
void MainWindow::on_actInsRobot_triggered() {

	ui->stackedWidget->setCurrentWidget(ui->InsRobot);
	ui->menuBar->setEnabled(false);

	auto robot = m_RobotCtl->novoRobot();

	if (robot) {
		ui->ListRobots->addItem(robot->nomeRobot());
		ui->InsProvaRobot->addItem(robot->nomeRobot());
		ui->AltProvaRobot->addItem(robot->nomeRobot());
		auto ID = ui->ListRobots->item(ui->ListRobots->count() - 1);
		m_RobotID.insert(ID, robot);
	}
	ui->ListRobots->setCurrentRow(ui->ListRobots->count() - 1);
}

void MainWindow::on_InsRobotOkCancel_accepted() {

	auto robotLista = ui->ListRobots->currentItem();
	if (robotLista) {
		auto robot = m_RobotID.value(robotLista);

		if (robot) {
			int indexInsProva = ui->InsProvaRobot->findText(robot->nomeRobot());
			int indexAltProva = ui->AltProvaRobot->findText(robot->nomeRobot());

			bool same = false;

			int quantidade = ui->ListRobots->count();

			for (int i = 0; i < quantidade; i++) {

				QString nome = ui->InsRobotNome->text();

				//if(nome == "") {
				//	nome = "Sem nome";
				//}

				ui->ListRobots->setCurrentRow(i);
				auto robotListai = ui->ListRobots->currentItem();
				auto roboti = m_RobotID.value(robotListai);

				if (nome == roboti->nomeRobot()) {
					same = true;
				}
			}

			if (same == false) {

				if (ui->InsRobotNome->text().isEmpty()) {
					robot->setNomeRobot(robot->nomeRobot());
				}
				else {
					robot->setNomeRobot(ui->InsRobotNome->text());
				}
				if (ui->InsRobotModelo->text().isEmpty()) {
					robot->setModeloRobot(robot->modeloRobot());
				}
				else {
					robot->setModeloRobot(ui->InsRobotModelo->text());
				}
				if (ui->InsRobotSensor->text().isEmpty()) {
					robot->setNumeroSensorRobot(robot->numeroSensorRobot());
				}
				else {
					robot->setNumeroSensorRobot(ui->InsRobotSensor->text());
				}
				if (ui->InsRobotEquipa->currentText().isEmpty()) {
					robot->setNomeEquipaRobot(robot->nomeEquipaRobot());
				}
				else {
					robot->setNomeEquipaRobot(ui->InsRobotEquipa->currentText());
				}

				m_BaseDados.insertRobots(robot->nomeRobot(), robot->modeloRobot(), robot->numeroSensorRobot(), robot->nomeEquipaRobot()); // Inserir robot base de dados

				robotLista->setText(robot->nomeRobot());

				ui->InsProvaRobot->setItemText(indexInsProva, robot->nomeRobot());
				ui->AltProvaRobot->setItemText(indexAltProva, robot->nomeRobot());

			}
			else {

				if (m_RobotCtl->eliminarRobot(robot)) {
					m_RobotID.remove(robotLista);
					delete robotLista;

					ui->InsProvaRobot->removeItem(indexInsProva);
					ui->AltProvaRobot->removeItem(indexAltProva);

				}

				QMessageBox messageBox;
				messageBox;
				messageBox.setWindowTitle("Erro!");
				messageBox.setText("O robot nao foi introduzido porque ja existe uma robot com o nome '" + ui->InsRobotNome->text() + "'.");
				messageBox.exec();
			}

			ui->InsRobotNome->clear();
			ui->InsRobotModelo->clear();
			ui->InsRobotSensor->clear();

			back();
		}
	}
}

void MainWindow::on_InsRobotOkCancel_rejected() {

	auto robotLista = ui->ListRobots->currentItem();
	if (robotLista) {
		auto robot = m_RobotID.value(robotLista);
		if (robot) {
			int indexInsProva = ui->InsProvaRobot->findText(robot->nomeRobot());
			int indexAltProva = ui->AltProvaRobot->findText(robot->nomeRobot());

			if (m_RobotCtl->eliminarRobot(robot)) {
				m_RobotID.remove(robotLista);
				delete robotLista;

				ui->InsProvaRobot->removeItem(indexInsProva);
				ui->AltProvaRobot->removeItem(indexAltProva);
			}
		}
	}
	ui->InsRobotNome->clear();
	ui->InsRobotModelo->clear();
	ui->InsRobotSensor->clear();
	back();
}

//Listar
void MainWindow::on_actListRobot_triggered() {

	ui->stackedWidget->setCurrentWidget(ui->ListRobot);
	ui->menuBar->setEnabled(false);

}

void MainWindow::on_ListRobotOk_clicked() {
	back();
}

//Alterar
void MainWindow::on_ListRobotAlterar_clicked() {

	ui->stackedWidget->setCurrentWidget(ui->AltRobot);
	ui->menuBar->setEnabled(false);

	auto robotLista = ui->ListRobots->currentItem();
	if (robotLista) {
		auto robot = m_RobotID.value(robotLista);
		if (robot) {
			ui->stackedWidget->setCurrentWidget(ui->AltRobot);
			ui->AltRobotNome->setPlaceholderText(robot->nomeRobot());
			ui->AltRobotModelo->setPlaceholderText(robot->modeloRobot());
			ui->AltRobotSensor->setValue(robot->numeroSensorRobot().toInt());
		}
	}
}

void MainWindow::on_AltRobotOkCancel_accepted() {

	auto robotLista = ui->ListRobots->currentItem();
	if (robotLista) {
		auto robot = m_RobotID.value(robotLista);
		if (robot) {

			QString nomeRQold = robot->nomeRobot();
			QString modeloRQold = robot->modeloRobot();
			QString numsensorRQold = robot->numeroSensorRobot();
			QString equipaRQold = robot->nomeEquipaRobot();
			QString nomeRQnew, modeloRQnew, numsensorRQnew, equipaRQnew;
			int indexInsProva = ui->InsProvaRobot->findText(robot->nomeRobot());
			int indexAltProva = ui->AltProvaRobot->findText(robot->nomeRobot());

			bool same = false;

			int quantidade = ui->ListRobots->count();

			for (int i = 0; i < quantidade; i++) {

				QString nome = ui->AltRobotNome->text();

				//if(nome == "") {
				//	nome = "Sem nome";
				//}

				ui->ListRobots->setCurrentRow(i);
				auto robotListai = ui->ListRobots->currentItem();
				auto roboti = m_RobotID.value(robotListai);

				if (nome == roboti->nomeRobot()) {
					same = true;
				}
			}

			if (same == false) {

				if (ui->AltRobotNome->text().isEmpty()) {
					robot->setNomeRobot(robot->nomeRobot());
					nomeRQnew = robot->nomeRobot();
				}
				else {
					robot->setNomeRobot(ui->AltRobotNome->text());
					nomeRQnew = ui->AltRobotNome->text();
				}
				if (ui->AltRobotModelo->text().isEmpty()) {
					robot->setModeloRobot(robot->modeloRobot());
					modeloRQnew = robot->modeloRobot();
				}
				else {
					robot->setModeloRobot(ui->AltRobotModelo->text());
					modeloRQnew = ui->AltRobotModelo->text();
				}
				if (ui->AltRobotSensor->text().isEmpty()) {
					robot->setNumeroSensorRobot(robot->numeroSensorRobot());
					numsensorRQnew = robot->numeroSensorRobot();
				}
				else {
					robot->setNumeroSensorRobot(ui->AltRobotSensor->text());
					numsensorRQnew = ui->AltRobotSensor->text();
				}
				if (ui->AltRobotEquipa->currentText().isEmpty()) {
					robot->setNomeEquipaRobot(robot->nomeEquipaRobot());
					equipaRQnew = robot->nomeEquipaRobot();
				}
				else {
					robot->setNomeEquipaRobot(ui->AltRobotEquipa->currentText());
					equipaRQnew = ui->AltRobotEquipa->currentText();
				}

				//m_BaseDados.updateRobots(nomeRQold, modeloRQold, numsensorRQold, equipaRQold, nomeRQnew, modeloRQnew, numsensorRQnew, equipaRQnew);  // Alterar Fobot Base de Dados

				robotLista->setText(robot->nomeRobot());

				ui->InsProvaRobot->setItemText(indexInsProva, robot->nomeRobot());
				ui->AltProvaRobot->setItemText(indexAltProva, robot->nomeRobot());

			}
			else {

				QMessageBox messageBox;
				messageBox;
				messageBox.setWindowTitle("Erro!");
				messageBox.setText("O robot nao foi alterado porque ja existe um robot com o nome '" + ui->AltRobotNome->text() + "'.");
				messageBox.exec();
			}

			ui->AltRobotNome->clear();
			ui->AltRobotModelo->clear();
			ui->AltRobotSensor->clear();
			back();
		}
	}
}

void MainWindow::on_AltRobotOkCancel_rejected() {
	ui->AltRobotNome->clear();
	ui->AltRobotModelo->clear();
	ui->AltRobotSensor->clear();
	back();
}

//Eliminar
void MainWindow::on_ListRobotEliminar_clicked() {

	auto robotLista = ui->ListRobots->currentItem();
	if (robotLista) {
		auto robot = m_RobotID.value(robotLista);
		if (robot) {
			int indexInsProva = ui->InsProvaRobot->findText(robot->nomeRobot());
			int indexAltProva = ui->AltProvaRobot->findText(robot->nomeRobot());

			if (m_RobotCtl->eliminarRobot(robot)) {
				m_RobotID.remove(robotLista);
				delete robotLista;
				
				ui->InsProvaRobot->removeItem(indexInsProva);
				ui->AltProvaRobot->removeItem(indexAltProva);

				//m_BaseDados.deleteRobots(robot->nomeRobot(), robot->modeloRobot(), robot->numeroSensorRobot(), robot->nomeEquipaRobot());  // Eliminar Robot Base de Dados
			}
		}
	}

	//qDebug() << "ELIMINA";
	//QListWidgetItem *robotLista;
	//try {
	//	if (ui->ListRobots->count() > 0) {
	//		robotLista = ui->ListRobots->currentItem();

	//		//if (robotLista) {
	//		Robot * robot = m_RobotID.value(robotLista);
	//		if (robot) {

	//			if (m_RobotCtl->eliminarRobot(robot)) {
	//				m_RobotID.remove(robotLista);
	//				delete robotLista;
	//				qDebug() << "on_AltRobotOkCancel_rejected";
	//				m_BaseDados.deleteRobots(robot->nomeRobot(), robot->modeloRobot(), robot->numeroSensorRobot(), robot->nomeEquipaRobot());  // Eliminar Robots Base de Dados
	//			}
	//		}
	//		//}
	//	}
	//	else { qDebug() << "Lista Vazia"; return; }
	//}
	//catch (QException & e) {
	//	qDebug() << e.what();
	//}
	//qDebug() << "robot" << robotLista->text();
}


//Prova :

//Inserir
void MainWindow::on_actInsProva_triggered() {

	ui->stackedWidget->setCurrentWidget(ui->InsProva);
	ui->menuBar->setEnabled(false);

	auto prova = m_ProvaCtl->novaProva();

	if (prova) {
		ui->ListProvas->addItem(prova->nomeProva());

		auto ID = ui->ListProvas->item(ui->ListProvas->count() - 1);
		m_ProvaID.insert(ID, prova);
	}
	ui->ListProvas->setCurrentRow(ui->ListProvas->count() - 1);

}

void MainWindow::on_InsProvaOkCancel_accepted() {

	auto provaLista = ui->ListProvas->currentItem();
	if (provaLista) {
		auto prova = m_ProvaID.value(provaLista);

		if (prova) {

			bool same = false;

			int quantidade = ui->ListProvas->count();

			for (int i = 0; i < quantidade; i++) {

				QString nome = ui->InsProvaNome->text();

				//if(nome == "") {
				//	nome = "Sem nome";
				//}

				ui->ListProvas->setCurrentRow(i);
				auto provaListai = ui->ListProvas->currentItem();
				auto provai = m_ProvaID.value(provaListai);

				if (nome == provai->nomeProva()) {
					same = true;
				}
			}

			if (same == false) {

				if (ui->InsProvaNome->text().isEmpty()) {
					prova->setNomeProva(prova->nomeProva());
				}
				else {
					prova->setNomeProva(ui->InsProvaNome->text());
				}
				if (ui->InsProvaEstado->currentText().isEmpty()) {
					prova->setEstadosProva(prova->estadosProva());
				}
				else {
					prova->setEstadosProva(ui->InsProvaEstado->currentText());
					int indexInsEstado = ui->InsProvaEstado->findText(ui->InsProvaEstado->currentText());
					ui->InsProvaEstado->removeItem(indexInsEstado);
					//int indexAltEstado = ui->AltProvaEstado->findText(ui->InsProvaEstado->currentText());
					//ui->AltProvaEstado->removeItem(indexAltEstado);
				}

				prova->setDataProva(ui->InsProvaData->date());

				prova->setTempoProva(ui->InsProvaTempo->time());

				if (ui->InsProvaRobot->currentText().isEmpty()) {
					prova->setNomeRobotProva(prova->nomeRobotProva());
				}
				else {
					prova->setNomeRobotProva(ui->InsProvaRobot->currentText());
				}

				m_BaseDados.insertProvas(prova->nomeProva(), prova->estadosProva(), prova->tempoProva(), prova->dataProva(), prova->nomeRobotProva()); // Inserir Prova Base de dados

				provaLista->setText(prova->nomeProva());

			}
			else {

				if (m_ProvaCtl->eliminarProva(prova)) {
					m_ProvaID.remove(provaLista);
					delete provaLista;

					//m_BaseDados.deleteProvas(prova->nomeProva(), prova->estadosProva(), prova->tempoProva(), prova->dataProva(), prova->nomeRobotProva());  // Eliminar Prova Base de Dados
				}

				QMessageBox messageBox;
				messageBox;
				messageBox.setWindowTitle("Erro!");
				messageBox.setText("A prova nao foi introduzida porque ja existe uma prova com o nome '" + ui->InsProvaNome->text() + "'.");
				messageBox.exec();
			}

			ui->InsProvaNome->clear();
			ui->InsProvaData->clear();
			ui->InsProvaTempo->clear();

			back();	
		}
	}
}

void MainWindow::on_InsProvaOkCancel_rejected() {

	auto provaLista = ui->ListProvas->currentItem();
	if (provaLista) {
		auto prova = m_ProvaID.value(provaLista);
		if (prova) {

			if (m_ProvaCtl->eliminarProva(prova)) {
				m_ProvaID.remove(provaLista);
				delete provaLista;
			}
		}
	}
	ui->InsProvaNome->clear();
	ui->InsProvaData->clear();
	ui->InsProvaTempo->clear();
	back();
}

//Listar
void MainWindow::on_actListProva_triggered() {

	ui->stackedWidget->setCurrentWidget(ui->ListProva);
	ui->menuBar->setEnabled(false);

}

void MainWindow::on_ListProvaOk_clicked() {
	back();
}

//Alterar
void MainWindow::on_ListProvaAlterar_clicked() {

	ui->stackedWidget->setCurrentWidget(ui->AltProva);
	ui->menuBar->setEnabled(false);

	auto provaLista = ui->ListProvas->currentItem();
	if (provaLista) {
		auto prova = m_ProvaID.value(provaLista);
		if (prova) {
			ui->stackedWidget->setCurrentWidget(ui->AltProva);
			ui->AltProvaNome->setPlaceholderText(prova->nomeProva());
			ui->AltProvaData->setDate(prova->dataProva());
			ui->AltProvaTempo->setTime(prova->tempoProva());
		}
	}
}

void MainWindow::on_AltProvaOkCancel_accepted() {

	auto provaLista = ui->ListProvas->currentItem();
	if (provaLista) {
		auto prova = m_ProvaID.value(provaLista);
		if (prova) {

			QString nomePQold = prova->nomeProva();
			QString estadosPQold = prova->estadosProva();
			QTime tempoPQold = prova->tempoProva();
			QDate dataPQold = prova->dataProva();
			QString robotPQold = prova->nomeRobotProva();
			QString nomePQnew, estadosPQnew, robotPQnew;
			QTime tempoPQnew;
			QDate dataPQnew;

			bool same = false;

			int quantidade = ui->ListProvas->count();

			for (int i = 0; i < quantidade; i++) {

				QString nome = ui->AltProvaNome->text();

				//if(nome == "") {
				//	nome = "Sem nome";
				//}

				ui->ListProvas->setCurrentRow(i);
				auto provaListai = ui->ListProvas->currentItem();
				auto provai = m_ProvaID.value(provaListai);

				if (nome == provai->nomeProva()) {
					same = true;
				}
			}

			if (same == false) {

				if (ui->AltProvaNome->text().isEmpty()) {
					prova->setNomeProva(prova->nomeProva());
					nomePQnew = prova->nomeProva();
				}
				else {
					prova->setNomeProva(ui->AltProvaNome->text());
					nomePQnew = ui->AltProvaNome->text();
				}
				if (ui->AltProvaEstado->currentText().isEmpty()) {
					prova->setEstadosProva(prova->estadosProva());
					estadosPQnew = prova->estadosProva();
				}
				else {
					prova->setEstadosProva(ui->AltProvaEstado->currentText());
					estadosPQnew = ui->AltProvaEstado->currentText();
				}

				prova->setDataProva(ui->AltProvaData->date());
				dataPQnew = ui->AltProvaData->date();

				prova->setTempoProva(ui->AltProvaTempo->time());
				tempoPQnew = ui->AltProvaTempo->time();

				if (ui->AltProvaRobot->currentText().isEmpty()) {
					prova->setNomeRobotProva(prova->nomeRobotProva());
					robotPQnew = prova->nomeRobotProva();
				}
				else {
					prova->setNomeRobotProva(ui->AltProvaRobot->currentText());
					robotPQnew = ui->AltProvaRobot->currentText();
				}

				//m_BaseDados.updateProvas(nomePQold, estadosPQold, tempoPQold, dataPQold, robotPQold, nomePQnew, estadosPQnew, tempoPQnew, dataPQnew, robotPQnew);  // Alterar Prova Base de Dados

				provaLista->setText(prova->nomeProva());

				if (estadosPQnew != estadosPQold) {

					int indexInsEstado = ui->InsProvaEstado->findText(estadosPQold);
					ui->InsProvaEstado->removeItem(indexInsEstado);
					int indexAltEstado = ui->AltProvaEstado->findText(estadosPQold);
					ui->AltProvaEstado->removeItem(indexAltEstado);
					int indexInsEstadoN = ui->InsProvaEstado->findText(estadosPQnew);
					ui->InsProvaEstado->removeItem(indexInsEstadoN);
				}

			}
			else {

				QMessageBox messageBox;
				messageBox;
				messageBox.setWindowTitle("Erro!");
				messageBox.setText("A prova nao foi alterada porque ja existe uma prova com o nome '" + ui->AltProvaNome->text() + "'.");
				messageBox.exec();
			}

			ui->AltProvaNome->clear();
			ui->AltProvaData->clear();
			ui->AltProvaTempo->clear();
			back();
		}
	}
}

void MainWindow::on_AltProvaOkCancel_rejected() {
	ui->AltProvaNome->clear();
	ui->AltProvaData->clear();
	ui->AltProvaTempo->clear();
	back();
}

//Eliminar
void MainWindow::on_ListProvaEliminar_clicked() {

	auto provaLista = ui->ListProvas->currentItem();
	if (provaLista) {
		auto prova = m_ProvaID.value(provaLista);
		if (prova) {

			if (m_ProvaCtl->eliminarProva(prova)) {
				m_ProvaID.remove(provaLista);
				delete provaLista;

				//m_BaseDados.deleteProvas(prova->nomeProva(), prova->estadosProva(), prova->tempoProva(), prova->dataProva(), prova->nomeRobotProva());  // Eliminar Prova Base de Dados
			}
		}
	}

	//qDebug() << "ELIMINA";
	//QListWidgetItem *provaLista;
	//try {
	//	if (ui->ListProvas->count() > 0) {
	//		provaLista = ui->ListProvas->currentItem();

	//		//if (provaLista) {
	//		Prova * prova = m_ProvaID.value(provaLista);
	//		if (prova) {

	//			if (m_ProvaCtl->eliminarProva(prova)) {
	//				m_ProvaID.remove(provaLista);
	//				delete provaLista;
	//				qDebug() << "on_AltProvaOkCancel_rejected";
	//				m_BaseDados.deleteProvas(prova->nomeProva(), prova->estadosProva(), prova->tempoProva(), prova->dataProva(), prova->nomeRobotProva());  // Eliminar Provas Base de Dados
	//			}
	//		}
	//		//}
	//	}
	//	else { qDebug() << "Lista Vazia"; return; }
	//}
	//catch (QException & e) {
	//	qDebug() << e.what();
	//}
	//qDebug() << "prova" << provaLista->text();
}
