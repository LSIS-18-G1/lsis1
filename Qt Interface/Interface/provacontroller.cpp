#include "provacontroller.h"

ProvaController::ProvaController(Projeto *proj, QObject *parent) : QObject(parent), m_projeto(proj)
{
    Q_ASSERT(proj != nullptr);
}

Prova *ProvaController::novaProva()
{
    auto novo =  m_projeto->novaProva();
    if (novo != nullptr) {
        novo->setNomeProva("Sem nome");
        novo->setEstadosProva("Sem estados");
        //novo->setTempoProva("Sem tempo");
        //novo->setDataProva("Sem data");
        novo->setNomeRobotProva("Sem robot");
    }
    return novo;

}

bool ProvaController::eliminarProva(Prova *prova)
{
    return m_projeto->eliminarProva(prova);
}
