#ifndef ELEMENTO_H
#define ELEMENTO_H

#include <QObject>
#include <QString>
#include <QStringList>

class Elemento : public QObject
{

    Q_OBJECT
    Q_PROPERTY(QString nomeElemento READ nomeElemento WRITE setNomeElemento NOTIFY nomeElementoChanged)
    Q_PROPERTY(QString numeroElemento READ numeroElemento WRITE setNumeroElemento NOTIFY numeroElementoChanged)
    Q_PROPERTY(QString nomeEquipaElemento READ nomeEquipaElemento WRITE setNomeEquipaElemento NOTIFY nomeEquipaElementoChanged)

public:
    explicit Elemento(QObject *parent = nullptr);

    QString nomeElemento() const;
    void setNomeElemento(const QString &nomeElemento);

    QString numeroElemento() const;
    void setNumeroElemento(const QString &numeroElemento);

    QString nomeEquipaElemento() const;
    void setNomeEquipaElemento(const QString &nomeEquipaElemento);

signals:

    void nomeElementoChanged();
    void numeroElementoChanged();
    void nomeEquipaElementoChanged();

    public slots:

private:
    QString m_nomeElemento;
    QString m_numeroElemento;
    QString m_nomeEquipaElemento;

};


#endif // ELEMENTO_H
