#ifndef PROJETO_H
#define PROJETO_H

#include <QObject>
#include <QList>
#include <equipa.h>
#include <elemento.h>
#include <prova.h>
#include <robot.h>

class Projeto : public QObject
{
    Q_OBJECT
public:

    explicit Projeto(QObject *parent = nullptr);

    // Equipa
    typedef QList<Equipa*> ListaEquipa;
    ListaEquipa listaEquipa() const;
    Equipa* novaEquipa();
    bool eliminarEquipa(Equipa *equipa);

    // Elemento
    typedef QList<Elemento*> ListaElemento;
    ListaElemento listaElemento() const;
    Elemento* novoElemento();
    bool eliminarElemento(Elemento *elemento);

    // Prova
    typedef QList<Prova*> ListaProva;
    ListaProva listaProva() const;
    Prova* novaProva();
    bool eliminarProva(Prova *prova);

    // Robot
    typedef QList<Robot*> ListaRobot;
    ListaRobot listaRobot() const;
    Robot* novoRobot();
    bool eliminarRobot(Robot *robot);

signals:
    void equipaAdded(Equipa *equipa);
    void equipaRemoved(Equipa *equipa);

    void elementoAdded(Elemento *elemento);
    void elementoRemoved(Elemento *elemento);

    void provaAdded(Prova *prova);
    void provaRemoved(Prova *prova);

    void robotAdded(Robot *robot);
    void robotRemoved(Robot *robot);

public slots:
private:
    ListaEquipa m_listaEquipa;
    ListaElemento m_listaElemento;
    ListaProva m_listaProva;
    ListaRobot m_listaRobot;
};

#endif // PROJETO_H
