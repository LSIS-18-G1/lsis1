#ifndef PROVA_H
#define PROVA_H

#include <QObject>
#include <QString>
#include <QTime>
#include <QDate>
#include <QStringList>

class Prova : public QObject
{
    Q_OBJECT
        Q_PROPERTY(QString nomeProva READ nomeProva WRITE setNomeProva NOTIFY NomeProvaChanged)
        Q_PROPERTY(QString estadosProva READ estadosProva WRITE setEstadosProva NOTIFY EstadosProvaChanged)
        Q_PROPERTY(QTime tempoProva READ tempoProva WRITE setTempoProva NOTIFY TempoProvaChanged)
        Q_PROPERTY(QDate dataProva READ dataProva WRITE setDataProva NOTIFY DataProvaChanged)
        Q_PROPERTY(QString nomeRobotProva READ nomeRobotProva WRITE setNomeRobotProva NOTIFY NomeRobotProvaChanged)


public:
    explicit Prova(QObject *parent = nullptr);

    QString nomeProva() const;
    void setNomeProva(const QString &nomeProva);

    QString estadosProva() const;
    void setEstadosProva(const QString &estadosProva);

    QTime tempoProva() const;
    void setTempoProva(const QTime &tempoProva);

    QDate dataProva() const;
    void setDataProva(const QDate &dataProva);

    QString nomeRobotProva() const;
    void setNomeRobotProva(const QString &nomeRobotProva);

signals:

    void NomeProvaChanged();
    void EstadosProvaChanged();
    void TempoProvaChanged();
    void DataProvaChanged();
    void NomeRobotProvaChanged();

public slots:

private:
    QString m_nomeProva;
    QString m_estadosProva;
    QTime m_tempoProva;
    QDate m_dataProva;
    QString m_nomeRobotProva;

};

#endif // PROVA_H
