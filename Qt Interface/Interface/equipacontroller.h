#ifndef EQUIPACONTROLLER_H
#define EQUIPACONTROLLER_H

#include <QObject>
#include <projeto.h>

class EquipaController : public QObject
{
    Q_OBJECT
public:
    explicit EquipaController(Projeto *proj, QObject *parent = nullptr);

    Equipa *novaEquipa();
    bool eliminarEquipa(Equipa *equipa);

signals:

public slots:

private:
    Projeto *m_projeto;
};

#endif // EQUIPASCONTROLLER_H
