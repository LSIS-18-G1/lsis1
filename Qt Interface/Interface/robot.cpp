#include "robot.h"

Robot::Robot(QObject *parent) : QObject(parent)
{

}

QString Robot::nomeRobot() const
{
    return m_nomeRobot;
}

void Robot::setNomeRobot(const QString &nomeRobot)
{
    if(m_nomeRobot != nomeRobot){
    m_nomeRobot = nomeRobot;
    emit nomeRobotChanged();
    }
}

QString Robot::modeloRobot() const
{
    return m_modeloRobot;
}

void Robot::setModeloRobot(const QString &modeloRobot)
{
    if(m_modeloRobot != modeloRobot){
    m_modeloRobot = modeloRobot;
    emit modeloRobotChanged();
    }
}

QString Robot::numeroSensorRobot() const
{
    return m_numeroSensorRobot;
}

void Robot::setNumeroSensorRobot(const QString &numeroSensorRobot)
{
    if(m_numeroSensorRobot != numeroSensorRobot){
    m_numeroSensorRobot = numeroSensorRobot;
    emit numeroSensorRobotChanged();
    }
}

QString Robot::nomeEquipaRobot() const
{
    return m_nomeEquipaRobot;
}

void Robot::setNomeEquipaRobot(const QString &nomeEquipaRobot)
{
    if (m_nomeEquipaRobot != nomeEquipaRobot) {
        m_nomeEquipaRobot = nomeEquipaRobot;
        emit nomeEquipaRobotChanged();
    }
}


