#include "equipacontroller.h"

EquipaController::EquipaController(Projeto *proj, QObject *parent) : QObject(parent), m_projeto(proj)
{
    Q_ASSERT(proj != nullptr);
}

Equipa *EquipaController::novaEquipa()
{
    auto novo =  m_projeto->novaEquipa();
    if (novo != nullptr) {
        novo->setNomeEquipa("Sem nome");
    }
    return novo;

}

bool EquipaController::eliminarEquipa(Equipa *equipa)
{
    return m_projeto->eliminarEquipa(equipa);
}
