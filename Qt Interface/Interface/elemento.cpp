#include "elemento.h"

Elemento::Elemento(QObject *parent) : QObject(parent)
{

}

QString Elemento::nomeElemento() const
{
    return m_nomeElemento;
}

void Elemento::setNomeElemento(const QString &nomeElemento)
{
    if (m_nomeElemento != nomeElemento) {
        m_nomeElemento = nomeElemento;
        emit nomeElementoChanged();
    }
}

QString Elemento::numeroElemento() const
{
    return m_numeroElemento;
}

void Elemento::setNumeroElemento(const QString &numeroElemento)
{
    if (m_numeroElemento != numeroElemento) {
        m_numeroElemento = numeroElemento;
        emit numeroElementoChanged();
    }
}

QString Elemento::nomeEquipaElemento() const
{
    return m_nomeEquipaElemento;
}

void Elemento::setNomeEquipaElemento(const QString &nomeEquipaElemento)
{
    if (m_nomeEquipaElemento != nomeEquipaElemento) {
        m_nomeEquipaElemento = nomeEquipaElemento;
        emit nomeEquipaElementoChanged();
    }
}
