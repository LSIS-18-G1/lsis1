#ifndef PROVACONTROLLER_H
#define PROVACONTROLLER_H


#include <QObject>
#include <projeto.h>

class ProvaController : public QObject
{
    Q_OBJECT
public:
    explicit ProvaController(Projeto *proj, QObject *parent = nullptr);

    Prova *novaProva();
    bool eliminarProva(Prova *prova);

signals:

public slots:

private:
    Projeto *m_projeto;
};
#endif // PROVACONTROLLER_H
