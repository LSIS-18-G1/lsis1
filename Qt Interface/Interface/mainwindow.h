#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHash>
#include <QString>
#include <QListWidget>
#include <QListWidgetItem>

#include "Projeto.h"
#include "BaseDados.h"

#include "EquipaController.h"
#include "ElementoController.h"
#include "RobotController.h"
#include "ProvaController.h"

#include "ui_mainwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(EquipaController *EquipCtl, ElementoController *ElementoCtl, RobotController *RobotCtl, ProvaController *ProvaCtl, QWidget *parent = 0);
    ~MainWindow();


private slots:

    void back();

    // Programa
    void on_actSearchData_triggered();
    void on_actClose_triggered();

	// Registar Estados

	void on_actRegistarEstados_triggered();
	void on_EstadoFrente_clicked();
	void on_EstadoTras_clicked();
	void on_EstadoDireita_clicked();
	void on_EstadoEsquerda_clicked();
	void on_RegistarEstadosGuardar_clicked();
	void on_RegistarEstadosCancelar_clicked();

	// Iniciar Recolha de dadods da Prova

	void on_actIniciarProva_triggered();
	void on_IniciarProvaCancelar_clicked();

    // Equipa

        // Inserir
        void on_actInsEquipa_triggered();
        void on_InsEquipaOkCancel_accepted();
        void on_InsEquipaOkCancel_rejected();

        //Listar
        void on_actListEquipa_triggered();
        void on_ListEquipaOk_clicked();

        //Alterar
        void on_ListEquipaAlterar_clicked();
        void on_AltEquipaOkCancel_accepted();
        void on_AltEquipaOkCancel_rejected();

        //Eliminar
        void on_ListEquipaEliminar_clicked();

    // Elemento

        // Inserir
        void on_actInsElemento_triggered();
        void on_InsElementoOkCancel_accepted();
        void on_InsElementoOkCancel_rejected();

        //Listar
        void on_actListElemento_triggered();
        void on_ListElementoOk_clicked();

        //Alterar
        void on_ListElementoAlterar_clicked();
        void on_AltElementoOkCancel_accepted();
        void on_AltElementoOkCancel_rejected();

        //Eliminar
        void on_ListElementoEliminar_clicked();

    // Robot

        // Inserir
        void on_actInsRobot_triggered();
        void on_InsRobotOkCancel_accepted();
        void on_InsRobotOkCancel_rejected();

        //Listar
        void on_actListRobot_triggered();
        void on_ListRobotOk_clicked();

        //Alterar
        void on_ListRobotAlterar_clicked();
        void on_AltRobotOkCancel_accepted();
        void on_AltRobotOkCancel_rejected();

        //Eliminar
        void on_ListRobotEliminar_clicked();

    // Prova

        // Inserir
        void on_actInsProva_triggered();
        void on_InsProvaOkCancel_accepted();
        void on_InsProvaOkCancel_rejected();

        //Listar
        void on_actListProva_triggered();
        void on_ListProvaOk_clicked();

        //Alterar
        void on_ListProvaAlterar_clicked();
        void on_AltProvaOkCancel_accepted();
        void on_AltProvaOkCancel_rejected();

        //Eliminar
        void on_ListProvaEliminar_clicked();


private:

    Ui::MainWindow *ui;

	QString Estados;

	Projeto m_projeto;

	BaseDados m_BaseDados;

    EquipaController *m_EquipaCtl;
    QHash<QListWidgetItem*, Equipa*> m_EquipaID;

    ElementoController *m_ElementoCtl;
    QHash<QListWidgetItem*, Elemento*> m_ElementoID;

    RobotController *m_RobotCtl;
    QHash<QListWidgetItem*, Robot*> m_RobotID;

    ProvaController *m_ProvaCtl;
    QHash<QListWidgetItem*, Prova*> m_ProvaID;
};

#endif // MAINWINDOW_H
