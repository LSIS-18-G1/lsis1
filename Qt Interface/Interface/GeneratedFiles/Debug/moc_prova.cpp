/****************************************************************************
** Meta object code from reading C++ file 'prova.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../prova.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'prova.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Prova_t {
    QByteArrayData data[12];
    char stringdata0[160];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Prova_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Prova_t qt_meta_stringdata_Prova = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Prova"
QT_MOC_LITERAL(1, 6, 16), // "NomeProvaChanged"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 19), // "EstadosProvaChanged"
QT_MOC_LITERAL(4, 44, 17), // "TempoProvaChanged"
QT_MOC_LITERAL(5, 62, 16), // "DataProvaChanged"
QT_MOC_LITERAL(6, 79, 21), // "NomeRobotProvaChanged"
QT_MOC_LITERAL(7, 101, 9), // "nomeProva"
QT_MOC_LITERAL(8, 111, 12), // "estadosProva"
QT_MOC_LITERAL(9, 124, 10), // "tempoProva"
QT_MOC_LITERAL(10, 135, 9), // "dataProva"
QT_MOC_LITERAL(11, 145, 14) // "nomeRobotProva"

    },
    "Prova\0NomeProvaChanged\0\0EstadosProvaChanged\0"
    "TempoProvaChanged\0DataProvaChanged\0"
    "NomeRobotProvaChanged\0nomeProva\0"
    "estadosProva\0tempoProva\0dataProva\0"
    "nomeRobotProva"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Prova[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       5,   44, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x06 /* Public */,
       3,    0,   40,    2, 0x06 /* Public */,
       4,    0,   41,    2, 0x06 /* Public */,
       5,    0,   42,    2, 0x06 /* Public */,
       6,    0,   43,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       7, QMetaType::QString, 0x00495103,
       8, QMetaType::QString, 0x00495103,
       9, QMetaType::QTime, 0x00495103,
      10, QMetaType::QDate, 0x00495103,
      11, QMetaType::QString, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,

       0        // eod
};

void Prova::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Prova *_t = static_cast<Prova *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->NomeProvaChanged(); break;
        case 1: _t->EstadosProvaChanged(); break;
        case 2: _t->TempoProvaChanged(); break;
        case 3: _t->DataProvaChanged(); break;
        case 4: _t->NomeRobotProvaChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Prova::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Prova::NomeProvaChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Prova::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Prova::EstadosProvaChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Prova::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Prova::TempoProvaChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Prova::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Prova::DataProvaChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Prova::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Prova::NomeRobotProvaChanged)) {
                *result = 4;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        Prova *_t = static_cast<Prova *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->nomeProva(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->estadosProva(); break;
        case 2: *reinterpret_cast< QTime*>(_v) = _t->tempoProva(); break;
        case 3: *reinterpret_cast< QDate*>(_v) = _t->dataProva(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->nomeRobotProva(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        Prova *_t = static_cast<Prova *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setNomeProva(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setEstadosProva(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setTempoProva(*reinterpret_cast< QTime*>(_v)); break;
        case 3: _t->setDataProva(*reinterpret_cast< QDate*>(_v)); break;
        case 4: _t->setNomeRobotProva(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Prova::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Prova.data,
      qt_meta_data_Prova,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Prova::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Prova::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Prova.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Prova::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Prova::NomeProvaChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Prova::EstadosProvaChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void Prova::TempoProvaChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void Prova::DataProvaChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void Prova::NomeRobotProvaChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
