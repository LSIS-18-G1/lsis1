/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[42];
    char stringdata0[1111];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 4), // "back"
QT_MOC_LITERAL(2, 16, 0), // ""
QT_MOC_LITERAL(3, 17, 26), // "on_actSearchData_triggered"
QT_MOC_LITERAL(4, 44, 20), // "on_actSave_triggered"
QT_MOC_LITERAL(5, 65, 21), // "on_actClose_triggered"
QT_MOC_LITERAL(6, 87, 25), // "on_actInsEquipa_triggered"
QT_MOC_LITERAL(7, 113, 29), // "on_InsEquipaOkCancel_accepted"
QT_MOC_LITERAL(8, 143, 29), // "on_InsEquipaOkCancel_rejected"
QT_MOC_LITERAL(9, 173, 26), // "on_actListEquipa_triggered"
QT_MOC_LITERAL(10, 200, 23), // "on_ListEquipaOk_clicked"
QT_MOC_LITERAL(11, 224, 28), // "on_ListEquipaAlterar_clicked"
QT_MOC_LITERAL(12, 253, 29), // "on_AltEquipaOkCancel_accepted"
QT_MOC_LITERAL(13, 283, 29), // "on_AltEquipaOkCancel_rejected"
QT_MOC_LITERAL(14, 313, 29), // "on_ListEquipaEliminar_clicked"
QT_MOC_LITERAL(15, 343, 27), // "on_actInsElemento_triggered"
QT_MOC_LITERAL(16, 371, 31), // "on_InsElementoOkCancel_accepted"
QT_MOC_LITERAL(17, 403, 31), // "on_InsElementoOkCancel_rejected"
QT_MOC_LITERAL(18, 435, 28), // "on_actListElemento_triggered"
QT_MOC_LITERAL(19, 464, 25), // "on_ListElementoOk_clicked"
QT_MOC_LITERAL(20, 490, 30), // "on_ListElementoAlterar_clicked"
QT_MOC_LITERAL(21, 521, 31), // "on_AltElementoOkCancel_accepted"
QT_MOC_LITERAL(22, 553, 31), // "on_AltElementoOkCancel_rejected"
QT_MOC_LITERAL(23, 585, 31), // "on_ListElementoEliminar_clicked"
QT_MOC_LITERAL(24, 617, 24), // "on_actInsRobot_triggered"
QT_MOC_LITERAL(25, 642, 28), // "on_InsRobotOkCancel_accepted"
QT_MOC_LITERAL(26, 671, 28), // "on_InsRobotOkCancel_rejected"
QT_MOC_LITERAL(27, 700, 25), // "on_actListRobot_triggered"
QT_MOC_LITERAL(28, 726, 22), // "on_ListRobotOk_clicked"
QT_MOC_LITERAL(29, 749, 27), // "on_ListRobotAlterar_clicked"
QT_MOC_LITERAL(30, 777, 28), // "on_AltRobotOkCancel_accepted"
QT_MOC_LITERAL(31, 806, 28), // "on_AltRobotOkCancel_rejected"
QT_MOC_LITERAL(32, 835, 28), // "on_ListRobotEliminar_clicked"
QT_MOC_LITERAL(33, 864, 24), // "on_actInsProva_triggered"
QT_MOC_LITERAL(34, 889, 28), // "on_InsProvaOkCancel_accepted"
QT_MOC_LITERAL(35, 918, 28), // "on_InsProvaOkCancel_rejected"
QT_MOC_LITERAL(36, 947, 25), // "on_actListProva_triggered"
QT_MOC_LITERAL(37, 973, 22), // "on_ListProvaOk_clicked"
QT_MOC_LITERAL(38, 996, 27), // "on_ListProvaAlterar_clicked"
QT_MOC_LITERAL(39, 1024, 28), // "on_AltProvaOkCancel_accepted"
QT_MOC_LITERAL(40, 1053, 28), // "on_AltProvaOkCancel_rejected"
QT_MOC_LITERAL(41, 1082, 28) // "on_ListProvaEliminar_clicked"

    },
    "MainWindow\0back\0\0on_actSearchData_triggered\0"
    "on_actSave_triggered\0on_actClose_triggered\0"
    "on_actInsEquipa_triggered\0"
    "on_InsEquipaOkCancel_accepted\0"
    "on_InsEquipaOkCancel_rejected\0"
    "on_actListEquipa_triggered\0"
    "on_ListEquipaOk_clicked\0"
    "on_ListEquipaAlterar_clicked\0"
    "on_AltEquipaOkCancel_accepted\0"
    "on_AltEquipaOkCancel_rejected\0"
    "on_ListEquipaEliminar_clicked\0"
    "on_actInsElemento_triggered\0"
    "on_InsElementoOkCancel_accepted\0"
    "on_InsElementoOkCancel_rejected\0"
    "on_actListElemento_triggered\0"
    "on_ListElementoOk_clicked\0"
    "on_ListElementoAlterar_clicked\0"
    "on_AltElementoOkCancel_accepted\0"
    "on_AltElementoOkCancel_rejected\0"
    "on_ListElementoEliminar_clicked\0"
    "on_actInsRobot_triggered\0"
    "on_InsRobotOkCancel_accepted\0"
    "on_InsRobotOkCancel_rejected\0"
    "on_actListRobot_triggered\0"
    "on_ListRobotOk_clicked\0"
    "on_ListRobotAlterar_clicked\0"
    "on_AltRobotOkCancel_accepted\0"
    "on_AltRobotOkCancel_rejected\0"
    "on_ListRobotEliminar_clicked\0"
    "on_actInsProva_triggered\0"
    "on_InsProvaOkCancel_accepted\0"
    "on_InsProvaOkCancel_rejected\0"
    "on_actListProva_triggered\0"
    "on_ListProvaOk_clicked\0"
    "on_ListProvaAlterar_clicked\0"
    "on_AltProvaOkCancel_accepted\0"
    "on_AltProvaOkCancel_rejected\0"
    "on_ListProvaEliminar_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      40,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  214,    2, 0x08 /* Private */,
       3,    0,  215,    2, 0x08 /* Private */,
       4,    0,  216,    2, 0x08 /* Private */,
       5,    0,  217,    2, 0x08 /* Private */,
       6,    0,  218,    2, 0x08 /* Private */,
       7,    0,  219,    2, 0x08 /* Private */,
       8,    0,  220,    2, 0x08 /* Private */,
       9,    0,  221,    2, 0x08 /* Private */,
      10,    0,  222,    2, 0x08 /* Private */,
      11,    0,  223,    2, 0x08 /* Private */,
      12,    0,  224,    2, 0x08 /* Private */,
      13,    0,  225,    2, 0x08 /* Private */,
      14,    0,  226,    2, 0x08 /* Private */,
      15,    0,  227,    2, 0x08 /* Private */,
      16,    0,  228,    2, 0x08 /* Private */,
      17,    0,  229,    2, 0x08 /* Private */,
      18,    0,  230,    2, 0x08 /* Private */,
      19,    0,  231,    2, 0x08 /* Private */,
      20,    0,  232,    2, 0x08 /* Private */,
      21,    0,  233,    2, 0x08 /* Private */,
      22,    0,  234,    2, 0x08 /* Private */,
      23,    0,  235,    2, 0x08 /* Private */,
      24,    0,  236,    2, 0x08 /* Private */,
      25,    0,  237,    2, 0x08 /* Private */,
      26,    0,  238,    2, 0x08 /* Private */,
      27,    0,  239,    2, 0x08 /* Private */,
      28,    0,  240,    2, 0x08 /* Private */,
      29,    0,  241,    2, 0x08 /* Private */,
      30,    0,  242,    2, 0x08 /* Private */,
      31,    0,  243,    2, 0x08 /* Private */,
      32,    0,  244,    2, 0x08 /* Private */,
      33,    0,  245,    2, 0x08 /* Private */,
      34,    0,  246,    2, 0x08 /* Private */,
      35,    0,  247,    2, 0x08 /* Private */,
      36,    0,  248,    2, 0x08 /* Private */,
      37,    0,  249,    2, 0x08 /* Private */,
      38,    0,  250,    2, 0x08 /* Private */,
      39,    0,  251,    2, 0x08 /* Private */,
      40,    0,  252,    2, 0x08 /* Private */,
      41,    0,  253,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->back(); break;
        case 1: _t->on_actSearchData_triggered(); break;
        case 2: _t->on_actSave_triggered(); break;
        case 3: _t->on_actClose_triggered(); break;
        case 4: _t->on_actInsEquipa_triggered(); break;
        case 5: _t->on_InsEquipaOkCancel_accepted(); break;
        case 6: _t->on_InsEquipaOkCancel_rejected(); break;
        case 7: _t->on_actListEquipa_triggered(); break;
        case 8: _t->on_ListEquipaOk_clicked(); break;
        case 9: _t->on_ListEquipaAlterar_clicked(); break;
        case 10: _t->on_AltEquipaOkCancel_accepted(); break;
        case 11: _t->on_AltEquipaOkCancel_rejected(); break;
        case 12: _t->on_ListEquipaEliminar_clicked(); break;
        case 13: _t->on_actInsElemento_triggered(); break;
        case 14: _t->on_InsElementoOkCancel_accepted(); break;
        case 15: _t->on_InsElementoOkCancel_rejected(); break;
        case 16: _t->on_actListElemento_triggered(); break;
        case 17: _t->on_ListElementoOk_clicked(); break;
        case 18: _t->on_ListElementoAlterar_clicked(); break;
        case 19: _t->on_AltElementoOkCancel_accepted(); break;
        case 20: _t->on_AltElementoOkCancel_rejected(); break;
        case 21: _t->on_ListElementoEliminar_clicked(); break;
        case 22: _t->on_actInsRobot_triggered(); break;
        case 23: _t->on_InsRobotOkCancel_accepted(); break;
        case 24: _t->on_InsRobotOkCancel_rejected(); break;
        case 25: _t->on_actListRobot_triggered(); break;
        case 26: _t->on_ListRobotOk_clicked(); break;
        case 27: _t->on_ListRobotAlterar_clicked(); break;
        case 28: _t->on_AltRobotOkCancel_accepted(); break;
        case 29: _t->on_AltRobotOkCancel_rejected(); break;
        case 30: _t->on_ListRobotEliminar_clicked(); break;
        case 31: _t->on_actInsProva_triggered(); break;
        case 32: _t->on_InsProvaOkCancel_accepted(); break;
        case 33: _t->on_InsProvaOkCancel_rejected(); break;
        case 34: _t->on_actListProva_triggered(); break;
        case 35: _t->on_ListProvaOk_clicked(); break;
        case 36: _t->on_ListProvaAlterar_clicked(); break;
        case 37: _t->on_AltProvaOkCancel_accepted(); break;
        case 38: _t->on_AltProvaOkCancel_rejected(); break;
        case 39: _t->on_ListProvaEliminar_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 40)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 40;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 40)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 40;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
