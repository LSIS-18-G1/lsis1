/****************************************************************************
** Meta object code from reading C++ file 'projeto.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../projeto.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'projeto.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Projeto_t {
    QByteArrayData data[18];
    char stringdata0[173];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Projeto_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Projeto_t qt_meta_stringdata_Projeto = {
    {
QT_MOC_LITERAL(0, 0, 7), // "Projeto"
QT_MOC_LITERAL(1, 8, 11), // "equipaAdded"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 7), // "Equipa*"
QT_MOC_LITERAL(4, 29, 6), // "equipa"
QT_MOC_LITERAL(5, 36, 13), // "equipaRemoved"
QT_MOC_LITERAL(6, 50, 13), // "elementoAdded"
QT_MOC_LITERAL(7, 64, 9), // "Elemento*"
QT_MOC_LITERAL(8, 74, 8), // "elemento"
QT_MOC_LITERAL(9, 83, 15), // "elementoRemoved"
QT_MOC_LITERAL(10, 99, 10), // "provaAdded"
QT_MOC_LITERAL(11, 110, 6), // "Prova*"
QT_MOC_LITERAL(12, 117, 5), // "prova"
QT_MOC_LITERAL(13, 123, 12), // "provaRemoved"
QT_MOC_LITERAL(14, 136, 10), // "robotAdded"
QT_MOC_LITERAL(15, 147, 6), // "Robot*"
QT_MOC_LITERAL(16, 154, 5), // "robot"
QT_MOC_LITERAL(17, 160, 12) // "robotRemoved"

    },
    "Projeto\0equipaAdded\0\0Equipa*\0equipa\0"
    "equipaRemoved\0elementoAdded\0Elemento*\0"
    "elemento\0elementoRemoved\0provaAdded\0"
    "Prova*\0prova\0provaRemoved\0robotAdded\0"
    "Robot*\0robot\0robotRemoved"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Projeto[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       5,    1,   57,    2, 0x06 /* Public */,
       6,    1,   60,    2, 0x06 /* Public */,
       9,    1,   63,    2, 0x06 /* Public */,
      10,    1,   66,    2, 0x06 /* Public */,
      13,    1,   69,    2, 0x06 /* Public */,
      14,    1,   72,    2, 0x06 /* Public */,
      17,    1,   75,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 15,   16,

       0        // eod
};

void Projeto::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Projeto *_t = static_cast<Projeto *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->equipaAdded((*reinterpret_cast< Equipa*(*)>(_a[1]))); break;
        case 1: _t->equipaRemoved((*reinterpret_cast< Equipa*(*)>(_a[1]))); break;
        case 2: _t->elementoAdded((*reinterpret_cast< Elemento*(*)>(_a[1]))); break;
        case 3: _t->elementoRemoved((*reinterpret_cast< Elemento*(*)>(_a[1]))); break;
        case 4: _t->provaAdded((*reinterpret_cast< Prova*(*)>(_a[1]))); break;
        case 5: _t->provaRemoved((*reinterpret_cast< Prova*(*)>(_a[1]))); break;
        case 6: _t->robotAdded((*reinterpret_cast< Robot*(*)>(_a[1]))); break;
        case 7: _t->robotRemoved((*reinterpret_cast< Robot*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Equipa* >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Equipa* >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Elemento* >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Elemento* >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Prova* >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Prova* >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Robot* >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Robot* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Projeto::*)(Equipa * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Projeto::equipaAdded)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Projeto::*)(Equipa * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Projeto::equipaRemoved)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Projeto::*)(Elemento * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Projeto::elementoAdded)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Projeto::*)(Elemento * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Projeto::elementoRemoved)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Projeto::*)(Prova * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Projeto::provaAdded)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (Projeto::*)(Prova * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Projeto::provaRemoved)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (Projeto::*)(Robot * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Projeto::robotAdded)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (Projeto::*)(Robot * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Projeto::robotRemoved)) {
                *result = 7;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Projeto::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Projeto.data,
      qt_meta_data_Projeto,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Projeto::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Projeto::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Projeto.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Projeto::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void Projeto::equipaAdded(Equipa * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Projeto::equipaRemoved(Equipa * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Projeto::elementoAdded(Elemento * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Projeto::elementoRemoved(Elemento * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Projeto::provaAdded(Prova * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Projeto::provaRemoved(Prova * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Projeto::robotAdded(Robot * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Projeto::robotRemoved(Robot * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
