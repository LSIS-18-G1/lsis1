/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[50];
    char stringdata0[1349];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 4), // "back"
QT_MOC_LITERAL(2, 16, 0), // ""
QT_MOC_LITERAL(3, 17, 26), // "on_actSearchData_triggered"
QT_MOC_LITERAL(4, 44, 21), // "on_actClose_triggered"
QT_MOC_LITERAL(5, 66, 31), // "on_actRegistarEstados_triggered"
QT_MOC_LITERAL(6, 98, 23), // "on_EstadoFrente_clicked"
QT_MOC_LITERAL(7, 122, 21), // "on_EstadoTras_clicked"
QT_MOC_LITERAL(8, 144, 24), // "on_EstadoDireita_clicked"
QT_MOC_LITERAL(9, 169, 25), // "on_EstadoEsquerda_clicked"
QT_MOC_LITERAL(10, 195, 33), // "on_RegistarEstadosGuardar_cli..."
QT_MOC_LITERAL(11, 229, 34), // "on_RegistarEstadosCancelar_cl..."
QT_MOC_LITERAL(12, 264, 28), // "on_actIniciarProva_triggered"
QT_MOC_LITERAL(13, 293, 31), // "on_IniciarProvaCancelar_clicked"
QT_MOC_LITERAL(14, 325, 25), // "on_actInsEquipa_triggered"
QT_MOC_LITERAL(15, 351, 29), // "on_InsEquipaOkCancel_accepted"
QT_MOC_LITERAL(16, 381, 29), // "on_InsEquipaOkCancel_rejected"
QT_MOC_LITERAL(17, 411, 26), // "on_actListEquipa_triggered"
QT_MOC_LITERAL(18, 438, 23), // "on_ListEquipaOk_clicked"
QT_MOC_LITERAL(19, 462, 28), // "on_ListEquipaAlterar_clicked"
QT_MOC_LITERAL(20, 491, 29), // "on_AltEquipaOkCancel_accepted"
QT_MOC_LITERAL(21, 521, 29), // "on_AltEquipaOkCancel_rejected"
QT_MOC_LITERAL(22, 551, 29), // "on_ListEquipaEliminar_clicked"
QT_MOC_LITERAL(23, 581, 27), // "on_actInsElemento_triggered"
QT_MOC_LITERAL(24, 609, 31), // "on_InsElementoOkCancel_accepted"
QT_MOC_LITERAL(25, 641, 31), // "on_InsElementoOkCancel_rejected"
QT_MOC_LITERAL(26, 673, 28), // "on_actListElemento_triggered"
QT_MOC_LITERAL(27, 702, 25), // "on_ListElementoOk_clicked"
QT_MOC_LITERAL(28, 728, 30), // "on_ListElementoAlterar_clicked"
QT_MOC_LITERAL(29, 759, 31), // "on_AltElementoOkCancel_accepted"
QT_MOC_LITERAL(30, 791, 31), // "on_AltElementoOkCancel_rejected"
QT_MOC_LITERAL(31, 823, 31), // "on_ListElementoEliminar_clicked"
QT_MOC_LITERAL(32, 855, 24), // "on_actInsRobot_triggered"
QT_MOC_LITERAL(33, 880, 28), // "on_InsRobotOkCancel_accepted"
QT_MOC_LITERAL(34, 909, 28), // "on_InsRobotOkCancel_rejected"
QT_MOC_LITERAL(35, 938, 25), // "on_actListRobot_triggered"
QT_MOC_LITERAL(36, 964, 22), // "on_ListRobotOk_clicked"
QT_MOC_LITERAL(37, 987, 27), // "on_ListRobotAlterar_clicked"
QT_MOC_LITERAL(38, 1015, 28), // "on_AltRobotOkCancel_accepted"
QT_MOC_LITERAL(39, 1044, 28), // "on_AltRobotOkCancel_rejected"
QT_MOC_LITERAL(40, 1073, 28), // "on_ListRobotEliminar_clicked"
QT_MOC_LITERAL(41, 1102, 24), // "on_actInsProva_triggered"
QT_MOC_LITERAL(42, 1127, 28), // "on_InsProvaOkCancel_accepted"
QT_MOC_LITERAL(43, 1156, 28), // "on_InsProvaOkCancel_rejected"
QT_MOC_LITERAL(44, 1185, 25), // "on_actListProva_triggered"
QT_MOC_LITERAL(45, 1211, 22), // "on_ListProvaOk_clicked"
QT_MOC_LITERAL(46, 1234, 27), // "on_ListProvaAlterar_clicked"
QT_MOC_LITERAL(47, 1262, 28), // "on_AltProvaOkCancel_accepted"
QT_MOC_LITERAL(48, 1291, 28), // "on_AltProvaOkCancel_rejected"
QT_MOC_LITERAL(49, 1320, 28) // "on_ListProvaEliminar_clicked"

    },
    "MainWindow\0back\0\0on_actSearchData_triggered\0"
    "on_actClose_triggered\0"
    "on_actRegistarEstados_triggered\0"
    "on_EstadoFrente_clicked\0on_EstadoTras_clicked\0"
    "on_EstadoDireita_clicked\0"
    "on_EstadoEsquerda_clicked\0"
    "on_RegistarEstadosGuardar_clicked\0"
    "on_RegistarEstadosCancelar_clicked\0"
    "on_actIniciarProva_triggered\0"
    "on_IniciarProvaCancelar_clicked\0"
    "on_actInsEquipa_triggered\0"
    "on_InsEquipaOkCancel_accepted\0"
    "on_InsEquipaOkCancel_rejected\0"
    "on_actListEquipa_triggered\0"
    "on_ListEquipaOk_clicked\0"
    "on_ListEquipaAlterar_clicked\0"
    "on_AltEquipaOkCancel_accepted\0"
    "on_AltEquipaOkCancel_rejected\0"
    "on_ListEquipaEliminar_clicked\0"
    "on_actInsElemento_triggered\0"
    "on_InsElementoOkCancel_accepted\0"
    "on_InsElementoOkCancel_rejected\0"
    "on_actListElemento_triggered\0"
    "on_ListElementoOk_clicked\0"
    "on_ListElementoAlterar_clicked\0"
    "on_AltElementoOkCancel_accepted\0"
    "on_AltElementoOkCancel_rejected\0"
    "on_ListElementoEliminar_clicked\0"
    "on_actInsRobot_triggered\0"
    "on_InsRobotOkCancel_accepted\0"
    "on_InsRobotOkCancel_rejected\0"
    "on_actListRobot_triggered\0"
    "on_ListRobotOk_clicked\0"
    "on_ListRobotAlterar_clicked\0"
    "on_AltRobotOkCancel_accepted\0"
    "on_AltRobotOkCancel_rejected\0"
    "on_ListRobotEliminar_clicked\0"
    "on_actInsProva_triggered\0"
    "on_InsProvaOkCancel_accepted\0"
    "on_InsProvaOkCancel_rejected\0"
    "on_actListProva_triggered\0"
    "on_ListProvaOk_clicked\0"
    "on_ListProvaAlterar_clicked\0"
    "on_AltProvaOkCancel_accepted\0"
    "on_AltProvaOkCancel_rejected\0"
    "on_ListProvaEliminar_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      48,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  254,    2, 0x08 /* Private */,
       3,    0,  255,    2, 0x08 /* Private */,
       4,    0,  256,    2, 0x08 /* Private */,
       5,    0,  257,    2, 0x08 /* Private */,
       6,    0,  258,    2, 0x08 /* Private */,
       7,    0,  259,    2, 0x08 /* Private */,
       8,    0,  260,    2, 0x08 /* Private */,
       9,    0,  261,    2, 0x08 /* Private */,
      10,    0,  262,    2, 0x08 /* Private */,
      11,    0,  263,    2, 0x08 /* Private */,
      12,    0,  264,    2, 0x08 /* Private */,
      13,    0,  265,    2, 0x08 /* Private */,
      14,    0,  266,    2, 0x08 /* Private */,
      15,    0,  267,    2, 0x08 /* Private */,
      16,    0,  268,    2, 0x08 /* Private */,
      17,    0,  269,    2, 0x08 /* Private */,
      18,    0,  270,    2, 0x08 /* Private */,
      19,    0,  271,    2, 0x08 /* Private */,
      20,    0,  272,    2, 0x08 /* Private */,
      21,    0,  273,    2, 0x08 /* Private */,
      22,    0,  274,    2, 0x08 /* Private */,
      23,    0,  275,    2, 0x08 /* Private */,
      24,    0,  276,    2, 0x08 /* Private */,
      25,    0,  277,    2, 0x08 /* Private */,
      26,    0,  278,    2, 0x08 /* Private */,
      27,    0,  279,    2, 0x08 /* Private */,
      28,    0,  280,    2, 0x08 /* Private */,
      29,    0,  281,    2, 0x08 /* Private */,
      30,    0,  282,    2, 0x08 /* Private */,
      31,    0,  283,    2, 0x08 /* Private */,
      32,    0,  284,    2, 0x08 /* Private */,
      33,    0,  285,    2, 0x08 /* Private */,
      34,    0,  286,    2, 0x08 /* Private */,
      35,    0,  287,    2, 0x08 /* Private */,
      36,    0,  288,    2, 0x08 /* Private */,
      37,    0,  289,    2, 0x08 /* Private */,
      38,    0,  290,    2, 0x08 /* Private */,
      39,    0,  291,    2, 0x08 /* Private */,
      40,    0,  292,    2, 0x08 /* Private */,
      41,    0,  293,    2, 0x08 /* Private */,
      42,    0,  294,    2, 0x08 /* Private */,
      43,    0,  295,    2, 0x08 /* Private */,
      44,    0,  296,    2, 0x08 /* Private */,
      45,    0,  297,    2, 0x08 /* Private */,
      46,    0,  298,    2, 0x08 /* Private */,
      47,    0,  299,    2, 0x08 /* Private */,
      48,    0,  300,    2, 0x08 /* Private */,
      49,    0,  301,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->back(); break;
        case 1: _t->on_actSearchData_triggered(); break;
        case 2: _t->on_actClose_triggered(); break;
        case 3: _t->on_actRegistarEstados_triggered(); break;
        case 4: _t->on_EstadoFrente_clicked(); break;
        case 5: _t->on_EstadoTras_clicked(); break;
        case 6: _t->on_EstadoDireita_clicked(); break;
        case 7: _t->on_EstadoEsquerda_clicked(); break;
        case 8: _t->on_RegistarEstadosGuardar_clicked(); break;
        case 9: _t->on_RegistarEstadosCancelar_clicked(); break;
        case 10: _t->on_actIniciarProva_triggered(); break;
        case 11: _t->on_IniciarProvaCancelar_clicked(); break;
        case 12: _t->on_actInsEquipa_triggered(); break;
        case 13: _t->on_InsEquipaOkCancel_accepted(); break;
        case 14: _t->on_InsEquipaOkCancel_rejected(); break;
        case 15: _t->on_actListEquipa_triggered(); break;
        case 16: _t->on_ListEquipaOk_clicked(); break;
        case 17: _t->on_ListEquipaAlterar_clicked(); break;
        case 18: _t->on_AltEquipaOkCancel_accepted(); break;
        case 19: _t->on_AltEquipaOkCancel_rejected(); break;
        case 20: _t->on_ListEquipaEliminar_clicked(); break;
        case 21: _t->on_actInsElemento_triggered(); break;
        case 22: _t->on_InsElementoOkCancel_accepted(); break;
        case 23: _t->on_InsElementoOkCancel_rejected(); break;
        case 24: _t->on_actListElemento_triggered(); break;
        case 25: _t->on_ListElementoOk_clicked(); break;
        case 26: _t->on_ListElementoAlterar_clicked(); break;
        case 27: _t->on_AltElementoOkCancel_accepted(); break;
        case 28: _t->on_AltElementoOkCancel_rejected(); break;
        case 29: _t->on_ListElementoEliminar_clicked(); break;
        case 30: _t->on_actInsRobot_triggered(); break;
        case 31: _t->on_InsRobotOkCancel_accepted(); break;
        case 32: _t->on_InsRobotOkCancel_rejected(); break;
        case 33: _t->on_actListRobot_triggered(); break;
        case 34: _t->on_ListRobotOk_clicked(); break;
        case 35: _t->on_ListRobotAlterar_clicked(); break;
        case 36: _t->on_AltRobotOkCancel_accepted(); break;
        case 37: _t->on_AltRobotOkCancel_rejected(); break;
        case 38: _t->on_ListRobotEliminar_clicked(); break;
        case 39: _t->on_actInsProva_triggered(); break;
        case 40: _t->on_InsProvaOkCancel_accepted(); break;
        case 41: _t->on_InsProvaOkCancel_rejected(); break;
        case 42: _t->on_actListProva_triggered(); break;
        case 43: _t->on_ListProvaOk_clicked(); break;
        case 44: _t->on_ListProvaAlterar_clicked(); break;
        case 45: _t->on_AltProvaOkCancel_accepted(); break;
        case 46: _t->on_AltProvaOkCancel_rejected(); break;
        case 47: _t->on_ListProvaEliminar_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 48)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 48;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 48)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 48;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
