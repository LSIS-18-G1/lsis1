#include "Equipa.h"
#include <QString>

using namespace std;

Equipa::Equipa() {
}

Equipa::Equipa(QString nome) {
	this->nome = nome;
}

Equipa::Equipa(const Equipa &equip) {
	this-> nome = equip.nome;
}

QString Equipa::getNome() const {
	return this->nome;
}

void Equipa::setNome(QString nome) {
	this->nome = nome;
}