#include "Robo.h"
#include <QString>

using namespace std;

Robo::Robo() {
}

Robo::Robo(QString nome, QString modelo, QString num_sensor) {
	this->nome = nome;
	this->modelo = modelo;
	this->num_sensor = num_sensor;
}

Robo::Robo(const Robo &robo) {
	this->nome = robo.nome;
	this->modelo = robo.modelo;
	this->num_sensor = robo.num_sensor;
}


QString Robo::getNome() const {
	return this->nome;
}
QString Robo::getModelo() const {
	return this->modelo;
}
QString Robo::getNum_sensor() const {
	return this->num_sensor;
}

void Robo::setNome(QString nome) {
	this->nome = nome;
}
void Robo::setModelo(QString modelo) {
	this->modelo = modelo;
}
void Robo::setNum_sensor(QString num_sensor) {
	this->num_sensor = num_sensor;
}