#include "Prova.h"
#include <QString>

using namespace std;

Prova::Prova() {
}

Prova::Prova(QString estados, QTime tempo, QDate data) {
	this->estados = estados;
	this->tempo = tempo;
	this->data = data;
}

Prova::Prova(const Prova &prova) {
	this->estados= prova.estados;
	this->tempo = prova.tempo;
	this->data = prova.data;
}

QString Prova::getEstados() const {
	return this->estados;
}
QTime Prova::getTempo() const {
	return this->tempo;
}
QDate Prova::getData() const {
	return this->data;
}

void Prova::setEstados(QString estados) {
	this->estados = estados;
}
void Prova::setTempo(QTime tempo) {
	this->tempo = tempo;
}
void Prova::setData(QDate data) {
	this->data = data;
}