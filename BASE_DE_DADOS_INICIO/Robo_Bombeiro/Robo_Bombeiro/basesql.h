#pragma once
#pragma once
#include "Equipa.h"
#include "Elementos.h"
#include "Robo.h"
#include "Prova.h"
#include <vector>

using namespace std;


class basesql {

public:

	void teste();
	
	void getEquipas(vector<Equipa>& equip);
	void getElementos(vector<Elementos>& elem);
	void getRobos(vector<Robo>& robo);
	void getProvas(vector<Prova>& prova);

	void insertEquipas(Equipa equip);
	void insertElementos(Elementos elem);
	void insertRobos(Robo robo);
	void insertProvas(Prova prova);

	void alterarEquipasNome(QHASH id, QString nome);

	void alterarElementosNome(QHASH id, QString nome);
	void alterarElementosNum(QHASH id, QString num);

	void alterarRobosNome(QHASH id, QString nome);
	void alterarRobosModelo(QHASH id, QString modelo);
	void alterarRobosNum_sensor(QHASH id, QString num_sensor);

	void alterarProvasEstados(QHASH id, QString estados);
	void alterarProvasTempo(QHASH id, QTime tempo);
	void alterarProvasData(QHASH id, QDate data);


	void apagarEquipas(QHASH id);
	void apagarElementos(QHASH id);
	void apagarRobos(QHASH id);
	void apagarProvas(QHASH id);
	
};