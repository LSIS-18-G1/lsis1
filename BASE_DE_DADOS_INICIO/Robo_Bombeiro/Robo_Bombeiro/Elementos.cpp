#include "Elementos.h"
#include <QString>

using namespace std;

Elementos::Elementos() {
}

Elementos::Elementos(QString nome, QString num){
	this->nome = nome;
	this->num = num;
}

Elementos::Elementos(const Elementos &elem) {;
	this->nome = elem.nome;
	this->num = elem.num;

}

QString Elementos::getNome() const {
	return this->nome;
}
QString Elementos::getNum() const {
	return this->num;
}
void Elementos::setNome(QString nome) {
	this->nome = nome;
}
void Elementos::setNum(QString num) {
	this->num = num;
}
