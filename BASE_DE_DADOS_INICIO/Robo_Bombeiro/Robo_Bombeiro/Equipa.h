#pragma once
#include <QString>
using namespace std;

class Equipa {

private:
	//parāmetros da classe equipa
	QString nome;

public:
	//Construtores e Funcao Copia
	Equipa();
	Equipa(QString nome);
	Equipa(const Equipa & equip);//copiar equipa

	//gets
	QString getNome() const;
	
	//sets
	void setNome(QString nome);

};