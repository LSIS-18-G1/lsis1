#pragma once
#include <QString>
using namespace std;

class Robo {

private:
	//parāmetros da classe Robo

	QString nome;
	QString modelo;
	QString num_sensor;

public:
	//Construtores e Funcao Copia
	Robo();
	Robo(QString nome, QString modelo, QString num_sensor);
	Robo(const Robo & robo);//copiar Robo

	//gets
	QString getNome() const;
	QString getModelo() const;
	QString getNum_sensor() const;

	//sets

	void setNome(QString nome);
	void setModelo(QString modelo);
	void setNum_sensor(QString num_sensor);
};