#pragma once
#include <QString>
using namespace std;

class Prova {

private:
	//parāmetros da classe Prova
	QString estados;
	QTime tempo;
	QDate data;
public:
	//Construtores e Funcao Copia
	Prova();
	Prova(QString estados, QTime tempo, QDate data);
	Prova(const Prova & prova);//copiar Prova

	//gets
	QString getEstados() const;
	QTime getTempo() const;
	QDate getData()const;

	//sets
	void setEstados(QString estados);
	void setTempo(QTime tempo);
	void setData(QDate data);
};