#pragma once
#include <QString>
using namespace std;

class Elementos {

private:
	//parāmetros da classe Elementos
	QString nome;
	QString num;

public:
	//Construtores e Funcao Copia
	Elementos();
	Elementos(QString nome, QString num);
	Elementos(const Elementos & elem);//copiar Elementos

	//gets
	QString getNome() const;
	QString getNum() const;

	//sets
	void setNome(QString nome);
	void setNum( QString num);

};