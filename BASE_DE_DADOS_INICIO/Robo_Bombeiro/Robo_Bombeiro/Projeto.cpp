#include "Projeto.h"
#include "Utils.h"
#include <iostream>
#include <vector>
using namespace std;

Utils util2;
basesql dados2;

Projeto::Projeto() {
}

Projeto::Projeto(vector<Equipa> equipas, vector<Elementos> elementos, vector<Robo> robos, vector<Prova> provas) {
	this->equipas = equipas;
	this->elementos = elementos;
	this->robos = robos;
	this->provas = provas;
}

Projeto::Projeto(Projeto &proj) {
	for (int i1 = 0; i1 < this->equipas.size(); i1++) {
		this->equipas.push_back(proj.equipas[i1]);
	}
	for (int i2 = 0; i2 < this->elementos.size(); i2++) {
		this->elementos.push_back(proj.elementos[i2]);
	}
	for (int i3 = 0; i3 < this->robos.size(); i3++) {
		this->robos.push_back(proj.robos[i3]);
	}
	for (int i4 = 0; i4 < this->provas.size(); i4++) {
		this->provas.push_back(proj.provas[i4]);
	}
}

void Projeto::startConnection() {
	dados2.getEquipas(equipas);
	dados2.getElementos(elementos);
	dados2.getRobos(robos);
	dados2.getProvas(provas);
}

vector<Equipa> Projeto::getListaEquipas() {
	return this->equipas;
}
vector<Elementos> Projeto::getListaElementos() {
	return this->elementos;
}
vector<Robo> Projeto::getListaRobos() {
	return this->robos;
}
vector<Prova> Projeto::getListaProvas() {
	return this->provas;
}

void Projeto::setListaEquipas(vector<Equipa> equipas) {
	this->equipas = equipas;
}
void Projeto::setListaElementos(vector<Elementos> elementos) {
	this->elementos = elementos;
}
void Projeto::setListaRobos(vector<Robo> robos) {
	this->robos = robos;
}
void Projeto::setListaProvas(vector<Prova> provas) {
	this->provas = provas;
}

void Projeto::addEquipas(Equipa equip) {
	this->equipas.push_back(equip);
	dados2.insertEquipas(equip);
}
void Projeto::addElementos(Elementos elem) {
	this->elementos.push_back(elem);
	dados2.insertElementos(elem);
}
void Projeto::addRobos(Robo robo) {
	this->robos.push_back(robo);
	dados2.insertRobos(robo);
}
void Projeto::addProvas(Prova prova) {
	this->provas.push_back(prova);
	dados2.insertProvas(prova);
}

void Projeto::alterarEquipas(int pos, int alteracao) {

	double novodouble;
	string novastring;

	bool same = false;

	switch (alteracao) {
	case 1:
		cout << "Introduza um novo nome :" << endl;
		cin >> novastring;
		novastring = util2.converterMinuscula(novastring);
		this->equipas[pos].setNome(novastring);
		dados2.alterarEquipasNome(this->equipas[pos].getID(), novastring);
		break;
	}
}

void Projeto::alterarElementos(int pos, int alteracao) {

	int novoint;
	string novastring;

	bool same = false;
	switch (alteracao) {
	case 1:
		cout << "Introduza o novo nome:" << endl;
		cin >> novastring;
		novastring = util2.converterMinuscula(novastring);

		this->elementos[pos].setNome(novastring);
		dados2.alterarElementosNome(this->elementos[pos].getID(), novastring);
		break;
	case 2:

		cout << endl << "Insira o novo numero do Elemento:" << endl;
		cin >> novoint;
		this->elementos[pos].setNum(novoint);
		dados2.alterarElementosNum(this->elementos[pos].getID(), novoint);
		break;

	case 3:
		cout << "Introduza um email:" << endl;
		cin >> novastring;
		novastring = util2.converterMinuscula(novastring);

		this->elementos[pos].setEmail(novastring);
		dados2.alterarElementosEmail(this->elementos[pos].getID(), novastring);
		break;
	}
}

void Projeto::alterarRobos(int pos, int alteracao) {

	double novodouble;
	int novoint;
	string novastring;

	bool same = false;
	
	switch (alteracao) {
	case 1:
		
		cout << "Introduza o novo nome do robo:" << endl;
		cin >> novastring;
		novastring = util2.converterMinuscula(novastring);

		this->robos[pos].setNome(novastring);
		dados2.alterarRobosNome(this->robos[pos].getID(), novastring);
		break;
	case 2:
		cout << "Introduza o novo modelo do robo:" << endl;
		cin >> novastring;
		novastring = util2.converterMinuscula(novastring);

		this->robos[pos].setModelo(novastring);
		dados2.alterarRobosModelo(this->robos[pos].getID(), novastring);
		break;

	case 3:

		cout << "Introduza o numero de sensores do robo:" << endl;
		cin >> novoint;

		this->robos[pos].setNum_sensor(novoint);
		dados2.alterarRobosNum_sensor(this->robos[pos].getID(), novoint);
		break;
	case 4:
		cout << "Introduza o melhor tempo do robo:" << endl;
		cin >> novodouble;

		this->robos[pos].setMelhor_tempo(novodouble);
		dados2.alterarRobosMelhor_tempo(this->robos[pos].getID(), novodouble);
		break;
	}
}

void Projeto::alterarProvas(int pos, int alteracao) {

	double novodouble;
	string novastring;

	bool same = false;

	switch (alteracao) {
	case 1:
		
		cout << "Introduza os novos estados:" << endl;
		cin >> novastring;
		novastring = util2.converterMinuscula(novastring);

		this->provas[pos].setEstados(novastring);
		dados2.alterarProvasEstados(this->provas[pos].getID(), novastring);
		break;
	case 2:
		cout << "Introduza o tempo:" << endl;
		cin >> novodouble;

		this->provas[pos].setTempo(novodouble);
		dados2.alterarProvasTempo(this->provas[pos].getID(), novodouble);
		break;
	}
}

void Projeto::apagarEquipas(int pos) {
	dados2.apagarEquipas(this->equipas[pos].getID());
	this->equipas.erase(equipas.begin() + pos);
}
void Projeto::apagarElementos(int pos) {
	dados2.apagarElementos(this->elementos[pos].getID());
	this->elementos.erase(elementos.begin() + pos);
}
void Projeto::apagarRobos(int pos) {
	dados2.apagarRobos(this->robos[pos].getID());
	this->robos.erase(robos.begin() + pos);
}
void Projeto::apagarProvas(int pos) {
	dados2.apagarProvas(this->provas[pos].getID());
	this->provas.erase(provas.begin() + pos);
}